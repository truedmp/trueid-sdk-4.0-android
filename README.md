# TrueID SDK #

### **Latest Version: 4.12.0** ###

### Overview ###

“TrueID SDK” is the framework gathering authentication patterns of TrueID Login. 

### Requirements ###

* minSdkVersion >= 9
* Permission
    - (Manifest.permission.READ_EXTERNAL_STORAGE) //SSO Required
    - (Manifest.permission.WRITE_EXTERNAL_STORAGE) //SSO Required
    - (Manifest.permission.CAMERA) //QR Code Required

### Implementation ###

#### Import SDK from dependency at build.gradle in app folder ####

```
#!gradle
repositories {
    mavenCentral()
}

dependencies {
    compile 'io.bitbucket.truedmp:trueid-sso-sdk:4.12.0'
}
```

#### Define variable in file values/string.xml ####

```
#!xml
<string name="trueid_client_id">X</string>
<string name="trueid_client_flow">Z</string>

<string name="trueid_client_scheme_for_sdk">your_scheme_sdk</string>
<string name="trueid_client_scheme_for_app">your_scheme_app</string>
```
* Get an X and Z  please contact True ID team.
* your_scheme_sdk : This param must be related with value in TrueID app to refer to your app correctly.(like your app name with no space for sdk authentication. Ex. trueid, trueidmusic, trueidtv, anywhere etc)
* your_scheme_app : Specify this param with no space for jumping to your activity which implement LoginServiceListener

#### Define Activity for True ID App in AndroidManifest.xml ####
```
#!xml
<!-- Define AuthActivity in Android Manifest -->
<activity
      android:name="com.tdcm.truelifelogin.activities.AuthActivity"
      android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
      android:theme="@android:style/Theme.Translucent.NoTitleBar">

      <intent-filter>
            <action android:name="android.intent.action.VIEW"/>
            <category android:name="android.intent.category.DEFAULT"/>
            <category android:name="android.intent.category.BROWSABLE"/>
            <data android:scheme="@string/trueid_client_scheme_for_sdk"/>
      </intent-filter>

</activity>

<!-- Example your Activity which implement LoginServiceListener (Only login case) -->
<activity android:name="com.example.project.YourActivity">
      <intent-filter>
            <action android:name="android.intent.action.VIEW"/>
            <category android:name="android.intent.category.DEFAULT"/>
            <category android:name="android.intent.category.BROWSABLE"/>
            <data android:scheme="@string/trueid_client_scheme_for_app"/>
      </intent-filter>
</activity>

```

#### Implement and interface class in Activity ####
```
#!kotlin
class MainActivity: AppCompatActivity(), LoginServiceListener {
```

#### Initial SDK ####
```
#!kotlin
// Single Sign On (SSO)
val service = LoginService(activity: Activity, scope: List<String>, reDirectUrl: String, env: SDKEnvironment)

// Self Login (Without SSO)
val service = LoginService(activity: Activity, scope: List<String>, reDirectUrl: String, isSelf: Boolean
      , env: SDKEnvironment)
```
* Arrays.asList (“public_profile”, ”mobile”, ”email”, ”references”) or (InitialScope.PROFILE, InitialScope.MOBILE, InitialScope.EMAIL, InitialScope.REFERENCE)
* String reDirectUrl = Redirect url client / default is empty string
* Boolean isSelf auto default is FALSE for using single sign on (SSO)
* SDKEnvironment is STAGING or PRODUCTION

#### Register & Forgot Password ####
```
#!kotlin
// Request
service.register(language: String, latitude: String, longitude: String, flow: String, isAutoLogin: Boolean)
// Callback
override fun onRegisterSuccess(login_code: String?, client_id: String?) {
}
override fun onRegisterError(errorObject: String?) {
}

// Request
service.recovery(language: String, latitude: String, longitude: String, isAutoLogin: Boolean)
// Callback
override fun onForgotSuccess(login_code: String?, client_id: String?) {
}
override fun onForgotError(errorObject: String?) {
}
```
* String language “th” = Thai language / “en” = English language
* String latitude = north-south point position on the Earth’s surface.
* String longitude = east-west point position on the Earth’s surface.
* String flow “A” = sign-up request default / “D” = sign-up request e-mail.
* Boolean isAutoLogin true = Auto login when success , false = return only login code 

#### Login ####
```
#!kotlin
// Request
service.login(language: String, latitude: String, longitude: String, isAutoLogin: Boolean)
// Callback
override fun onLoginSuccess(json: String?, expires_second: Int) {
}
override fun onLoginError(msg: String?) {
}
```
* String language “th” = Thai language / “en” = English language
* String latitude = north-south point position on the Earth’s surface.
* String longitude = east-west point position on the Earth’s surface.
* Boolean isAutoLogin true = default

#### Get User’s Profile (JWT) ####
```
#!kotlin
// Request 
service.getLoginInfo()
// Callback
override fun onLoginSuccess(json: String?, expires_second: Int) {
}
override fun onLoginError(msg: String?) {
}
```

#### Get User’s Profile (Server) ####
```
#!kotlin
// Request 
service.getProfileMore()
// Callback
override fun onGetInfoSuccess(json: String?, expires_second: Int) {
}
override fun onGetInfoFailed(errorMsg: String?) {
}
```

#### Verify Thai ID (Mapping Product) ####
```
#!kotlin
// Request
service.verifyThaiId(language: String)
// Callback
override fun onMappingAlready(msg: String?) {
}
override fun onMappingSuccess(msg: String?) {
}
override fun onMappingFailed(msg: String?) {
}
```
* String language “th” = Thai language / “en” = English language

#### Refresh Token ####
```
#!kotlin
override fun onResume() {
   super.onResume()
   service.onResume()
}
// Callback
override fun onRefreshAccessToken(isSuccess: Boolean) {
}
override fun onRefreshAccessTokenFailed(errorObject: String?) {
}
```
* Access token will be expired after 1 hour auto refresh it by calling service.onResume() in onResume method in Activity

#### Get Access Token or Verify Access Token (Client) ####
```
#!kotlin
// Not sure if it will expire
val accessToken: String = service.accessToken

// Fresh access token
// Request 
service.selfVerify(listener: VerifyListener)
// callback
override fun onVerifySuccess(accessToken: String?) {
}
override fun onVerifyFailed(errorMsg: String?) {
}
```

#### Get Refresh Token (Client) ####
```
#!kotlin
val refreshToken: String = service.refreshToken
```

#### User cancel ####
```
#!kotlin
// Callback
override fun onCanceled() {
}
```
* This callback will return when user press back while the process has not finish yet. 

#### Logout ####
```
#!kotlin
// Request
service.logout()
// Callback
override fun onLogoutRespond(isSuccess: Boolean, json: String?) {
}
```

#### Find TrueID App ####
```
#!kotlin
// Request
service.findTrueIDApp()
// Callback
override fun onFindTrueIDApp(isFound: Boolean) {
}
```

#### Call TrueID App ####
```
#!kotlin
service.callTrueIDApp()
```
* If TrueID app is installed in your device and when you use this  method it will take you to TrueID app but if you haven’t installed TrueID app yet it will alert you to go to download TrueID app in Google Play. 

#### Google Analytics Tracking Screen ####
```
#!kotlin
// Callback
override fun onReceivedScreen(screens: Screens?) {
   val screenName: String? = screens?.screenName
}]
```

#### Google Analytics Event Tracking ####
```
#!kotlin
// Callback
override fun onReceivedEvent(events: Events?) {
   val eventAction: String? = events?.action
   val eventCategory: String? = events?.category
   val eventLabel: String? = events?.label
}
```

#### Get DeviceID ####
```
#!kotlin
val deviceID: String = service.deviceID
```

#### Revoke Already ####
```
#!kotlin
// Callback
override fun onRevokeAlready() {
}
```
* This callback will return when user logout with other application.

#### Check User isLogin ####
```
#!kotlin
val isLogin: Boolean = service.isLogin
```

Please be noted that you should avoid publishing the same version because it will overwritten the previous one and may result in inconsistency release.