package com.tdcm.simple;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tdcm.simple.activities.CoreActivity;

/**
 * Created by Pratanporn on 9/10/17 AD.
 */
public class SplashScreenActivity extends Activity{
    protected final static String TAG = SplashScreenActivity.class.getSimpleName();

    public static final int ACCESS = 0;
    public static final int LIVE_PLAY = 1;
    public static final int NEAR_ME = 2;
    public static final int MY_ACCOUNT = 3;
    public static final int PAYMENT = 4;

    final int splashTimeOut = 4000;
    boolean hasIntent = false;
    boolean fromDeepLink = false;
    boolean fromDeepLinkNotification = false;
    String mReferralApp;
    boolean fromTrueApp;
    String mDeepLinkUrl;
    String mDeepLinkUrlFromNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Log.e(TAG, "onCreate");

        Intent intent = getIntent();
        if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_VIEW)) {
            if (intent.getData() != null) {
                String scheme = intent.getData().getScheme();
                String referral = intent.getData().getQueryParameter("referral");
                Log.e(TAG, "scheme "+ scheme);
                Log.e(TAG, "referral "+ referral);

                if (scheme.equalsIgnoreCase("trueid")) {
                    intent.putExtra("referral_app", referral);
                    intent.putExtra("from_true_app", (referral != null));
                }else {
                    Log.e(TAG, "scheme not true id null");
                }
            } else {
                Log.e(TAG, "data has null");
            }
        } else {
            Log.e(TAG, "action view has null");
        }

        intent = new Intent(SplashScreenActivity.this, CoreActivity.class);
        startActivity(intent);
        finish();
//        validateIntentFromOtherApp();
//        startExitTimeout();
    }

    private void validateIntentFromOtherApp() {
        Intent intent = getIntent();
        if (intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_VIEW)) {
                hasIntent = true;
                String scheme = intent.getData().getScheme();
                if (scheme.equalsIgnoreCase("trueid")) {
                    mReferralApp = intent.getData().getQueryParameter("referral");
                    fromTrueApp = mReferralApp != null;
                }
            }
        }
    }

//    private void validateIntentFromDeepLink() {
//        // Build GoogleApiClient with AppInvite API for receiving deep links
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(AppInvite.API)
//                .build();
//
//        // Check if this app was launched from a deep link. Setting autoLaunchDeepLink to true
//        // would automatically launch the deep link if one is found.
//        boolean autoLaunchDeepLink = false;
//        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
//                .setResultCallback(
//                        new ResultCallback<AppInviteInvitationResult>() {
//                            @Override
//                            public void onResult(@NonNull AppInviteInvitationResult result) {
//                                if (result.getStatus().isSuccess()) {
//                                    // Extract deep link from Intent
//                                    Intent intent = result.getInvitationIntent();
//                                    String deepLink = AppInviteReferral.getDeepLink(intent);
//
//                                    if (!deepLink.isEmpty()) {
//                                        fromDeepLink = true;
//                                        mDeepLinkUrl = deepLink;
//                                    }
//
//                                    // Handle the deep link. For example, open the linked
//                                    // content, or apply promotional credit to the user's
//                                    // account.
//
//                                    // ...
//                                } else {
//                                    Log.d(TAG, "getInvitation: no deep link found.");
//                                }
//                            }
//                        });
//    }

    private void startExitTimeout() {
        Thread splashThread = new Thread() {
            int wait = 0;

            @Override
            public void run() {
                try {
                    super.run();
                    while (wait < splashTimeOut) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (Exception ignored) {

                } finally {
                    ExitSplashScreen();
                }
            }
        };
        splashThread.start();
    }

    private void ExitSplashScreen() {
        Intent intent = null;
        //            intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent = new Intent(SplashScreenActivity.this, CoreActivity.class);
        if (hasIntent) {
            intent.putExtra("referral_app", mReferralApp);
            intent.putExtra("from_true_app", fromTrueApp);
        }

        startActivity(intent);
        finish();
    }
}
