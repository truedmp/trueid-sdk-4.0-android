package com.tdcm.simple

import android.app.Application
import com.tdcm.truelifelogin.authentication.TrueIDSDK

/**
 * Created by boyDroids on 27/6/2018 AD.
 * ^.^
 */
class ThisApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        TrueIDSDK.onApplicationCreate()
    }
}