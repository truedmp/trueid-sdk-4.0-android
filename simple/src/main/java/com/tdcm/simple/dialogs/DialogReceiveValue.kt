package com.tdcm.simple.dialogs

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.tdcm.simple.R
import kotlinx.android.synthetic.main.dialog_receive_value.*

internal class DialogReceiveValue(context: Context) : Dialog(context) {

    companion object {
        private val TAG = DialogReceiveValue::class.java.simpleName
    }

    interface WalletIdListener {
        fun onValues(bindingIds: String)
        fun onCancel()
    }

    private var callback: WalletIdListener? = null

    fun setWalletIdListener(callback: WalletIdListener): DialogReceiveValue {
        this.callback = callback
        return this@DialogReceiveValue
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_receive_value)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        setOnCancelListener {
            callback?.onCancel()
            dismiss()
        }

        closeBtn.setOnClickListener {
            callback?.onCancel()
            dismiss()
        }

        buttonConfirm.setOnClickListener {
            val value = inputValue.text.toString()
            if (value.isEmpty()) {
                Toast.makeText(context, "Please input value...", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            callback?.onValues(value)
            dismiss()
        }
    }
}