package com.tdcm.simple.dialogs

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.tdcm.simple.R
import kotlinx.android.synthetic.main.dialog_receive_qr_data.*

/*
 * Created by boyDroids on 5/10/2018 AD.
 * ^.^
 */
internal class DialogQRData(context: Context) : Dialog(context) {

    companion object {
        private val TAG = DialogQRData::class.java.simpleName
    }

    interface QRDataListener {
        fun onValues(json: String)
        fun onCancel()
    }

    private var callback: QRDataListener? = null

    fun setListener(callback: QRDataListener): DialogQRData {
        this.callback = callback
        return this@DialogQRData
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_receive_qr_data)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        setOnCancelListener {
            callback?.onCancel()
            dismiss()
        }

        closeBtn.setOnClickListener {
            callback?.onCancel()
            dismiss()
        }

        buttonConfirm.setOnClickListener {
            val value = inputData.text.toString()
            if (value.isEmpty()) {
                Toast.makeText(context, "Please input value Json from qr code", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            callback?.onValues(value)
            dismiss()
        }
    }
}