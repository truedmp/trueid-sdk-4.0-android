package com.tdcm.simple.utility

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.activity.ComponentActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class Utilities {

    private val appTrueId = "trueid"

    private fun permissionNeed(): ArrayList<String> {
        val need = ArrayList<String>()
        need.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        need.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        need.add(Manifest.permission.ACCESS_FINE_LOCATION)
        return need
    }

    fun checkPermission(componentActivity: ComponentActivity): Boolean {
        val listNeed = permissionNeed()
        val listGranted = ArrayList<String>()
        for (i in listNeed.indices) {
            if (ContextCompat.checkSelfPermission(
                            componentActivity,
                            listNeed[i]
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                listGranted.add(listNeed[i])
            }
        }
        if (listGranted.isNotEmpty()) {
            ActivityCompat.requestPermissions(componentActivity, listNeed.toTypedArray(), 100)
            return false
        }
        return true
    }

    fun validateIntentFromOtherApp(activity: Activity): Boolean {
        val intent = activity.intent
        if (intent.action == null) return false
        if (intent.data == null) return false

        if (intent.action == Intent.ACTION_VIEW) {
//            if (intent.data.scheme == appTrueId){
//                return intent.data.getQueryParameter("referral") != null
//            }
            return intent.data?.let { it.scheme == appTrueId } == true &&
                    intent.data?.let {
                        it.getQueryParameter("referral") != null
                    } == true

        }
        return false
    }

    fun toJsonViewer(json: String?): String {
        if (json == null) return ""
        val jsonFormat = StringBuilder()
        var indentString = ""

        for (i in json.indices) {
            val letter = json[i]
            when (letter) {
                '{', '[' -> {
                    jsonFormat.append("\n" + indentString + letter + "\n")
                    indentString += "\t"
                    jsonFormat.append(indentString)
                }
                '}', ']' -> {
                    indentString = indentString.replaceFirst("\t".toRegex(), "")
                    jsonFormat.append("\n" + indentString + letter)
                }
                ',' -> jsonFormat.append(letter + "\n" + indentString)
                else -> jsonFormat.append(letter)
            }
        }
        return jsonFormat.toString()
    }
}
