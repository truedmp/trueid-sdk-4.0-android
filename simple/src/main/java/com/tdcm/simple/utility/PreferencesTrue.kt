package com.tdcm.simple.utility

import android.content.Context
import android.content.SharedPreferences

class PreferencesTrue(context: Context) {

    private val prefs: SharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    /**
     * =========================================================================================
     * Key Value
     * =========================================================================================
     **/
    private val environmentKey: String = "ENVIRONMENT_KEY"
    private val ssoKey: String = "SSO_KEY"
    private val clientIdKey: String = "CLIENT_ID_KEY"

    var environment: String
        get() = prefs.getString(environmentKey, "").toString()
        set(value) = prefs.edit().putString(environmentKey, value).apply()

    var sso: String
        get() = prefs.getString(ssoKey, "").toString()
        set(value) = prefs.edit().putString(ssoKey, value).apply()

    var clientId: String
        get() = prefs.getString(clientIdKey, "").toString()
        set(value) = prefs.edit().putString(clientIdKey, value).apply()
}