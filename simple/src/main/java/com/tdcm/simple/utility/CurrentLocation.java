package com.tdcm.simple.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by boyDroid on 6/5/2017 AD.
 * theerapong.petchsitong@gmail.com
 */

public class CurrentLocation {
    protected final static String TAG = CurrentLocation.class.getSimpleName();

    private static final int TIME_MIN = 1000 * 60 * 2;
    private static final int TIME_RUNNABLE = 1000;
    private static final long LISTENER_MIN_TIME = 0L;
    private static final float LISTENER_MIN_DISTRICT = 0.0f;

    private Context context;

    public CurrentLocation(Context context) {
        this.context = context;
    }

    private Location currentBestLocation = null;
    private ServiceLocationListener gpsLocationListener;
    private ServiceLocationListener networkLocationListener;
    private ServiceLocationListener passiveLocationListener;
    private LocationManager locationManager;

    private Handler handler = new Handler();

    public Location fetchLocation() {
        if (context == null) return null;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager == null) return null;

        try {
            LocationProvider gpsProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
            LocationProvider networkProvider = locationManager.getProvider(LocationManager.NETWORK_PROVIDER);
            LocationProvider passiveProvider = locationManager.getProvider(LocationManager.PASSIVE_PROVIDER);

            //Figure out if we have a location somewhere that we can use as a current best location
            if (gpsProvider != null) {
                Location lastKnownGPSLocation = locationManager.getLastKnownLocation(gpsProvider.getName());
                if (isBetterLocation(lastKnownGPSLocation, currentBestLocation))
                    currentBestLocation = lastKnownGPSLocation;
            }

            if (networkProvider != null) {
                Location lastKnownNetworkLocation = locationManager.getLastKnownLocation(networkProvider.getName());
                if (isBetterLocation(lastKnownNetworkLocation, currentBestLocation))
                    currentBestLocation = lastKnownNetworkLocation;
            }

            if (passiveProvider != null) {
                Location lastKnownPassiveLocation = locationManager.getLastKnownLocation(passiveProvider.getName());
                if (isBetterLocation(lastKnownPassiveLocation, currentBestLocation)) {
                    currentBestLocation = lastKnownPassiveLocation;
                }
            }

            gpsLocationListener = new ServiceLocationListener();
            networkLocationListener = new ServiceLocationListener();
            passiveLocationListener = new ServiceLocationListener();

            if (gpsProvider != null) {
                locationManager.requestLocationUpdates(gpsProvider.getName(), LISTENER_MIN_TIME, LISTENER_MIN_DISTRICT, gpsLocationListener);
            }

            if (networkProvider != null) {
                locationManager.requestLocationUpdates(networkProvider.getName(), LISTENER_MIN_TIME, LISTENER_MIN_DISTRICT, networkLocationListener);
            }

            if (passiveProvider != null) {
                locationManager.requestLocationUpdates(passiveProvider.getName(), LISTENER_MIN_TIME, LISTENER_MIN_DISTRICT, passiveLocationListener);
            }

            if (gpsProvider != null || networkProvider != null || passiveProvider != null) {
                handler.postDelayed(timerRunnable, TIME_RUNNABLE);
            } else {
                handler.post(timerRunnable);
            }

            Log.d(TAG, "current best " + new Gson().toJson(currentBestLocation));
            return currentBestLocation;
        } catch (SecurityException se) {
            finish();
            return null;
        }
    }

    private class ServiceLocationListener implements android.location.LocationListener {

        @Override
        public void onLocationChanged(Location newLocation) {
            synchronized (this) {
                if (isBetterLocation(newLocation, currentBestLocation)) {
                    currentBestLocation = newLocation;

                    if (currentBestLocation != null && currentBestLocation.hasAccuracy() && currentBestLocation.getAccuracy() <= 100) {
                        finish();
                    }
                }
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    }

    private synchronized void finish() {
        handler.removeCallbacks(timerRunnable);
        handler.post(timerRunnable);
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    private static boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null || location == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TIME_MIN;
        boolean isSignificantlyOlder = timeDelta < -TIME_MIN;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            if (currentBestLocation != null) {
                locationManager.removeUpdates(gpsLocationListener);
                locationManager.removeUpdates(networkLocationListener);
                locationManager.removeUpdates(passiveLocationListener);
            }
        }
    };
}


