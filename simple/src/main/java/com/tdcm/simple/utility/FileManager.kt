package com.tdcm.simple.utility

import android.os.Build
import android.os.Environment
import java.io.File

/**
 *  Created by Yothinindy on 8/14/2018 AD
 */
object FileManager {

    private fun getDirectory(path: String?): File {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            /*--- /storage/sdcard0/Documents ---*/
            val dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), path)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            dir
        } else {
            /*--- /storage/sdcard0 ---*/
            val dir = File(Environment.getExternalStorageDirectory(), path)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            dir
        }
    }

    fun getFileToken(path: String): File {
        val dir = getDirectory(path)
        return File(dir, "")
    }

    fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory)
            for (child in fileOrDirectory.listFiles()!!)
                deleteRecursive(child)

        fileOrDirectory.delete()
    }
}