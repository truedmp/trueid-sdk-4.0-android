package com.tdcm.simple;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.tdcm.truelifelogin.authentication.LoginService;
import com.tdcm.truelifelogin.interfaces.LoginServiceListener;
import com.tdcm.truelifelogin.models.Events;
import com.tdcm.truelifelogin.models.Screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity implements LoginServiceListener {
    protected final static String TAG = MainActivity.class.getSimpleName();

    private Button btnGetAccessToken, btnGetInfo, btnGetRefreshToken, btnLogout, btnRegister, btn_VerifyThaiId,
            btnGetProfileServer, btnClear, btn_startService, btn_stopService, btn_Login_Native, btn_Forgot, btnLogin;
    private Switch sbtn, s_flow_btn, s_auto_btn, swt_runservice;
    private TextView txt, location_txt;
    private LinearLayout loginZone, linearLayout1, linearLayout2;
    private EditText edt_scope;
    private LoginService mLoginService;
    private boolean is4g = false;
    private boolean isWifi = false;
    private boolean condition = false;
    private boolean isRunService = false;
    private boolean isTracking = false;
    private String lat = "0.0", lng = "0.0";
    String lang = "en";
    String flow = "A";

    boolean hasIntent = false;
    String mReferralApp;
    boolean fromTrueApp;
    final int splashTimeOut = 4000;

    LinearLayout layout_goto;
    Button btn_sdk3_1, btn_sdk3_2, btn_sdk3_3, btn_sdk4_1, btn_sdk4_2, btn_sdk4_3;

    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkPermissions();
        }

        bindWidget();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        validateIntentFromOtherApp();
        startExitTimeout();

        mLoginService = new LoginService(this, Arrays.asList(""), false, "");

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoginService.login(lang, lat, lng, condition);
//                mLoginService.login(lang, lat, lng, condition, isRunService);
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText("");
            }
        });

        btnGetInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginService.getLoginInfo();
            }
        });

        findViewById(R.id.btnCallTrueIdApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginService.callTrueIDApp();
            }
        });

        btnGetAccessToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.setText(formatString(String.format("AccessToken: %s", mLoginService.getAccessToken())));
            }
        });

        btnGetRefreshToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.setText(formatString(String.format("RefreshToken: %s", mLoginService.getRefreshToken())));
            }
        });

        findViewById(R.id.btnFindApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginService.FindTrueIDApp();
            }
        });

        btn_Login_Native.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btn_Forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginService.register(lang, lat, lng, flow, condition);
//                mLoginService.register(lang, lat, lng, flow, condition, isRunService);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mAlert = new AlertDialog.Builder(MainActivity.this);
                mAlert.setMessage(getString(R.string.logout_title));
                mAlert.setCancelable(false);
                mAlert.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mLoginService.logout();
                    }
                });
                mAlert.setNegativeButton(getString(android.R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                mAlert.show();
            }
        });

        btn_startService = findViewById(R.id.btn_startService);
        btn_startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mLoginService.startService();
            }
        });

        btn_stopService = findViewById(R.id.btn_stopService);
        btn_stopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "stop_service", Toast.LENGTH_LONG).show();
            }
        });

        btn_VerifyThaiId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        sbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setText("TH");
                    lang = "th";
                } else {
                    buttonView.setText("EN");
                    lang = "en";
                }
            }
        });

        s_flow_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setText("Flow : D");
                    flow = "D";
                } else {
                    buttonView.setText("Flow : A");
                    flow = "A";
                }
            }
        });

        s_auto_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setText("Auto Login : True");
                    condition = true;
                } else {
                    buttonView.setText("Auto Login : False");
                    condition = false;
                }
            }
        });

        swt_runservice = (Switch) findViewById(R.id.swt_runservice);
        swt_runservice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setText("Run BG service : TRUE     ");
                    isRunService = true;
                } else {
                    buttonView.setText("Run BG service : FALSE     ");
                    isRunService = false;
                }
            }
        });

        Switch swt_ga = findViewById(R.id.swt_ga);
        swt_ga.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    compoundButton.setText("GA Tracking : Event");
                    isTracking = true;
                } else {
                    compoundButton.setText("GA Tracking : Screen");
                    isTracking = false;
                }
            }
        });


        layout_goto = findViewById(R.id.layout_goto);
        layout_goto.setVisibility(View.GONE);

        btn_sdk3_1 = findViewById(R.id.btn_sdk3_1);
        btn_sdk3_2 = findViewById(R.id.btn_sdk3_2);
        btn_sdk3_3 = findViewById(R.id.btn_sdk3_3);

        btn_sdk4_1 = findViewById(R.id.btn_sdk4_1);
        btn_sdk4_2 = findViewById(R.id.btn_sdk4_2);
        btn_sdk4_3 = findViewById(R.id.btn_sdk4_3);

        btn_sdk3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("com.tdcm.trueidapp");
            }
        });
        btn_sdk3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("truetv3.sdk.trueid.apptruetv_3");
            }
        });
        btn_sdk3_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("truetv3.sdk.trueid.apptruemusic_3");
            }
        });
        btn_sdk4_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("com.tdcm.trueidapp");
            }
        });
        btn_sdk4_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("com.brown.pratanporn.apptruetv");
            }
        });
        btn_sdk4_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenApp("com.brown.pratanporn.demo1");
            }
        });

        edt_scope.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                mLoginService.setScope(Arrays.asList(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onOpenApp(String name) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(name);
        if (launchIntent != null) {
            startActivity(launchIntent);
        } else {
            Toast.makeText(this, "no app name: " + name, Toast.LENGTH_LONG).show();
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    private void getLocation() {

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location;
        if (isWifi) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        if (location != null) {
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
        }
//        location_txt.setText(lat + "," + lng);
        location_txt.setText(String.format("%s,%s", lat, lng));

        final LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lat = String.valueOf(location.getLatitude());
                lng = String.valueOf(location.getLongitude());
//                location_txt.setText(lat + "," + lng);
                location_txt.setText(String.format("%s,%s", lat, lng));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 200, 10, locationListener);
    }

    private void bindWidget() {
        btnGetAccessToken = findViewById(R.id.btnAccessToken);
        btnGetInfo = findViewById(R.id.btnGetInfo);
        btnGetRefreshToken = findViewById(R.id.btnRefreshToken);
        btnLogout = findViewById(R.id.btnLogout);
        btn_Forgot = findViewById(R.id.btnForgotPass);
        btn_Login_Native = findViewById(R.id.btnLogin_native);
        btnRegister = findViewById(R.id.btnRegister);
        btn_VerifyThaiId = findViewById(R.id.btnVerifyThaiId);
        btnClear = findViewById(R.id.btnClear);
        loginZone = findViewById(R.id.loginZone);
        btnGetProfileServer = findViewById(R.id.btnGetInfoServer);
        txt = findViewById(R.id.txt);
        location_txt = findViewById(R.id.location_txt);
        sbtn = findViewById(R.id.swt_btn);
        s_flow_btn = findViewById(R.id.swt_flow_btn);
        s_auto_btn = findViewById(R.id.swt_auto_btn);
        linearLayout1 = findViewById(R.id.linearLayout1);
        linearLayout2 = findViewById(R.id.linearLayout2);
        edt_scope = findViewById(R.id.edt_scope);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoginService.onResume();
        switchButton();
    }

    @Override
    public void onLoginSuccess(String response, int expires_second) {
        switchButton();
        txt.setText(formatString(response + "expires_second = " + expires_second));
    }

    @Override
    public void onLoginError(String msg) {
        txt.setText(formatString(msg));
    }

    @Override
    public void onLogoutRespond(boolean isSuccess, String json) {
        switchButton();
        txt.setText(formatString(String.format("onLogoutRespond: %s", json)));
    }

    @Override
    public void onRefreshAccessToken(boolean isSuccess) {
        switchButton();
    }

    @Override
    public void onCanceled() {
        txt.setText("onCanceled");
    }

    @Override
    public void onFindTrueIDApp(boolean isFound) {
        txt.setText(String.format("onFindTrueIDApp: %s", isFound));
    }

    @Override
    public void onMappingAlready(String msg) {

    }

    @Override
    public void onMappingSuccess(String msg) {

    }

    @Override
    public void onMappingFailed(String msg) {

    }

    @Override
    public void onRegisterSuccess(String login_code, String client_id) {

    }

    @Override
    public void onRegisterError(String errorObject) {

    }

    @Override
    public void onForgotSuccess(String login_code, String client_i) {

    }

    @Override
    public void onForgotError(String errorObject) {

    }

    @Override
    public void onGetInfoSuccess(String json, int expires_second) {

    }

    @Override
    public void onGetInfoFailed(String errormsg) {

    }

    @Override
    public void onReceivedScreen(Screens screens) {

    }

    @Override
    public void onReceivedEvent(Events events) {

    }

    @Override
    public void onRefreshAccessTokenFailed(String errorObject) {

    }

    @Override
    public void onRevokeAlready() {

    }

    private void switchButton() {
        String getAccessToken = mLoginService.getAccessToken();
        if (getAccessToken.isEmpty()) {
            btnGetAccessToken.setVisibility(View.GONE);
            btnGetInfo.setVisibility(View.GONE);
            btnGetProfileServer.setVisibility(View.GONE);
            btnGetRefreshToken.setVisibility(View.GONE);
            btnLogout.setVisibility(View.GONE);
            btnRegister.setVisibility(View.VISIBLE);
            btn_Forgot.setVisibility(View.VISIBLE);
            btn_Login_Native.setVisibility(View.VISIBLE);
            loginZone.setVisibility(View.VISIBLE);
            s_auto_btn.setVisibility(View.VISIBLE);
            s_flow_btn.setVisibility(View.VISIBLE);
            sbtn.setVisibility(View.VISIBLE);
            btn_VerifyThaiId.setVisibility(View.GONE);
            btn_startService.setVisibility(View.GONE);
            btn_stopService.setVisibility(View.GONE);
            swt_runservice.setVisibility(View.VISIBLE);
            linearLayout1.setVisibility(View.VISIBLE);
            linearLayout2.setVisibility(View.VISIBLE);
            layout_goto.setVisibility(View.GONE);
        } else {
            btnGetAccessToken.setVisibility(View.VISIBLE);
            btnGetInfo.setVisibility(View.VISIBLE);
            btnGetProfileServer.setVisibility(View.VISIBLE);
            btnGetRefreshToken.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.VISIBLE);
            btnRegister.setVisibility(View.GONE);
            btn_Forgot.setVisibility(View.GONE);
            btn_Login_Native.setVisibility(View.GONE);
            loginZone.setVisibility(View.GONE);
            btn_VerifyThaiId.setVisibility(View.VISIBLE);
            s_auto_btn.setVisibility(View.GONE);
            s_flow_btn.setVisibility(View.GONE);
            sbtn.setVisibility(View.GONE);
            swt_runservice.setVisibility(View.GONE);
            linearLayout1.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.GONE);
            if (isRunService) {
                btn_startService.setVisibility(View.VISIBLE);
                btn_stopService.setVisibility(View.VISIBLE);
            }
            layout_goto.setVisibility(View.VISIBLE);
        }
    }

    public static String formatString(String text) {

        StringBuilder json = new StringBuilder();
        String indentString = "";

        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            switch (letter) {
                case '{':
                case '[':
                    json.append("\n" + indentString + letter + "\n");
                    indentString = indentString + "\t";
                    json.append(indentString);
                    break;
                case '}':
                case ']':
                    indentString = indentString.replaceFirst("\t", "");
                    json.append("\n" + indentString + letter);
                    break;
                case ',':
                    json.append(letter + "\n" + indentString);
                    break;

                default:
                    json.append(letter);
                    break;
            }
        }

        return json.toString();
    }

    private void validateIntentFromOtherApp() {
        Intent intent = getIntent();
        if (intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_VIEW)) {
                hasIntent = true;
                String scheme = intent.getData().getScheme();
                if (scheme.equalsIgnoreCase("trueid")) {
                    mReferralApp = intent.getData().getQueryParameter("referral");
                    fromTrueApp = mReferralApp != null;

                }
            }
        }
    }

    private void startExitTimeout() {
        Thread splashThread = new Thread() {
            int wait = 0;

            @Override
            public void run() {
                try {
                    super.run();
                    while (wait < splashTimeOut) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (Exception ignored) {

                } finally {
                    ExitSplashScreen();
                }
            }
        };
        splashThread.start();
    }

    private void ExitSplashScreen() {
        if (fromTrueApp) {
            btnLogin.performClick();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLoginService.onResume();
                Toast.makeText(MainActivity.this, "Permission (already) Granted!", Toast.LENGTH_SHORT).show();
                bindWidget();
                getLocation();
            }
            return;
        }
    }
}