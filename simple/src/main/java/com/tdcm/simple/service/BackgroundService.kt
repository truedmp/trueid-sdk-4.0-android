package com.tdcm.simple.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.tdcm.truelifelogin.authentication.AuthTokensManager

/**
 * Created by boyDroids on 6/6/2018 AD.
 * ^.^
 */
internal class BackgroundService : Service() {

    companion object {
        private val TAG = BackgroundService::class.java.simpleName

        fun start(context: Context) {
            Log.w(TAG, "BackgroundService is started")
            val intent = Intent(context, BackgroundService::class.java)
            context.startService(intent)
        }

        fun stop(context: Context) {
            Log.w(TAG, "BackgroundService is stopped")
            val intent = Intent(context, BackgroundService::class.java)
            context.stopService(intent)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        /**
         * Simple use function for get Access Token by Client
         * Please Used >> implementation 'com.truedmp.trueidsdk:0.0.0.20' for testing.
         * Before use this function please follow step from SDK document.
         * @see <a href="https://docs.google.com/document/d/1oi3xVwDFoNXkVTwoQvoAZWL59-RYh4S_xx00zTVySNQ/edit?usp=sharing">Document</a>
         * And function "AuthTokensManager(...)" can use after init >>
         * "LoginService(activity, listScope, isDev, directUrl) in activity class"
         * */
        AuthTokensManager(this@BackgroundService)
                .get(object : AuthTokensManager.AuthListener {
                    override fun onAuthSuccess(accessToken: String?) {
                        Log.w(TAG, "Access Token\n$accessToken")
                        stopSelf()
                    }

                    override fun onAuthFailed(errorMessage: String?, isRevoke: Boolean) {
                        /**
                         * @param errorMessage is showing msg or manage something.
                         * @param isRevoke is return "true" please clear session in application and login again.
                         * */
                        Log.w(TAG, "errorMessage : $errorMessage")
                        Log.w(TAG, "isRevoke : $isRevoke")
                        stopSelf()
                    }
                })
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }
}