package com.tdcm.simple.activities

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.tdcm.simple.R
import com.tdcm.simple.service.BackgroundService

class NextActivity : BaseActivity() {

    override fun setTag(): AppCompatActivity {
        return this@NextActivity
    }

    override fun onCreateActivity(): Int {
        return R.layout.activity_next
    }

    override fun onActivityCreated() {
        Log.e(TAG, "NextActivity is onActivityCreated()")
    }

    companion object {
        fun startActivity(activity: Activity?) {
            activity?.let {
                Intent(activity, NextActivity::class.java).apply {
                    it.startActivity(this)
                }
            }
        }
    }

    /**
     * onStart() for enable background service for inside class,
     * auto running function get access token by "AuthTokensManager".
     * */
    override fun onStart() {
        super.onStart()
        BackgroundService.start(this@NextActivity)
    }

    /**
     * onDestroy() for disable background service running.
     * */
    override fun onDestroy() {
        super.onDestroy()
        BackgroundService.stop(this@NextActivity)
    }
}