package com.tdcm.simple.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tdcm.simple.R
import com.tdcm.simple.utility.Utilities
import com.tdcm.truelifelogin.authentication.TrueIDSDK

class SplashScreenActivity : BaseActivity() {

    private var handler: Handler? = null
    private var runnable: Runnable? = null
    private var delayTime: Long = 0
    private var time: Long = 2000L

    override fun setTag(): AppCompatActivity {
        return this@SplashScreenActivity
    }

    override fun onCreateActivity(): Int {
        return R.layout.activity_splash_screen
    }

    override fun onActivityCreated() {
        hideStatusBar()
        if (Utilities().checkPermission(this)) {
            checkIntent(intent)
        }

        Log.d(TAG, "TrueID SDK Version Name : ${TrueIDSDK.versionName()}")
        Log.d(TAG, "TrueID SDK Version Code : ${TrueIDSDK.versionCode()}")
    }

    private fun hideStatusBar() {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    private fun startHandler(intent: Intent) {
        handler = Handler()
        runnable = Runnable {
            startActivity(intent)
            finish()
        }

    }

    override fun onResume() {
        super.onResume()
        delayTime = time
        runnable?.let { _runnable ->
            handler?.postDelayed(_runnable, delayTime)
        }

        time = System.currentTimeMillis()

    }

    override fun onPause() {
        super.onPause()
        runnable?.let { _runnable ->
            handler?.removeCallbacks(_runnable)
        }

        time = delayTime - (System.currentTimeMillis() - time)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100 && (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(
                this@SplashScreenActivity,
                "Permission (already) Granted!",
                Toast.LENGTH_SHORT
            ).show()

            checkIntent(intent)
        }
    }

    private fun checkIntent(intent: Intent?) {
        val mIntent = Intent(this@SplashScreenActivity, CoreActivity::class.java)
        if (intent == null) {
            startHandler(mIntent)
            return
        }

        if (intent.action != null && intent.action == Intent.ACTION_VIEW) {
            intent.data?.also {
                val scheme = it.scheme
                val ref = it.getQueryParameter("referral")

                Log.w(TAG, "scheme : $scheme")
                Log.w(TAG, "referral : $ref")

                if (scheme.equals("trueid", true)) {
                    mIntent.putExtra("referral_app", ref)
                    mIntent.putExtra("from_true_app", (ref != null))
                }
            }
        }

        startHandler(mIntent)
    }
}
