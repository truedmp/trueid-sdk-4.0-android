package com.tdcm.simple.activities

import android.annotation.SuppressLint
import android.location.Location
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.tdcm.simple.BuildConfig
import com.tdcm.simple.R
import com.tdcm.simple.dialogs.DialogQRData
import com.tdcm.simple.dialogs.DialogReceiveValue
import com.tdcm.simple.utility.CurrentLocation
import com.tdcm.simple.utility.PreferencesTrue
import com.tdcm.simple.utility.Utilities
import com.tdcm.truelifelogin.authentication.LoginService
import com.tdcm.truelifelogin.authentication.TrueIDSDK
import com.tdcm.truelifelogin.constants.InitialScope
import com.tdcm.truelifelogin.constants.SDKEnvironment
import com.tdcm.truelifelogin.interfaces.*
import com.tdcm.truelifelogin.models.Events
import com.tdcm.truelifelogin.models.Screens
import kotlinx.android.synthetic.main.activity_core.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class CoreActivity : BaseActivity(), LoginServiceListener, ActionProcessListener {

    private lateinit var service: LoginService
    private var language = "EN"
    private var flow = "A"
    private var lat = "0.0"
    private var lon = "0.0"
    private var isAuto = false

    private var location: Location? = null

    private lateinit var pf: PreferencesTrue

    private lateinit var env: SDKEnvironment

    @SuppressLint("SetTextI18n")
    override fun onLoginSuccess(json: String?, expires_second: Int) {
        Log.d(
            "%%%%%",
            "CoreActivity.onLoginSuccess() called with: json = [$json], expires_second = [$expires_second]"
        )
        hideProgress()

        checkAccessToken()
        if (json == null) return
        val data = JSONObject(json).apply {
            put("expires_second", expires_second)
        }
        showTextViewJson("onLoginSuccess", "message", data.toString())
        getJson(json)
    }

    override fun onLoginError(msg: String?) {
        hideProgress()

        if (msg == null) return
        showTextViewJson("onLoginError", "error", msg)
    }

    override fun onLogoutRespond(isSuccess: Boolean, json: String?) {
        hideProgress()

        checkAccessToken()
        showTextViewJson("onLogoutRespond", "message", json ?: "")
    }

    override fun onRefreshAccessToken(isSuccess: Boolean) {
        hideProgress()

        checkAccessToken()
        showTextViewJson("onRefreshAccessToken", "isSuccess", isSuccess.toString())
    }

    override fun onRefreshAccessTokenFailed(errorObject: String?) {
        if (errorObject == null) return
        showTextViewJson("onRefreshAccessTokenFailed", "error", errorObject)
    }

    override fun onCanceled() {
        hideProgress()
        showTextViewJson("onCanceled", "message", "onCanceled")
    }

    override fun onFindTrueIDApp(isFound: Boolean) {
        showTextViewJson("onFindTrueIDApp", "isFound", isFound.toString())
    }

    override fun onMappingAlready(msg: String?) {
        if (msg == null) return
        showTextViewJson("onMappingAlready", "message", msg)
    }

    override fun onMappingSuccess(msg: String?) {
        if (msg == null) return
        showTextViewJson("onMappingSuccess", "message", msg)

    }

    override fun onMappingFailed(msg: String?) {
        if (msg == null) return
        showTextViewJson("onMappingFailed", "error", msg)
    }

    override fun onRegisterSuccess(login_code: String?, client_id: String?) {
        if (login_code == null && client_id == null) return
        val data = JSONObject().apply {
            put("loginCode", login_code)
            put("clientId", client_id)
        }
        showTextViewJson("onRegisterSuccess", "message", data.toString())
    }

    override fun onRegisterError(errorObject: String?) {
        if (errorObject == null) return
        showTextViewJson("onRegisterError", "error", errorObject)
    }

    override fun onForgotSuccess(login_code: String?, client_id: String?) {
        if (login_code == null && client_id == null) return
        val data = JSONObject().apply {
            put("login_code", login_code)
            put("client_id", client_id)
        }
        showTextViewJson("onForgotSuccess", "message", data.toString())
    }

    override fun onForgotError(errorObject: String?) {
        if (errorObject == null) return
        showTextViewJson("onForgotError", "error", errorObject)
    }

    @SuppressLint("SetTextI18n")
    override fun onGetInfoSuccess(json: String?, expires_second: Int) {
        if (json == null) return
        val value = JSONObject(json).apply {
            put("time", expires_second.toString())
        }
        showTextViewJson("onGetInfoSuccess", "message", value.toString())
        getJson(json)
    }

    override fun onGetInfoFailed(errormsg: String?) {
        if (errormsg == null) return
        showTextViewJson("onGetInfoFailed", "error", errormsg)
    }

    override fun onReceivedScreen(screens: Screens?) {
        Log.w(TAG, "onReceivedScreen ${Gson().toJson(screens)}")

    }

    override fun onReceivedEvent(events: Events?) {
        Log.w(TAG, "onReceivedEvent ${Gson().toJson(events)}")
    }

    override fun onRevokeAlready() {
        hideProgress()

        checkAccessToken()
        showTextViewJson("onRevokeAlready", "error", "onRevokeAlready")
    }

    /*from action listener*/
    override fun onFinish(isSuccess: Boolean) {
    }

    override fun setTag(): AppCompatActivity {
        return this@CoreActivity
    }

    override fun onCreateActivity(): Int {
        return R.layout.activity_core
    }

    override fun onActivityCreated() {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        /*check intent from trueId*/
        exitSplashScreen()

        /* init toolbar */
        initToolbar()

        /*init service login*/
        initLoginService(Arrays.asList(""))
        initLayoutListener()
        setJsonFormat()
    }

    override fun onResume() {
        super.onResume()
        service.onResume()
        checkAccessToken()

        location = CurrentLocation(this@CoreActivity).fetchLocation()
        if (location != null) {
            lat = location?.latitude.toString()
            lon = location?.longitude.toString()

            textLocation.text = String.format("$lat , $lon")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        service.logout().also {
            pf.environment = ""
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun initLoginService(listScope: List<String>) {
        val result = Gson().toJson(listScope)
        textScope.text = result

        pf = PreferencesTrue(this)
        env = when (pf.environment) {
            SDKEnvironment.ALPHA.value -> SDKEnvironment.ALPHA
            SDKEnvironment.STAGING.value -> SDKEnvironment.STAGING
            SDKEnvironment.PRODUCTION.value -> SDKEnvironment.PRODUCTION
            else -> SDKEnvironment.STAGING
        }
        Log.d(TAG, "ENV : ${env.value}")

        /*init service login*/
        val directUrl = getString(R.string.re_direct_url)
        /* OLD Version */
//        service = LoginService(this@CoreActivity, listScope, isDev, directUrl)
        /* New Version */
        service = LoginService(this@CoreActivity, listScope, directUrl, env)
        service.setActionListener(this@CoreActivity)
        service.setBuildConfig(BuildConfig.DEBUG)
        service.setClientId(pf.clientId)

        progress.visibility = View.INVISIBLE
    }


    private fun initLayoutListener() {
        sdkVersionTextView.text = getString(R.string.sdk_version, TrueIDSDK.versionName())

        btnClear.setOnClickListener {
            textDisplay.removeAllViewsInLayout()
        }

        btnSwLanguage.text = getString(R.string.str_language_en).toUpperCase()
        btnSwLanguage.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                buttonView.text = getString(R.string.str_language_th).toUpperCase()
                language = getString(R.string.str_language_th)
            } else {
                buttonView.text = getString(R.string.str_language_en).toUpperCase()
                language = getString(R.string.str_language_en)
            }
        }

        btnSwFlow.text =
            getString(R.string.str_flow_title, getString(R.string.str_flow_a).toUpperCase())
        btnSwFlow.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                buttonView.text =
                    getString(R.string.str_flow_title, getString(R.string.str_flow_d).toUpperCase())
                flow = getString(R.string.str_flow_d)
            } else {
                buttonView.text =
                    getString(R.string.str_flow_title, getString(R.string.str_flow_a).toUpperCase())
                flow = getString(R.string.str_flow_a)
            }
        }

        btnSwAuto.text = getString(R.string.str_auto_login, btnSwAuto.isChecked.toString())
        btnSwAuto.setOnCheckedChangeListener { buttonView, isChecked ->
            isAuto = isChecked
            buttonView.text = getString(R.string.str_auto_login, isChecked.toString())
        }

        llScope.setOnClickListener {
            dialogScope()
        }

        btnLogin.setOnClickListener {
            service.login(language, lat, lon, isAuto)
        }

        btnRegister.setOnClickListener {
            service.register(language, lat, lon, flow, isAuto)
        }

        btnForgotPass.setOnClickListener {
            service.recovery(language, lat, lon, isAuto)
        }

        btnVerifyThaiId.setOnClickListener {
            service.verifyThaiId(language)
        }

        btnLogout.setOnClickListener {
            OtherActivity.startActivity(this)
        }

        btnGetInfo.setOnClickListener {
            service.getLoginInfo()
        }

        btnGetMore.setOnClickListener {
            service.getProfileMore()
        }

        btnCallTrueIdApp.setOnClickListener {
            service.callTrueIDApp()
        }

        btnAccessToken.setOnClickListener {
            service.selfVerify(object : VerifyListener {
                override fun onVerifySuccess(accessToken: String?) {
                    if (accessToken == null) return
                    val token = getString(R.string.str_access_token, accessToken)
                    showTextViewJson("onVerifySuccess", "Access Token", accessToken)
                    Log.w(TAG, token)
                }

                override fun onVerifyFailed(errorMsg: String?) {
                    if (errorMsg == null) return
                    showTextViewJson("onVerifyFailed", "error", errorMsg)
                    Log.w(TAG, errorMsg)
                }
            })
        }

        btnRefreshToken.setOnClickListener {
            // parse format
            val refreshToken = getString(R.string.str_refresh_token, service.refreshToken)
            showTextViewJson("RefreshToken", "Refresh Token", service.refreshToken)
            Log.w(TAG, refreshToken)
        }

        btnChangePassword.setOnClickListener {
            service.updatePassword(
                language,
                lat,
                lon,
                object : TrueIDInterface.ChangePasswordListener {
                    override fun onChangePasswordSuccess(accessToken: String) {
                        showTextViewJson("onChangePasswordSuccess", "Access Token", accessToken)
                    }

                    override fun onChangePasswordError(message: String) {
                        showTextViewJson("onChangePasswordError", "error", message)
                    }
                })
        }

        btnFindApp.setOnClickListener {
            service.FindTrueIDApp()
        }

        btnQR.setOnClickListener {
            service.openQRScanner(object : QRScanListener {

                override fun onSuccess(message: String?) {
                    if (message == null) return
                    showTextViewJson("onSuccessQR", "message", message)
                }

                override fun onError(message: String?) {
                    if (message == null) return
                    showTextViewJson("onErrorQR", "error", message)
                }

                override fun onCancel() {
                    showTextViewJson("onCancelQR", "message", "onCancel")
                }
            })
        }
        btnQrGeneral.setOnClickListener {
            service.openQRGeneral(object : TrueIDInterface.QRGeneralListener {
                override fun onSuccess(value: String?) {
                    if (value == null) return
                    showTextViewJson("onSuccess", "value", value)
                }

                override fun onCancel() {
                    showTextViewJson("onCancel", "value", "")
                }
            })
        }

        btnQrData.setOnClickListener {
            DialogQRData(this).setListener(object : DialogQRData.QRDataListener {
                override fun onValues(json: String) {
                    service.sendQRData(json, object : QRScanListener {

                        override fun onSuccess(message: String?) {
                            if (message == null) return
                            showTextViewJson("onSuccessQR", "message", message)
                        }

                        override fun onError(message: String?) {
                            if (message == null) return
                            showTextViewJson("onErrorQR", "error", message)
                        }

                        override fun onCancel() {
                            showTextViewJson("onCancelQR", "message", "onCancel")
                        }
                    })
                }

                override fun onCancel() {
                }
            }).show()
        }


        bindingTmnBtn.setOnClickListener {
            DialogReceiveValue(this).setWalletIdListener(object :
                DialogReceiveValue.WalletIdListener {
                override fun onValues(bindingIds: String) {
                    showProgress()
                    bindingTMN(bindingIds)
                }

                override fun onCancel() {
                    hideProgress()
                }
            }).show()
        }

        force.setOnClickListener {
            DialogReceiveValue(this).setWalletIdListener(object :
                DialogReceiveValue.WalletIdListener {
                override fun onValues(bindingIds: String) {
                    forceRefreshToken(bindingIds)
                }

                override fun onCancel() {
                    hideProgress()
                }
            }).show()
        }
    }

    private fun forceRefreshToken(key: String) {
        service.forceRefresh(key, object : TrueIDInterface.RefreshTokenListener {
            override fun onSuccess(accessToken: String) {
                hideProgress()
                showTextViewJson("ForceSuccess", "accessToken", accessToken)
            }

            override fun onError(message: String) {
                hideProgress()
                showTextViewJson("ForceError", "error", message)
            }

        })

    }

    private fun bindingTMN(bindingIds: String) {
        service.bindingTrueMoney(
            this,
            bindingIds,
            object : TrueIDInterface.BindingTrueMoneyListener {
                override fun onBindingSuccess(accessToken: String?) {
                    Log.d(TAG, "onBindingSuccess : $accessToken")
                    if (accessToken == null) return
                    showTextViewJson("onBindingSuccess", "accessToken", accessToken)
                    hideProgress()

                }

                override fun onBindingError(message: String?) {
                    Log.d(TAG, "onBindingError : $message")
                    if (message == null) return
                    showTextViewJson("onBindingError", "error", message)
                    hideProgress()
                }
            })
    }

    private fun checkAccessToken() {
        val accessToken = service.accessToken
        if (accessToken != null && !accessToken.isEmpty()) {
            layoutSession.visibility = View.GONE
            layoutContent.visibility = View.VISIBLE
        } else {
            layoutSession.visibility = View.VISIBLE
            layoutContent.visibility = View.GONE
        }
    }

    private fun exitSplashScreen() {
        Log.e(TAG, "exitSplashScreen")
        Handler().postDelayed({
            if (Utilities().validateIntentFromOtherApp(this@CoreActivity)) {
                btnLogin.performClick()
            }
        }, 4000)
    }

    private fun dialogScope() {
        val allScope = arrayOf(
            InitialScope.PROFILE,
            InitialScope.MOBILE,
            InitialScope.EMAIL,
            InitialScope.REFERENCE
        )
        val selectedScope = ArrayList<String>()

        val builder = AlertDialog.Builder(this@CoreActivity)
        builder.setTitle("All scope")
        builder.setMultiChoiceItems(allScope, null) { _, which, _ ->
            selectedScope.add(allScope[which])
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialogInterface, _ ->
            dialogInterface.dismiss()
        }

        builder.setPositiveButton(getString(android.R.string.ok)) { dialogInterface, _ ->
            showProgress()

            dialogInterface.dismiss()
            initLoginService(selectedScope)
        }
        builder.create().show()
    }

    private fun getJson(json: String) {
        try {
            val jsonObj = JSONObject(json)
            if (jsonObj.has("sub")) {
                pf.sso = jsonObj.getString("sub")
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun showTextViewJson(valueCallback: String, keyName: String, message: String) {
        Log.d(
            "%%%%%",
            "CoreActivity.showTextViewJson() called with: valueCallback = [$valueCallback], keyName = [$keyName], message = [$message]"
        )
//        val clipBoard = getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
//        clipBoard?.apply {
//            setPrimaryClip(ClipData.newPlainText("copy", message))
//            Toast.makeText(this@CoreActivity, "copy data to clipboard", Toast.LENGTH_SHORT).show()
//        }

        JSONObject().apply {
            put("callback", valueCallback)
            if (message.startsWith("{")) {
                put(keyName, JSONObject(message))
            } else {
                put(keyName, message)
            }
        }.also { json ->
            Log.d("%%%%%", "CoreActivity.showTextViewJson() called with: json = [$json]")
            textDisplay.bindJson(json)
        }
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.INVISIBLE
    }

    private fun setJsonFormat() {
        textDisplay.setTextSize(18F)
        textDisplay.setKeyColor(0xFF9DDFE9.toInt())
        textDisplay.setValueTextColor(0xFFFFCF6C.toInt())
        textDisplay.setValueNumberColor(0xFF9767D4.toInt())
        textDisplay.setValueUrlColor(0xFF32CA2F.toInt())
        textDisplay.setBracesColor(0xFFFFFFFF.toInt())
    }

}
