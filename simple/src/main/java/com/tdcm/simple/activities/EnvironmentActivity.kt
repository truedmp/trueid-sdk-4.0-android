package com.tdcm.simple.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tdcm.simple.R
import com.tdcm.simple.utility.PreferencesTrue
import com.tdcm.truelifelogin.authentication.TrueIDSDK
import com.tdcm.truelifelogin.constants.SDKEnvironment
import kotlinx.android.synthetic.main.activity_environment.*

class EnvironmentActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var pf: PreferencesTrue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_environment)

        sdkVersionTextView.text = getString(R.string.sdk_version, TrueIDSDK.versionName())
        alphaBtn.setOnClickListener(this)
        stagingBtn.setOnClickListener(this)
        productionBtn.setOnClickListener(this)

        pf = PreferencesTrue(this)

        if (!pf.environment.isEmpty()) {
            startIntentSplashScreen()
        }
    }

    override fun onClick(v: View?) {
        val clientId = editTextClientId.text.toString()

        when (v) {
            alphaBtn -> {
                setApplicationPref(SDKEnvironment.ALPHA.value, clientId)
                startIntentSplashScreen()
            }
            stagingBtn -> {
                setApplicationPref(SDKEnvironment.STAGING.value, clientId)
                startIntentSplashScreen()
            }
            productionBtn -> {
                setApplicationPref(SDKEnvironment.PRODUCTION.value, clientId)
                startIntentSplashScreen()
            }
        }
    }

    private fun setApplicationPref(envValue: String, clientId: String) {
        if (!clientId.isEmpty() || !clientId.isBlank()) {
            pf.clientId = clientId
        } else {
            pf.clientId = resources.getString(R.string.trueid_client_id)
        }

        pf.environment = envValue
    }

    private fun startIntentSplashScreen() {
        Intent().apply {
            setClass(this@EnvironmentActivity, SplashScreenActivity::class.java)
            startActivity(this)
            finish()
        }
    }
}
