package com.tdcm.simple.activities

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.tdcm.simple.utility.PreferencesTrue

abstract class BaseActivity : AppCompatActivity() {
    companion object {
        var TAG: String = BaseActivity::class.java.simpleName
    }

    /**
     * @return please return activity class. can use "return this@Activity"
     */
    abstract fun setTag(): AppCompatActivity

    /**
     * @return please return resource layout.
     */
    abstract fun onCreateActivity(): Int

    abstract fun onActivityCreated()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(onCreateActivity())
        TAG = setTag()::class.java.simpleName

        onActivityCreated()

        val pf = PreferencesTrue(this@BaseActivity)
        val appName = applicationInfo.loadLabel(packageManager).toString()

        supportActionBar?.title = "$appName (${pf.clientId})"

        WebView.setWebContentsDebuggingEnabled(true)
    }
}
