package com.tdcm.simple.activities

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tdcm.simple.R
import com.tdcm.simple.utility.PreferencesTrue
import com.tdcm.truelifelogin.authentication.LoginService
import com.tdcm.truelifelogin.constants.SDKEnvironment
import kotlinx.android.synthetic.main.activity_other.*
import java.util.*

class OtherActivity : BaseActivity() {

    companion object {
        fun startActivity(activity: Activity?) {
            activity?.let {
                Intent(activity, OtherActivity::class.java).apply {
                    it.startActivity(this)
                }
            }
        }
    }

    override fun setTag(): AppCompatActivity {
        return this@OtherActivity
    }

    override fun onCreateActivity(): Int {
        return R.layout.activity_other
    }

    override fun onActivityCreated() {
        val pref = PreferencesTrue(this)
        val environment = when (pref.environment) {
            SDKEnvironment.ALPHA.value -> SDKEnvironment.ALPHA
            SDKEnvironment.STAGING.value -> SDKEnvironment.STAGING
            SDKEnvironment.PRODUCTION.value -> SDKEnvironment.PRODUCTION
            else -> SDKEnvironment.STAGING
        }
        Log.d(TAG, "ENVIRONMENT : ${environment.value}")

        btnLogout.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setMessage(getString(R.string.logout_title))
                setCancelable(false)
                setPositiveButton(getString(android.R.string.yes)) { _, _ ->

                    LoginService(this@OtherActivity,
                            Arrays.asList(""),
                            "",
                            environment).logout().also {
                        progressLoading.visibility = View.VISIBLE
                    }

                    Handler().postDelayed({
                        finish()
                    }, 5000)

                }
                setNegativeButton(getString(android.R.string.no)) { dialog, _ ->
                    dialog.dismiss()
                }
                show()
            }
        }
    }
}
