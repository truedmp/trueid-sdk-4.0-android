package com.tdcm.truelifelogin.internal

import android.content.Context
import android.util.Base64
import com.tdcm.truelifelogin.constants.JWTError
import com.tdcm.truelifelogin.httpclient.Validate
import com.tdcm.truelifelogin.utils.APIs
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.UtilsSDK
import org.jose4j.jwk.HttpsJwks
import org.jose4j.jwk.JsonWebKey
import org.jose4j.jwt.NumericDate
import org.jose4j.jwt.consumer.ErrorCodes
import org.jose4j.jwt.consumer.InvalidJwtException
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.concurrent.TimeUnit

/*
 * Created by boyDroids on 24/4/2018 AD.
 * ^.^
 */
internal class JWTUtilsSDK {

    internal companion object {
        private val TAG = JWTUtilsSDK::class.java.simpleName

        @JvmStatic
        fun check(context: Context): String {
            val pf = PreferencesTrueID(context)
            val mAccessToken = pf.access_token_client
            val mJwtUrl = APIs.jwk_url ?: pf.jwtUrl
            val mKid = APIs.kid_android ?: pf.kidAndroid

            if (mJwtUrl.isEmpty() || mKid.isEmpty()) return JWTError.ERROR_NOT_INITIAL
            if (mAccessToken == null || mAccessToken.isEmpty()) return JWTError.ERROR_ACCESS_EMPTY
            if (!Validate.hasInternetAccess(context)) return JWTError.ERROR_INTERNET_ACCESS

            try {
                // First priority check date/time is not over limit difference by server
                val getBest = UtilsSDK.getBestCurrentTime(context)
                if (getBest == 0L) return JWTError.ERROR_BAD_DATETIME

                val json = getBody(mAccessToken)
                val obj = JSONObject(json)
                val aud = obj.get("aud").toString()
                val iss = obj.get("iss").toString()

                val jwtHTTP = HttpsJwks(mJwtUrl)

                var jsonWebKey: JsonWebKey? = null
                for (i in jwtHTTP.jsonWebKeys.indices) {
                    val keyId = jwtHTTP.jsonWebKeys[i].keyId
                    if (keyId.contains(mKid)) {
                        jsonWebKey = jwtHTTP.jsonWebKeys[i]
                    }
                }

                if (jsonWebKey == null) return JWTError.ERROR_JWT_WEB_KEY

                val jwtCustomer = JwtConsumerBuilder()
                        .setRequireExpirationTime()
                        .setAllowedClockSkewInSeconds(3600)
                        .setRequireSubject()
                        .setExpectedIssuer(iss)
                        .setExpectedAudience(aud)
                        .setVerificationKeyResolver(HttpsJwksVerificationKeyResolver(jwtHTTP))
                        .setVerificationKey(jsonWebKey.key)
                        .setRelaxVerificationKeyValidation()
                        .build()

                val jwtClaims = jwtCustomer.processToClaims(mAccessToken)
                val getExp = getExpTime(jwtClaims.expirationTime.value).also {
                    pf.cache_timestamp = it
                }

                val timeExp = NumericDate.fromSeconds(getExp)
                val timeNow = NumericDate.fromSeconds(getBest)
                if (timeExp.isBefore(timeNow)) return JWTError.JWT_TIME_EXPIRE

                return json
            } catch (e: InvalidJwtException) {
                return when {
                    e.hasErrorCode(ErrorCodes.EXPIRED) -> JWTError.JWT_TIME_EXPIRE
                    e.hasErrorCode(ErrorCodes.SIGNATURE_INVALID) -> JWTError.JWT_SIGNATURE_INVALID
                    else -> {
                        e.printStackTrace()
                        JWTError.ERROR_JWT_INVALID
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return JWTError.ERROR_EXCEPTION
            }
        }

        fun getBody(bodyJWT: String): String {
            return try {
                val split2 = bodyJWT.substringAfter(".").substringBefore(".")
                getJson(split2)
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
                JWTError.ERROR_JSON_ENCODE
            }
        }

        private fun getJson(strEncoded: String): String {
            val decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE)
            return String(decodedBytes, charset(KEYS.UTF8))
        }

        fun getExpTime(expTime: Long): Long {
            val tenMinutes = TimeUnit.MINUTES.toSeconds(10)
            return (expTime - tenMinutes)
        }

        @JvmStatic
        fun getErrorCode(error: String): JSONObject {
            return JSONObject().apply {
                put("code", error.substringAfter("#"))
            }
        }
    }
}