package com.tdcm.truelifelogin.internal

import android.content.Context
import com.tdcm.truelifelogin.constants.JWTInvalidCase
import com.tdcm.truelifelogin.tasks.TaskRefreshToken
import com.tdcm.truelifelogin.tasks.TaskValidateJWT
import com.tdcm.truelifelogin.utils.PreferencesTrueID

/*
* Created by yothinindy on 5/10/2018 AD.
* ==============================================
*    Reference by.                             |
*    {                                         |
*    Created by boyDroids on 4/10/2018 AD.     |
*      ^.^                                     |
*    }                                         |
* ==============================================
*/
internal class ValidateJwtToken(val context: Context) {
    interface ValidateJwtTokenListener {
        fun onSuccess(payload: String?)
        fun onError(msgErr: String?)
        fun onRevoke()
    }

    private var callback: ValidateJwtTokenListener? = null

    fun setValidateJwtTokenListener(callback: ValidateJwtTokenListener?): ValidateJwtToken {
        this.callback = callback
        return this@ValidateJwtToken
    }

    fun check() {
        val task = TaskValidateJWT(context)
        task.setValidateJWTListener(object : TaskValidateJWT.ValidateJWTListener {
            override fun onSuccess(jsonProfile: String?) {
                callback?.onSuccess(jsonProfile)
            }

            override fun onError(msgErr: String?, eCase: JWTInvalidCase) {
                when (eCase) {
                    JWTInvalidCase.NONE -> {
                        callback?.onError(msgErr)
                    }
                    JWTInvalidCase.EXPIRE -> {
                        val pref = PreferencesTrueID(context)
                        fetch(pref.refresh_token_client, false)
                    }
                    JWTInvalidCase.REVOKE -> {
                        callback?.onRevoke()
                    }
                }
            }
        })
        task.execute()
    }

    fun fetch(refreshToken: String, isRoot: Boolean) {
        val task = TaskRefreshToken(context, refreshToken, isRoot)
        task.setOnTaskRefreshToken(object : TaskRefreshToken.RefreshListener {
            override fun onSucceed() {
                val payload = JWTUtilsSDK.getBody(PreferencesTrueID(context).access_token_client)
                callback?.onSuccess(payload)
            }

            override fun onFailed(error: String, isRevoke: Boolean) {
                if (isRevoke) {
                    callback?.onRevoke()
                } else {
                    callback?.onError(error)
                }
            }
        })
        task.taskExecute()
    }
}