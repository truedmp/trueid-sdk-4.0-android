package com.tdcm.truelifelogin.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.util.Base64
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.KeyConfigs
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.internal.JWTUtilsSDK
import com.tdcm.truelifelogin.models.Events
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.Inet4Address
import java.net.NetworkInterface
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

internal class UtilsSDK {
    internal companion object {
        private val TAG = UtilsSDK::class.java.simpleName

        @JvmStatic
        fun setPreferenceToken(jsonObj: JSONObject, context: Context) {
            setPreferenceToken(jsonObj, context, false)
        }

        @JvmStatic
        fun setPreferenceToken(jsonObj: JSONObject, context: Context, isSkipFileToken: Boolean) {
            Logcat.d(TAG, "Set preference after create session.")
            try {
                val pf = PreferencesTrueID(context)
                val expire = jsonObj.getInt(KEYS.expires_in)

                var rootAccessToken =
                    if (jsonObj.has(KEYS.root_access_token)) jsonObj.getString(KEYS.root_access_token) else pf.access_token_root
                var rootRefreshToken =
                    if (jsonObj.has(KEYS.root_refresh_token)) jsonObj.getString(KEYS.root_refresh_token) else pf.refresh_token_root
                val accessToken = jsonObj.getString(KEYS.access_token)
                val refreshToken = jsonObj.getString(KEYS.refresh_token)

                val fileToken = FileSDK.getFileToken(context)
                val isFileTokenExists = fileToken.exists()
                val isFileSdkNotEmpty = FileSDK.readFile(context, fileToken).isNotEmpty()
                val isWriteExternal = isWriteExternalPermission(context)
                if (isWriteExternal
                    && isFileTokenExists
                    && isFileSdkNotEmpty
                    && isSkipFileToken.not()
                ) {
                    val fileObj = JSONObject(FileSDK.readFile(context, fileToken))
                    rootAccessToken = fileObj.getString(KEYS.root_access_token)
                    rootRefreshToken = fileObj.getString(KEYS.root_refresh_token)
                }

                pf.access_token_root = rootAccessToken
                pf.refresh_token_root = rootRefreshToken
                pf.access_token_client = accessToken
                pf.refresh_token_client = refreshToken

                pf.token_expires_second = expire

                setExpTime(pf.access_token_client, pf)
                createFileToken(context)
            } catch (e: JSONException) {
                e.printStackTrace()
                Logcat.e(TAG, e.toString())
            }
        }

        private fun setExpTime(accessTokenClient: String, pf: PreferencesTrueID) {
            val json = JWTUtilsSDK.getBody(accessTokenClient)
            val payload = JSONObject(json)
            if (payload.has("exp")) {
                pf.cache_timestamp = JWTUtilsSDK.getExpTime(payload.getLong("exp"))
            }
        }

        private fun createFileToken(context: Context) {
            val pf = PreferencesTrueID(context)
            if (pf.isSelf) return

            val fileToken = FileSDK.getFileToken(context)
            FileSDK.writeFileToken(context, fileToken)
        }

        @JvmStatic
        fun launchApplication(context: Context, packageName: String): Boolean {
            return context.packageManager.getLaunchIntentForPackage(packageName) != null
        }

        @JvmStatic
        fun isTrueIDv4(context: Context): Boolean {
            /* trueID is using sdk v4 since version 2.4.0 */
            if (!launchApplication(context, KEYS.ACCOUNT_TYPE)) return false

            val pInfo = context.packageManager.getPackageInfo(KEYS.ACCOUNT_TYPE, 0)
            val vCode = pInfo.versionCode
            Logcat.w(TAG, "trueId is version $vCode")
            return vCode >= 240000
        }

        @JvmStatic
        fun openApplicationByScheme(activity: Activity, schemeSDK: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("trueid://?referral=$schemeSDK")
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            activity.startActivity(intent)
        }

        @JvmStatic
        fun openToastInternet(activity: Activity?, language: String) {
            val wordTH = "พบปัญหาในการเชื่อมต่ออินเตอร์เน็ต"
            val wordEN = "Something went wrong, Internet connection problem."
            val message = if (language.equals("en", true)) wordEN else wordTH

            activity?.let {
                Toast.makeText(it, message, Toast.LENGTH_LONG).show()
            }
        }


        @JvmStatic
        fun openStoreDialog(activity: Activity?, uriString: String) {
            activity?.let {
                val message = it.getString(R.string.trueid_install_app)

                val builder = AlertDialog.Builder(activity)
                builder.setMessage(message)
                builder.setNegativeButton(it.getString(android.R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                builder.setPositiveButton(it.getString(android.R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                    openIntentActionView(it, uriString)
                }
                builder.show()
            }
        }

        @JvmStatic
        fun openIntentActionView(activity: Activity, uriString: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(uriString)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            activity.startActivity(intent)
        }

        @JvmStatic
        fun getErrorObjectFromCode(context: Context, errorCode: String): JSONObject {
            return getErrorObject(context, getErrorCode(errorCode))
        }

        @JvmStatic
        fun getErrorCode(error: String): JSONObject {
            return JSONObject().apply {
                put("code", error.substringAfter("#"))
            }
        }

        @JvmStatic
        fun getErrorObject(context: Context, dataError: JSONObject): JSONObject {
            try {
                //checking file error in device
                val exists = FileSecurity.isExists(context, Files.Errors)
                if (!exists) {
                    return parsingRawData(dataError)
                }

                //get error code object by json in file error
                val errorCode = dataError.getString(KEYS.code)
                val rawObj = JSONObject(FileSecurity.read(context, Files.Errors)).also {
                    insertSDKError(context, it)
                }
                return if (rawObj.has(errorCode)) {
                    rawObj.getJSONObject(errorCode)
                } else {
                    parsingRawData(dataError)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return parsingRawData(dataError)
            }
        }

        private fun insertSDKError(context: Context, errorObject: JSONObject): JSONObject {
            val raw = FileSDK.readJsonAssets(context, Files.LocalErrors)
            val jArray = JSONArray(raw)
            for (i in 0 until jArray.length()) {
                val jObj = jArray.getJSONObject(i)
                errorObject.put(jObj.getString("service_code"), jObj)
            }
            return errorObject
        }

        private fun parsingRawData(dataError: JSONObject): JSONObject {
            return try {
                JSONObject().apply {
                    put("service", "SSO")
                    put("service_code", dataError.get("code"))
                    put("message_default", dataError.get("message"))
                    put("message_en", dataError.getJSONObject("messages").get("en"))
                    put("message_th", dataError.getJSONObject("messages").get("th"))
                }
            } catch (e: JSONException) {
                Logcat.e(TAG, e.toString())
                JSONObject().apply {
                    put("service", "True SDK")
                    put("service_code", dataError.get("code"))
                    put("message_default", "Something error, please try again.")
                    put("message_en", "Something error, please try again.")
                    put("message_th", "เกิดบางอย่างขึ้นกับระบบกรุณาโหลดหน้านี้ใหม่อีกครั้ง")
                }
            }
        }

        fun getEvents(objAction: Actions, isSuccess: Boolean): Events {
            val category = objAction.category
            val action = objAction.name
            val reason = objAction.arrayList_reason
            val statusCode = objAction.arrayList_status_code

            try {
                if (reason.size != 0 && statusCode.size == 0) {
                    return if (reason.size > 1) {
                        val label =
                            if (isSuccess) objAction.arrayList_reason[0] else objAction.arrayList_reason[1]
                        Events(category, action, label)
                    } else {
                        val label = objAction.arrayList_reason[0]
                        Events(category, action, label)
                    }
                } else if (reason.size == 0 && statusCode.size != 0) {
                    return if (statusCode.size > 1) {
                        val code =
                            if (isSuccess) objAction.arrayList_status_code[0].code else objAction.arrayList_status_code[1].code
                        val msg =
                            if (isSuccess) objAction.arrayList_status_code[0].msg else objAction.arrayList_status_code[1].msg
                        Events(category, action, "$code,$msg")
                    } else {
                        val code = objAction.arrayList_status_code[0].code
                        val msg = objAction.arrayList_status_code[0].msg
                        Events(category, action, "$code,$msg")
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return Events(category, action, "")
        }

        @JvmStatic
        fun clearCookie(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.createInstance(context)
                val cookieManager = CookieManager.getInstance()
                cookieManager.removeAllCookies(null)
            } else {
                val cookieSyncManager = CookieSyncManager.createInstance(context)
                cookieSyncManager.startSync()
                val cookieManager = CookieManager.getInstance()
                cookieManager.removeAllCookie()
                cookieManager.removeSessionCookie()
                cookieSyncManager.stopSync()
                cookieSyncManager.sync()
            }
        }

        @JvmStatic
        fun clearSession(context: Context) {
            clearCookie(context)
            val isWrite = isWriteExternalPermission(context)
            val pf = PreferencesTrueID(context).apply {
                refresh_token_client = ""
                refresh_token_root = ""
                access_token_client = ""
                access_token_root = ""
                root_refresh_token = ""
            }

            FileSecurity.delete(context, Files.Profiles)
            if (!pf.isSelf && isWrite) {
                FileSDK.deleteFile(FileSDK.getFileToken(context))
            }
        }

        private fun isAutoDateTime(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Settings.Global.getInt(context.contentResolver, Settings.Global.AUTO_TIME, 0) == 1
            } else {
                Settings.System.getInt(context.contentResolver, Settings.System.AUTO_TIME, 0) == 1
            }
        }

        /**
         * function is get best time by checked is auto date time first.
         * is auto date time return value timestamp by device settings.
         * and not call service get time from firebase server and check result value again.
         * if result is not value can use time by 0(default)
         * */
        @JvmStatic
        fun getBestCurrentTime(context: Context?): Long {
            if (context == null) return 0
            val maxDiffTime = 6
            val pref = PreferencesTrueID(context)

            return try {
                val localTime = System.currentTimeMillis() / 1000

                return if (isAutoDateTime(context)) {
                    //(1) Is better case. sdk can using time in local device
                    Logcat.d(TAG, "Local Time : $localTime")
                    localTime
                } else if (!pref.alertDateTime) {
                    //(2) Notify first time checking is auto date time is off
                    Logcat.d(TAG, "Alert Time")
                    pref.alertDateTime = true
                    0
                } else {
                    //(3) Is auto date time is off. all time using time from ntp server
                    val ntpTime = SDKHttpServices(context).serverTimeNTP()
                    Logcat.d(TAG, "Server Time : $ntpTime")

                    val diffMilli = (ntpTime - localTime) * 1000
                    val diffHr = TimeUnit.MILLISECONDS.toHours(diffMilli)
                    Logcat.d(TAG, "diffHr : $diffHr")

                    if (diffHr < maxDiffTime) {
                        ntpTime
                    } else {
                        0
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Logcat.e(TAG, "Exception : $e")
                0
            }
        }

        /**
         * function is check write external storage permission open in client application
         * */
        @JvmStatic
        fun isWriteExternalPermission(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                Environment.isExternalStorageManager()
            } else {
                val result: Int = ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
                val result1: Int = ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
            }
        }

        /**
         * function is check camera permission open in client application
         * */
        @JvmStatic
        fun isCameraPermission(context: Context?): Boolean {
            val permission = Manifest.permission.CAMERA
            val res = context?.run {
                checkCallingOrSelfPermission(permission)
            }
            return res == PackageManager.PERMISSION_GRANTED
        }


        @JvmStatic
        fun bindPrefsFireBase(
            context: Context?,
            _jwtUrl: String,
            _kidAndroid: String,
            _apiGetToken: String
        ) {
            context?.let {
                PreferencesTrueID(it).apply {
                    jwtUrl = _jwtUrl
                    kidAndroid = _kidAndroid
                    apiGetTokenUrl = _apiGetToken
                }
            }
        }

        @JvmStatic
        fun getIPAddress(): String {
            val defaultIPAddress = "127.0.0.1"
            try {
                val en: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
                while (en.hasMoreElements()) {

                    val enumIPAdr = en.nextElement().inetAddresses
                    while (enumIPAdr.hasMoreElements()) {
                        val iNetAddress = enumIPAdr.nextElement()

//                        Logcat.w(TAG, "ip1--:$iNetAddress")
//                        Logcat.w(TAG, "ip2--:${iNetAddress.hostAddress}")

                        // for getting IPV4 format
//                        if (!iNetAddress.isLoopbackAddress && InetAddressUtils.isIPv4Address(iNetAddress.hostAddress)) {
                        if (!iNetAddress.isLoopbackAddress && (iNetAddress is Inet4Address)) {
                            return iNetAddress.hostAddress.toString()
                        }
                    }
                }
                return defaultIPAddress
            } catch (e: Exception) {
                e.printStackTrace()
                return defaultIPAddress
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getDeviceId(context: Context?): String {
            return try {
                val androidID =
                    Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)
                if (androidID == null || androidID.isEmpty()) {
                    val buildID = "${Build.BOARD.length % 10}" +
                            "${Build.BRAND.length % 10}" +
                            "${Build.DEVICE.length % 10}" +
                            "${Build.MANUFACTURER.length % 10}" +
                            "${Build.MODEL.length % 10}" +
                            "${Build.PRODUCT.length % 10}"
                    val serial = Build::class.java.getField("SERIAL").get(null).toString()
                    UUID(buildID.hashCode().toLong(), serial.hashCode().toLong()).toString()
                } else {
                    androidID
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Logcat.e(TAG, "Exception : $e")
                ""
            }
        }

        @JvmStatic
        fun encode(str: String): String {
            val encodedBytes = Base64.encode(str.toByteArray(), Base64.URL_SAFE)
            return String(encodedBytes, Charsets.UTF_8)
        }

        fun isEmailValid(email: String): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }

        @JvmStatic
        fun openDateSetting(context: Context, jsonObject: JSONObject) {
            val pref = PreferencesTrueID(context)

            val message: String
            val btnPositive: String

            if (pref.language == KeyConfigs.langEN) {
                message = if (jsonObject.has("message_en")) {
                    jsonObject.getString("message_en")
                } else {
                    context.resources.getString(R.string.date_error_en)
                }
                btnPositive = context.resources.getString(R.string.date_setting_en)
            } else {
                message = if (jsonObject.has("message_th")) {
                    jsonObject.getString("message_th")
                } else {
                    context.resources.getString(R.string.date_error_th)
                }
                btnPositive = context.resources.getString(R.string.date_setting_th)
            }

            AlertDialog.Builder(context).apply {
                setMessage(message)
                setPositiveButton(btnPositive) { dialog, _ ->
                    context.startActivity(Intent(android.provider.Settings.ACTION_DATE_SETTINGS))
                    dialog.dismiss()
                }
                setCancelable(false)
            }.create().show()
        }
    }
}