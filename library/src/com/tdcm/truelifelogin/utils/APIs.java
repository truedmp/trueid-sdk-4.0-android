package com.tdcm.truelifelogin.utils;

public class APIs {

    public static String ACCOUNT_TYPE = "com.tdcm.trueidapp";
    public static String web_signin_url;
    public static String web_signup_url;
    public static String web_verify_thaiid_url;
    public static String web_recovery_url;
    public static String web_update_password;
    public static String jwk_url;
    public static String api_get_user_info_url;
    public static String api_get_token_url;
    public static String policy_th_url;
    public static String policy_en_url;
    public static String term_th_url;
    public static String term_en_url;
    public static String revoke_url;
    public static String avatar_url;
    public static int timeout;
    public static int time_refresh_token;
    public static String api_get_root_token_credential;
    public static String kid_android;
    public static String default_scope;
    public static String api_server_time;
    public static String api_auth_qrcode;
    public static String api_server_ntp;

    public static String url_google_204 = "https://clients3.google.com/generate_204";
    public static String external_ntp_google = "time.google.com";
    public static String tmn_binding_url;
    public static String sc;


}