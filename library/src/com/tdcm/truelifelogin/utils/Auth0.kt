package com.tdcm.truelifelogin.utils

import android.util.Base64
import org.json.JSONObject
import java.security.MessageDigest
import java.security.SecureRandom


internal object Auth0 {

    fun randomState(): String {
        val random = (0..999).shuffled().first()
        return random.toString()
    }

    fun createCodeVerifier(): String {
        val sr = SecureRandom()
        val code = ByteArray(32)
        sr.nextBytes(code)
        val verifier = Base64.encodeToString(code, Base64.URL_SAFE or Base64.NO_WRAP or Base64.NO_PADDING)

        return if (!verifier.isNullOrEmpty()) verifier else ""
    }

    fun createCodeChallenge(verifier: String): String {
        val bytes = verifier.toByteArray(Charsets.US_ASCII)
        val md = MessageDigest.getInstance("SHA-256")
        md.update(bytes, 0, bytes.size)
        val digest = md.digest()
        return Base64.encodeToString(digest, Base64.URL_SAFE or Base64.NO_WRAP or Base64.NO_PADDING)
    }

    fun snipVerifierObject(fullURL: String): JSONObject {
        val codeVerifier = fullURL.substringAfter("{").substringBefore("}")
        val stage = fullURL.substringAfter("${KEYS.AUTH0_STATE}=").substringBefore("&")
        val authURL = fullURL.replace("{$codeVerifier}", "")

        return JSONObject().apply {
            put(KEYS.AUTH0_STATE, stage)
            put(KEYS.AUTH0_CODE_VERIFIER, codeVerifier)
            put(KEYS.AUTH0_URL, authURL)
        }
    }
}