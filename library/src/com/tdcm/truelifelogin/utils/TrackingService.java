package com.tdcm.truelifelogin.utils;

import android.content.Context;

import com.tdcm.truelifelogin.models.Screens;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * Created by Pratanporn on 10/5/2017 AD.
 * Updated by boyDroids on 30/3/2018 AD.
 */

public class TrackingService {
    public static String TAG = TrackingService.class.getSimpleName();

    private Context context;
    private JSONObject objEvent;
    private JSONObject objScr;
    private String strCategory;

    public TrackingService(Context context) {
        this(context, null);
    }

    public TrackingService(Context context, String url) {
        this.context = context;

        if (url != null) {
            /* fetch GA */
            AAAConfigurations.loadGA(context);
        }

        boolean exists = FileSecurity.INSTANCE.isExists(context, Files.Analytics);

        if (exists) {
            String response = FileSecurity.INSTANCE.read(context, Files.Analytics);
            try {
                objEvent = new JSONObject(response)
                        .getJSONObject(KeysTracking.Events)
                        .getJSONObject(KeysTracking.Account)
                        .getJSONObject(KeysTracking.Action);

                objScr = new JSONObject(response)
                        .getJSONObject(KeysTracking.Screens);

                strCategory = new JSONObject(response)
                        .getJSONObject(KeysTracking.Events)
                        .getJSONObject(KeysTracking.Account).getString(KeysTracking.Category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Screens getScreens(String key) {
        Screens screens = null;
        try {
            screens = new Screens(objScr.getString(key));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return screens;
    }

    ////////////////////////////////////////EVENT///////////////////////////////////////////////////

    public Actions getEvents(String key) {
        Actions action = new Actions();
        if (objEvent != null) {
            if (objEvent.has(key)) {
                try {
                    JSONObject jsonObject1 = objEvent.getJSONObject(key);
                    ArrayList<String> arrayList_reason = new ArrayList<>();
                    ArrayList<StatusCodes> arrayList_status_code = new ArrayList<>();

                    if (jsonObject1.getString(KeysTracking.reason).length() > 0) {
                        JSONArray jsonArray_reason = jsonObject1.getJSONArray(KeysTracking.reason);
                        for (int i = 0; i < jsonArray_reason.length(); i++) {
                            arrayList_reason.add(jsonArray_reason.getString(i));
                        }
                    }

                    if (jsonObject1.getString(KeysTracking.status_code).length() > 0) {
                        JSONArray jsonArray_status_code = jsonObject1.getJSONArray(KeysTracking.status_code);
                        JSONObject jsonObject_status_code;
                        for (int i = 0; i < jsonArray_status_code.length(); i++) {
                            jsonObject_status_code = jsonArray_status_code.getJSONObject(i);
                            StatusCodes statusCode = new StatusCodes();
                            statusCode.setCode(jsonObject_status_code.getString(KeysTracking.code));
                            statusCode.setMsg(jsonObject_status_code.getString(KeysTracking.msg));
                            arrayList_status_code.add(statusCode);
                        }
                    }

                    action.setCategory(strCategory);
                    action.setArrayList_status_code(arrayList_status_code);
                    action.setArrayList_reason(arrayList_reason);
                    action.setName(jsonObject1.getString(KeysTracking.name));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return action;
    }

    public String TrackingScreensURL(String url) {
        String scr = "";
        url = url.toLowerCase();
        if (url.contains(KeysTracking.path_signup_otp)) {
            scr = KeysTracking.Screens_SIGNUP_OTP;
        } else if (url.contains(KeysTracking.path_signin)) {
            scr = KeysTracking.Screens_LOGIN;
        } else if (url.contains(KeysTracking.path_signup_success)) {
            scr = KeysTracking.Screens_SIGNUP_SUCCESS;
        } else if (url.contains(KeysTracking.path_signup_already)) {
            scr = KeysTracking.Screens_SIGN_UP_ALREADY;
        } else if (url.contains(KeysTracking.path_signup) &&
                url.contains(KeysTracking.path_flowA)) {
            scr = KeysTracking.Screens_SIGNUP_FLOW_A;
        } else if (url.contains(KeysTracking.path_signup) &&
                url.contains(KeysTracking.path_flowD)) {
            scr = KeysTracking.Screens_SIGNUP_FLOW_D;
        } else if (url.contains(KeysTracking.path_forgot)) {
            scr = KeysTracking.Screens_FORGOT;
        } else if (url.contains(KeysTracking.path_forgot_otp)) {
            scr = KeysTracking.Screens_FORGOT_OTP;
        } else if (url.contains(KeysTracking.path_forgot_verify_code)) {
            scr = KeysTracking.Screens_FORGOT_EMAIL_CODE;
        } else if (url.contains(KeysTracking.path_forgot_email)) {
            scr = KeysTracking.Screens_FORGOT_EMAIL_CODE;
        } else if (url.contains(KeysTracking.path_forgot_newpass)) {
            scr = KeysTracking.Screens_FORGOT_RESET_PASS;
        } else if (url.contains(KeysTracking.path_forgot_success)) {
            scr = KeysTracking.Screens_FORGOT_SUCCESS;
        } else if (url.contains(KeysTracking.path_verify)) {
            scr = KeysTracking.Screens_VERIFY;
        } else if (url.contains(KeysTracking.path_verify_identity)) {
            scr = KeysTracking.Screens_FORGOT_VERIFY;
        } else if (url.contains(KeysTracking.path_verify_service)) {
            scr = KeysTracking.Screens_VERIFY_SERVICE;
        } else if (url.contains(KeysTracking.path_verify_otp)) {
            scr = KeysTracking.Screens_VERIFY_OTP;
        } else if (url.contains(KeysTracking.path_verify_success)) {
            scr = KeysTracking.Screens_VERIFY_SUCCESS;
        } else if (url.contains(KeysTracking.path_verify_already)) {
            scr = KeysTracking.Screens_VERIFY_ALREADY;
        } else if (url.contains(KeysTracking.path_forgot_verify)) {
            scr = KeysTracking.Screens_VERIFY_ALREADY;
        } else if (url.contains(KeysTracking.path_change_password)) {
            scr = KeysTracking.Screens_CHANGE_NEW_PASSWORD;
        } else if (url.contains(KeysTracking.path_change_password_success)) {
            scr = KeysTracking.Screens_CHANGE_NEW_PASSWORD_SUCCESS;
        } else {
            Logcat.d(TAG, "NO TRACKING SCREEN");
        }
        return scr;

    }

}
