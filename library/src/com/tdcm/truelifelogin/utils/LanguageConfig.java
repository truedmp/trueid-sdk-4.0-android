package com.tdcm.truelifelogin.utils;

import android.content.Context;

import com.tdcm.truelifelogin.R;


/*
 * Created by Pratanporn on 8/24/2017 AD.
 */

public class LanguageConfig {

    Context context;

    public String error_msg;
    public String login;
    public String user;
    public String user_hint;
    public String password;
    public String password_hint;
    public String error_user;
    public String error_pass;
    public String error_wrong_user;
    public String error_server_wrong_id_1;
    public String error_server_wrong_id_2;
    public String error_server_wrong_id_3;
    public String error_server_wrong_id_4;

    public LanguageConfig(Context context, String language) {
        this.context = context;
        setLanguage(language);
    }

    public void setLanguage(String language) {
        if (language == KEYS.th) {
            error_msg = getString(R.string.error_msg_th);
            login = getString(R.string.login_th);
            user = getString(R.string.user_th);
            user_hint = getString(R.string.user_hint_th);
            password = getString(R.string.password_th);
            password_hint = getString(R.string.password_hint_th);
            error_user = getString(R.string.error_user_th);
            error_pass = getString(R.string.error_pass_th);
            error_wrong_user = getString(R.string.error_wrong_user_th);
            error_server_wrong_id_1 = getString(R.string.error_server_wrong_id_1_th);
            error_server_wrong_id_2 = getString(R.string.error_server_wrong_id_2_th);
            error_server_wrong_id_3 = getString(R.string.error_server_wrong_id_3_th);
            error_server_wrong_id_4 = getString(R.string.error_server_wrong_id_4_th);
        } else {
            error_msg = getString(R.string.error_msg);
            login = getString(R.string.login);
            user = getString(R.string.user);
            user_hint = getString(R.string.user_hint);
            password = getString(R.string.password);
            password_hint = getString(R.string.password_hint);
            error_user = getString(R.string.error_user);
            error_pass = getString(R.string.error_pass);
            error_wrong_user = getString(R.string.error_wrong_user);
            error_server_wrong_id_1 = getString(R.string.error_server_wrong_id_1);
            error_server_wrong_id_2 = getString(R.string.error_server_wrong_id_2);
            error_server_wrong_id_3 = getString(R.string.error_server_wrong_id_3);
            error_server_wrong_id_4 = getString(R.string.error_server_wrong_id_4);
        }
    }

    private String getString(int id) {
//        Log.d("getLanguageString",""+id);
        return context.getResources().getString(id);
    }


}
