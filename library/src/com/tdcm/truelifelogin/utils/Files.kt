package com.tdcm.truelifelogin.utils

/*
 * Created by boyDroids on 2019-05-29.
 * ^.^
 */
internal object Files {
    const val Configs = "configs"
    const val Errors = "errors"
    const val Analytics = "analytics"
    const val Profiles = "profile"
    const val LocalErrors = "local_error"
}