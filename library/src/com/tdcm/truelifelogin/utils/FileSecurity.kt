package com.tdcm.truelifelogin.utils

import android.content.Context
import java.io.*

object FileSecurity {
    private val TAG = FileSecurity::class.java.simpleName

    fun write(context: Context, fileName: String, json: String?) {
        try {
            json?.let {
                val file = File(getJsonPath(context, fileName))
                val bos = BufferedOutputStream(FileOutputStream(file, false))
                val br = Security(context).encryptLibs(json).toByteArray()
                bos.write(br)
                bos.flush()
                bos.close()
                Logcat.d(TAG, "Created $fileName file success.")
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun read(context: Context, fileName: String): String {
        return try {
            val ips = File(getJsonPath(context, fileName)).inputStream()
            val data = ips.bufferedReader().use { it.readText() }
            Security(context).decryptLibs(data)
        } catch (e: FileNotFoundException) {
            Logcat.e(TAG, "read FileNotFoundException : $e")
            readAssets(context, fileName)
        } catch (e: IOException) {
            Logcat.e(TAG, "read IOException : $e")
            readAssets(context, fileName)
        } catch (e: Exception) {
            Logcat.e(TAG, "read Exception : $e")
            readAssets(context, fileName)
        }
    }

    fun delete(context: Context, fileName: String) {
        val path = context.filesDir.absolutePath + "/" + fileName
        File(path).delete()
        Logcat.d(TAG, "deleted $fileName file success")
    }

    fun readAssets(context: Context, fileName: String): String {
        val ips = context.assets.open(fileName)
        val data = ips.bufferedReader().use { it.readText() }
        return Security(context).decryptLibs(data)
    }

    fun isExists(context: Context, fileName: String?): Boolean {
        return File(getJsonPath(context, fileName)).exists()
    }

    private fun getJsonPath(context: Context, fileName: String?): String {
        return context.filesDir.absolutePath + "/" + fileName
    }
}