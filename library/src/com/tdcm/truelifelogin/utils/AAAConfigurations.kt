package com.tdcm.truelifelogin.utils

import android.content.Context
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import org.json.JSONException
import org.json.JSONObject
import javax.net.ssl.HttpsURLConnection

internal class AAAConfigurations {

    companion object {
        private val TAG = AAAConfigurations::class.java.simpleName

        @JvmStatic
        fun loadUrlConfig(context: Context, pf: PreferencesTrueID) {
            val environment = pf.environment
            val urlConfig = KeysTracking.config_url
            val jsonObject = SDKHttpServices(context).loadFireBase(urlConfig)

            try {
                if (jsonObject.getInt(KEYS.code) == HttpsURLConnection.HTTP_OK) {
                    /* replace file to device after load config from fireBase */
                    val data = jsonObject.getJSONObject(KEYS.data).toString()
                    jsonBinding(context, data, environment)
                    FileSecurity.write(context, Files.Configs, data)
                } else {
                    val exists = FileSecurity.isExists(context, Files.Configs)
                    if (exists) {
                        val resFile = FileSecurity.read(context, Files.Configs)
                        jsonBinding(context, resFile, environment)
                    } else {
                        Logcat.w(TAG, "(Configs) Can't load from server, auto using embed file.")

                        val embedFile = FileSecurity.readAssets(context, Files.Configs)
                        jsonBinding(context, embedFile, environment)
                        FileSecurity.write(context, Files.Configs, embedFile)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()

                val embedFile = FileSecurity.readAssets(context, Files.Configs)
                jsonBinding(context, embedFile, environment)
                FileSecurity.write(context, Files.Configs, embedFile)
            }
        }

        @JvmStatic
        fun loadError(context: Context) {
            val urlError = KeysTracking.error_code_url
            val jsonObject = SDKHttpServices(context).loadFireBase(urlError)

            try {
                if (jsonObject.getInt(KEYS.code) == HttpsURLConnection.HTTP_OK) {
                    /* replace file to device after load config from fireBase */
                    val data = jsonObject.getJSONObject(KEYS.data).toString()
                    FileSecurity.write(context, Files.Errors, data)
                } else {
                    val exists = FileSecurity.isExists(context, Files.Errors)
                    if (!exists) {
                        Logcat.w(TAG, "(Errors) Can't load from server, auto using embed file.")

                        val embedFile = FileSecurity.readAssets(context, Files.Errors)
                        FileSecurity.write(context, Files.Errors, embedFile)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()

                val embedFile = FileSecurity.readAssets(context, Files.Errors)
                FileSecurity.write(context, Files.Errors, embedFile)
            }
        }

        @JvmStatic
        fun loadGA(context: Context) {
            val urlGA = KeysTracking.ga_tracking_url
            val response = SDKHttpServices(context).loadFireBase(urlGA)

            try {
                if (response.getInt(KEYS.code) == HttpsURLConnection.HTTP_OK) {
                    /* replace file to device after load config from fireBase */
                    val data = response.getJSONObject(KEYS.data).toString()
                    FileSecurity.write(context, Files.Analytics, data)
                } else {
                    val exists = FileSecurity.isExists(context, Files.Analytics)
                    if (!exists) {
                        Logcat.w(TAG, "(GAs) Can't load from server, auto using embed file.")

                        val embedFile = FileSecurity.readAssets(context, Files.Analytics)
                        FileSecurity.write(context, Files.Analytics, embedFile)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        fun loadBundle(context: Context, pf: PreferencesTrueID) {
            // Read default config file from
            val json = FileSecurity.readAssets(context, Files.Configs)
            jsonBinding(context, json, pf.environment, "Bundle")
        }

        private fun jsonBinding(context: Context, strJson: String, environment: String) {
            jsonBinding(context, strJson, environment, "Fetch")
        }

        private fun jsonBinding(context: Context, strJson: String, environment: String, from: String) {
            try {
                Logcat.w(TAG, "($from) Load URLs configs in environment : $environment")
                val jsonObject = JSONObject(strJson)

                APIs.sc = jsonObject.getJSONObject("sdk").getString("sc")
                APIs.web_signin_url = jsonObject.getJSONObject(environment).getString("web_signin_url")
                APIs.web_signup_url = jsonObject.getJSONObject(environment).getString("web_signup_url")
                APIs.web_verify_thaiid_url = jsonObject.getJSONObject(environment).getString("web_verify_thaiid_url")
                APIs.jwk_url = jsonObject.getJSONObject(environment).getString("jwk_url")
                APIs.web_recovery_url = jsonObject.getJSONObject(environment).getString("web_recovery_url")
                APIs.api_get_token_url = jsonObject.getJSONObject(environment).getString("api_get_token_url")
                APIs.api_get_user_info_url = jsonObject.getJSONObject(environment).getString("api_get_user_info_url")
                APIs.policy_th_url = jsonObject.getJSONObject(environment).getString("policy_th_url")
                APIs.policy_en_url = jsonObject.getJSONObject(environment).getString("policy_en_url")
                APIs.term_th_url = jsonObject.getJSONObject(environment).getString("term_th_url")
                APIs.term_en_url = jsonObject.getJSONObject(environment).getString("term_en_url")
                APIs.revoke_url = jsonObject.getJSONObject(environment).getString("api_revoke_url")
                APIs.timeout = jsonObject.getJSONObject(environment).getInt("timeout")
                APIs.time_refresh_token = jsonObject.getJSONObject(environment).getInt("time_refresh_token")
                APIs.api_get_root_token_credential = jsonObject.getJSONObject(environment).getString("api_get_root_token_credential")
                APIs.kid_android = jsonObject.getJSONObject(environment).getString("kid_android")
                APIs.default_scope = jsonObject.getJSONObject(environment).getString("default_scope")
                APIs.api_auth_qrcode = jsonObject.getJSONObject(environment).getString("api_auth_qrcode")
                APIs.api_server_time = jsonObject.getJSONObject(environment).getString("server_datetime")
                APIs.tmn_binding_url = jsonObject.getJSONObject(environment).getString("tmn_binding_url")
                APIs.api_server_ntp = jsonObject.getJSONObject(environment).getString("ntp_android")
                APIs.web_update_password = jsonObject.getJSONObject(environment).getString("web_update_password")

                UtilsSDK.bindPrefsFireBase(context, APIs.jwk_url, APIs.kid_android, APIs.api_get_token_url)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }
}