package com.tdcm.truelifelogin.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.tdcm.truelifelogin.constants.SDKEnvironment;

/*
 * Created by Pratanporn on 11/4/2017 AD.
 * Updated by boyDroids on 30/3/2018 AD.
 */

public class PreferencesTrueID {

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private String scope = "scope";
    private String reDirectUrl = "reDirectUrl";
    private String isDev = "isDev";
    private String isSelf = "self_login";
    private String version = "version";
    private String android_id = "android_id";
    private String android_model = "android_model";
    private String client_id = "client_id";
    private String client_flow = "client_flow";
    private String client_scheme_for_sdk = "client_scheme_for_sdk";
    private String language = "language";
    private String location = "location";
    private String isAutoLogin = "isAutoLogin";
    private String isRunService = "isRunService";
    private String ip_address = "ip_address";
    private String package_name = "package_name";
    private String client_class = "client_class";
    private String access_token_root = "access_token_root";
    private String access_token_client = "access_token_client";
    private String refresh_token_root = "refresh_token_root";
    private String refresh_token_client = "refresh_token_client";
    private String token_expires_second = "token_expires_second";
    private String root_refresh_token = "root_refresh_token";
    private String cache_timestamp = "cache_timestamp";
    private String root_token_credential = "root_token_credential";
    private String isLogin = "isLogin";
    private String environment = "environment";
    private String permissionCamera = "permission_camera";
    private String isAlertDateTime = "alert_date_time";

    private String isFirstInstall = "isFirstInstall";
    private String installDateTime = "installDateTime";

    private String referral_app = "referral_app";
    private String from_true_app = "from_true_app";

    /**
     * For AuthTokenManager
     * */
    private String jwt_url = "jwt_url";
    private String kid_android = "kid_android";
    private String api_get_token_url = "api_get_token_url";

    public PreferencesTrueID(Context context) {
        sp = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void clearData() {
        editor.clear().commit();
    }

    /**
     * =============================================================================================
     * SET
     * =============================================================================================
     */

    public void setScope(String _scope) {
        editor.putString(scope, _scope).commit();
    }

    public void setReDirectUrl(String _reDirectUrl) {
        editor.putString(reDirectUrl, _reDirectUrl).commit();
    }

    public void setIsDev(Boolean _isDev) {
        editor.putBoolean(isDev, _isDev).commit();
    }

    public void setIsSelf(Boolean _isSelf) {
        editor.putBoolean(isSelf, _isSelf).commit();
    }

    public void setVersion(String _version) {
        editor.putString(version, _version).commit();
    }

    public void setAndroid_id(String _android_id) {
        editor.putString(android_id, _android_id).commit();
    }

    public void setAndroid_model(String _android_model) {
        editor.putString(android_model, _android_model).commit();
    }

    public void setClient_id(String _client_id) {
        editor.putString(client_id, _client_id).commit();
    }

    public void setClient_flow(String _client_flow) {
        editor.putString(client_flow, _client_flow).commit();
    }

    public void setClient_scheme_for_sdk(String _client_scheme_for_sdk) {
        editor.putString(client_scheme_for_sdk, _client_scheme_for_sdk).commit();
    }

    public void setLanguage(String _language) {
        editor.putString(language, _language).commit();
    }

    public void setLocation(String _location) {
        editor.putString(location, _location).commit();
    }

    public void setIsAutoLogin(Boolean _isAutoLogin) {
        editor.putBoolean(isAutoLogin, _isAutoLogin).commit();
    }

    public void setIsRunService(Boolean _isRunService) {
        editor.putBoolean(isRunService, _isRunService).commit();
    }

    public void setIp_address(String _ip_address) {
        editor.putString(ip_address, _ip_address).commit();
    }

    public void setPackage_name(String _package_name) {
        editor.putString(package_name, _package_name).commit();
    }

    public void setClient_class(String _client_class) {
        editor.putString(client_class, _client_class).commit();
    }

    public void setAccess_token_root(String _access_token_root) {
        editor.putString(access_token_root, _access_token_root).commit();
    }

    public void setAccess_token_client(String _access_token_client) {
        editor.putString(access_token_client, _access_token_client).commit();
    }

    public void setRefresh_token_root(String _refresh_token_root) {
        editor.putString(refresh_token_root, _refresh_token_root).commit();
    }

    public void setRefresh_token_client(String _refresh_token_client) {
        editor.putString(refresh_token_client, _refresh_token_client).commit();
    }

    public void setToken_expires_second(int _token_expires_second) {
        editor.putInt(token_expires_second, _token_expires_second).commit();
    }

    public void setCache_timestamp(long _cache_timestamp) {
        editor.putLong(cache_timestamp, _cache_timestamp).commit();
    }

    public void setRoot_refresh_token(String _root_refresh_token) {
        editor.putString(root_refresh_token, _root_refresh_token).commit();
    }

    public void setRoot_token_credential(String _root_token_credential) {
        editor.putString(root_token_credential, _root_token_credential).commit();
    }

    public void setIsLogin(boolean _isLogin) {
        editor.putBoolean(isLogin, _isLogin).commit();
    }

    public void setIsFirstInstall(boolean _isFirstInstall) {
        editor.putBoolean(isFirstInstall, _isFirstInstall).commit();
    }

    public void setInstallDateTime(String _installDateTime) {
        editor.putString(installDateTime, _installDateTime).commit();
    }

    public void setReferral_app(String _referral_app) {
        editor.putString(referral_app, _referral_app).commit();
    }

    public void setFrom_true_app(String _from_true_app) {
        editor.putString(from_true_app, _from_true_app).commit();
    }

    public void setEnvironment(String _environment) {
        editor.putString(environment, _environment).commit();
    }

    public void setCameraIgnore(Boolean _permission) {
        editor.putBoolean(permissionCamera, _permission).commit();
    }
    public void setAlertDateTime(boolean _isAlert) {
        editor.putBoolean(isAlertDateTime, _isAlert).commit();
    }

    /**
     * =============================================================================================
     * GET
     * =============================================================================================
     */

    public String getScope() {
        String strScope = sp.getString(scope, "");
        if (strScope.isEmpty()) {
            return APIs.default_scope;
        }
        return strScope;
    }

    public String getReDirectUrl() {
        String directUrl = sp.getString(reDirectUrl, "");
        if (directUrl.isEmpty() || directUrl.equals(" ")) {
            return "https://home.trueid.net";
        }
        return directUrl;
    }

    public boolean getIsDev() {
        return sp.getBoolean(isDev, true);
    }

    public boolean getIsSelf() {
        return sp.getBoolean(isSelf, false);
    }

    public String getVersion() {
        return sp.getString(version, "");
    }

    public String getAndroid_id() {
        return sp.getString(android_id, "UNKNOWN");
    }

    public String getAndroid_model() {
        return sp.getString(android_model, "UNKNOWN");
    }

    public String getClient_id() {
        return sp.getString(client_id, "");
    }

    public String getClient_flow() {
        return sp.getString(client_flow, "");
    }

    public String getClient_scheme_for_sdk() {
        return sp.getString(client_scheme_for_sdk, "");
    }

    public String getLanguage() {
        return sp.getString(language, "");
    }

    public String getLocation() {
        return sp.getString(location, "0.0,0.0");
    }

    public Boolean getIsAutoLogin() {
        return sp.getBoolean(isAutoLogin, false);
    }

    public Boolean getIsRunService() {
//        force return service is false (disable)
//        return sp.getBoolean(isRunService, false);
        return false;
    }

    public String getIp_address() {
       return sp.getString(ip_address, "0.0.0.0");
    }

    public String getPackage_name() {
        return sp.getString(package_name, "");
    }

    public String getClient_class() {
        return sp.getString(client_class, "");
    }

    public String getAccess_token_root() {
        return sp.getString(access_token_root, "");
    }

    public String getAccess_token_client() {
        return sp.getString(access_token_client, "");
    }

    public String getRefresh_token_root() {
        return sp.getString(refresh_token_root, "");
    }

    public String getRefresh_token_client() {
        return sp.getString(refresh_token_client, "");
    }

    public int getToken_expires_second() {
        return sp.getInt(token_expires_second, 1);
    }

    public long getCache_timestamp() {
        return sp.getLong(cache_timestamp, 0);
    }

    public String getRoot_refresh_token() {
        return sp.getString(root_refresh_token, "");
    }

    public String getRoot_token_credential() {
        return sp.getString(root_token_credential, "");
    }

    public boolean getIsLogin() {
        return sp.getBoolean(isLogin,false);
    }

    public boolean getIsFirstInstall() {
        return sp.getBoolean(isFirstInstall,false);
    }

    public String getInstallDateTime() {
        return sp.getString(installDateTime, "");
    }

    public String getReferral_app() {
        return sp.getString(referral_app, "");
    }

    public String getFrom_true_app() {
        return sp.getString(from_true_app, "");
    }

    public String getEnvironment() {
        return sp.getString(environment, SDKEnvironment.STAGING.getValue());
    }

    public boolean isCameraIgnore() {
        return sp.getBoolean(permissionCamera, false);
    }
    public boolean getAlertDateTime() {
        return sp.getBoolean(isAlertDateTime, false);
    }

    /**
     * =============================================================================================
     * For AuthTokenManager
     * =============================================================================================
     */

    public String getJWTUrl() {
        return sp.getString(jwt_url, "");
    }

    public void setJWTUrl(String JWTUrl) {
        editor.putString(jwt_url, JWTUrl).commit();
    }

    public String getKIDAndroid() {
        return sp.getString(kid_android, "");
    }

    public void setKIDAndroid(String KIDAndroid) {
        editor.putString(kid_android, KIDAndroid).commit();
    }

    public String getApiGetTokenUrl() {
        return sp.getString(api_get_token_url, "");

    }

    public void setApiGetTokenUrl(String apiGetTokenUrl) {
        editor.putString(api_get_token_url, apiGetTokenUrl).commit();
    }
}
