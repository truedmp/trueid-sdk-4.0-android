package com.tdcm.truelifelogin.utils;

/*
 * Created by Pratanporn on 10/4/2017 AD.
 */

public interface KeysTracking {

    String config_url = "https://aaasdk.dmpcdn.com/Q1myl4xIXa.txt";
    String error_code_url = "https://aaasdk.dmpcdn.com/AAA_ERROR_CODE.json";
    String ga_tracking_url = "https://aaasdk.dmpcdn.com/AAA_GA.json";

    String Screens = "Screens";
    String Screens_FORGOT = "FORGOT";
    String Screens_FORGOT_EMAIL_CODE = "FORGOT_EMAIL_CODE";
    String Screens_FORGOT_OTP = "FORGOT_OTP";
    String Screens_FORGOT_RESET_PASS = "FORGOT_RESET_PASS";
    String Screens_FORGOT_SUCCESS = "FORGOT_SUCCESS";
    String Screens_LOGIN = "LOGIN";
    String Screens_LOGIN_NATIVE = "LOGIN_NATIVE";
    String Screens_SIGNUP = "SIGNUP";
    String Screens_SIGNUP_OTP = "SIGNUP_OTP";
    String Screens_SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
    String Screens_VERIFY = "VERIFY";
    String Screens_VERIFY_ALREADY = "VERIFY_ALREADY";
    String Screens_VERIFY_OTP = "VERIFY_OTP";
    String Screens_VERIFY_SERVICE = "VERIFY_SERVICE";
    String Screens_VERIFY_SUCCESS = "VERIFY_SUCCESS";
    String Screens_FORGOT_VERIFY = "FORGOT_VERIFY";
    String Screens_SIGN_UP_ALREADY = "SIGN_UP_ALREADY";
    String Screens_SIGNUP_FLOW_A = "SIGNUP_FLOW_A";
    String Screens_SIGNUP_FLOW_D = "SIGNUP_FLOW_D";
    String Screens_ERRORHANDLE = "errorhandle";
    String Screens_QR_LOGIN_SCAN = "QR_LOGIN_SCAN";
    String Screens_QR_LOGIN_CONFIRM = "QR_LOGIN_CONFIRM";
    String Screens_CHANGE_NEW_PASSWORD = "CHANGE_NEW_PASSWORD";
    String Screens_CHANGE_NEW_PASSWORD_SUCCESS = "CHANGE_NEW_PASSWORD_SUCCESS";

    String Account = "Account";
    String Action = "Action";
    String Category = "Category";
    String reason = "reason";
    String name = "name";
    String status_code = "status_code";

    String Events = "Events";
    String Events_Forgot_Cancel = "Forgot_Cancel";
    String Events_Forgot_End = "Forgot_End";
    String Events_Forgot_Start = "Forgot_Start";
    String Events_Get_Profile = "Get_Profile";
    String Events_Get_Profile_More = "Get_Profile_More";
    String Events_Login_Cancel = "Login_Cancel";
    String Events_Login_End = "Login_End";
    String Events_Login_Native_Cancel = "Login_Native_Cancel";
    String Events_Login_Native_End = "Login_Native_End";
    String Events_Login_Native_Start = "Login_Native_Start";
    String Events_Login_Start = "Login_Start";
    String Events_Logout = "Logout";
    String Events_Refresh_Token = "Refresh_Token";
    String Events_Signup_Cancel = "Signup_Cancel";
    String Events_Signup_End = "Signup_End";
    String Events_Signup_Start = "Signup_Start";
    String Events_Update_Profile = "Update_Profile";
    String Events_Verify_Cancel = "Verify_Cancel";
    String Events_Verify_End = "Verify_End";
    String Events_Verify_Start = "Verify_Start";
    String Events_QR_Login_Start = "QR_Login_Start";
    String Events_QR_Login_Cancel = "QR_Login_Cancel";
    String Events_QR_Login_End = "QR_Login_End";
    String Events_Change_New_Password_Start = "Change_New_Password_Start";
    String Events_Change_New_Password_Cancel = "Change_New_Password_Cancel";
    String Events_Change_New_Password_End = "Change_New_Password_End";

    String code = "code";
    String msg = "msg";

    String path_signup_otp = "signup/otp";
    String path_signup = "signup?";
    String path_signin = "signin";
    String path_flowA = "flow=a";
    String path_flowD = "flow=d";
    String path_forgot = "recovery?";
    String path_forgot_otp = "recovery/otp?";
    String path_forgot_email = "recovery/email?";
    String path_forgot_verify_code = "recovery/verify_code?";
    String path_forgot_newpass = "recovery/password?";
    String path_forgot_success = "recovery/success?";
    String path_forgot_verify = "recovery/verify?";
    String path_verify = "mapping?";
    String path_verify_service = "mapping/mapproduct?";
    String path_verify_otp = "mapping/otp?";
    String path_verify_success = "mapping/success?";
    String path_verify_already = "mapping/already?";
    String path_verify_identity = "recovery/verify_identity?";
    String path_signup_already = "signup/exist?";
    String path_signup_success = "signup/success?";
    String path_change_password = "profile/newpassword?";
    String path_change_password_success = "profile/passwordsuccess?";

}
