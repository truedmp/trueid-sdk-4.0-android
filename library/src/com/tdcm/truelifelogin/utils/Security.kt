package com.tdcm.truelifelogin.utils

import android.content.Context
import android.util.Base64
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.utils.crypt.CryptLib
import java.security.MessageDigest
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

internal class Security(val context: Context) {

    fun encrypt(data: String): String {
        val keyTransformation = context.getString(R.string.file_key_transformation)

        val cipher = Cipher.getInstance(keyTransformation)
        cipher.init(Cipher.ENCRYPT_MODE, getSecreteKey())

        val plainTextBytes = data.toByteArray(Charsets.UTF_8)
        val buf = cipher.doFinal(plainTextBytes)
        val base64Bytes = Base64.encode(buf, Base64.DEFAULT)
        return String(base64Bytes)
    }

    fun decrypt(data: String): String {
        val keyTransformation = context.getString(R.string.file_key_transformation)

        val message = Base64.decode(data.toByteArray(), Base64.DEFAULT)
        val decipher = Cipher.getInstance(keyTransformation)
        decipher.init(Cipher.DECRYPT_MODE, getSecreteKey())

        val plainText = decipher.doFinal(message)
        return String(plainText, Charsets.UTF_8)
    }

    fun encryptLibs(data: String): String {
        val hash = context.getString(R.string.hash)
        return CryptLib().encryptPlainTextWithRandomIV(data, hash)
    }

    fun decryptLibs(data: String): String {
        val hash = context.getString(R.string.hash)
        return CryptLib().decryptCipherTextWithRandomIV(data, hash)
    }

    private fun getSecreteKey(): SecretKey {
        val keySecret = context.getString(R.string.file_key_secret)
        val algMD = context.getString(R.string.file_key_alg_md)
        val algSecret = context.getString(R.string.file_key_alg_secret)

        val md = MessageDigest.getInstance(algMD)
        val digestOfPassword = md.digest(keySecret.toByteArray(Charsets.UTF_8))
        val keyBytes = Arrays.copyOf(digestOfPassword, 24)
        return SecretKeySpec(keyBytes, algSecret)
    }
}