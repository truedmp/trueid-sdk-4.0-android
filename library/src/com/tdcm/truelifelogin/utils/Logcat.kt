package com.tdcm.truelifelogin.utils

import android.util.Log
import com.tdcm.truelifelogin.interfaces.BuildVariant

/*
 * Created by boyDroids on 2/7/2018 AD.
 * ^.^
 */
internal class Logcat {
    companion object {

        private fun getPrefix(tag: String): String {
            return "TrueSDK($tag)"
        }

        /**
         * LOG V
         * */
        @JvmStatic
        fun v(tag: String, message: String, always: Boolean) {
            if (BuildVariant.DEBUG || always) Log.v(getPrefix(tag), message)
        }

        @JvmStatic
        fun v(tag: String, message: String) {
            v(tag, message, false)
        }

        /**
         * LOG Debug
         * */
        @JvmStatic
        fun d(tag: String, message: String, always: Boolean) {
            if (BuildVariant.DEBUG || always) Log.d(getPrefix(tag), message)
        }

        @JvmStatic
        fun d(tag: String, message: String) {
            d(tag, message, false)
        }

        /**
         * LOG Info
         * */
        @JvmStatic
        fun i(tag: String, message: String, always: Boolean) {
            if (BuildVariant.DEBUG || always) Log.i(getPrefix(tag), message)
        }

        @JvmStatic
        fun i(tag: String, message: String) {
            i(tag, message, false)
        }

        /**
         * LOG Warning
         * */
        @JvmStatic
        fun w(tag: String, message: String, always: Boolean) {
            if (BuildVariant.DEBUG || always) Log.w(getPrefix(tag), message)
        }

        @JvmStatic
        fun w(tag: String, message: String) {
            w(tag, message, false)
        }

        /**
         * LOG Error
         * */
        @JvmStatic
        fun e(tag: String, message: String, always: Boolean) {
            if (BuildVariant.DEBUG || always) Log.e(getPrefix(tag), message)
        }

        @JvmStatic
        fun e(tag: String, message: String) {
            e(tag, message, false)
        }
    }
}