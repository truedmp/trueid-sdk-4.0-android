package com.tdcm.truelifelogin.utils;

import java.io.Serializable;

/*
 * Created by Pratanporn on 10/5/2017 AD.
 */

public class StatusCodes implements Serializable{

    String code;
    String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
