package com.tdcm.truelifelogin.utils;

/*
 * Created by Pratanporn on 11/5/2017 AD.
 */

public interface KEYS {

    String ACCOUNT_TYPE = "com.tdcm.trueidapp";
    String activity = "activity";
    String access_token = "access_token";
    String refresh_token = "refresh_token";
    String root_access_token = "root_access_token";
    String root_refresh_token = "root_refresh_token";
    String UTF8 = "UTF-8";
    String code = "code";
    String START_SERVICE = "TRUEID_SSO_START_SERVICE";
    String ERROR = "error";
    String MESSAGE = "message";
    String messages = "messages";
    String client_id = "client_id";
    String package_name = "package_name";
    String expires_in = "expires_in";
    String POST = "POST";
    String GET = "GET";
    String en = "en";
    String th = "th";
    String logout = "logout";
    String login_code = "login_code";
    String scope = "scope";
    String device_model = "device_model";
    String device_id = "device_id";
    String ip_address = "ip_address";
    String latlong = "latlong";
    String username = "username";
    String password = "password";
    String grant_type = "grant_type";
    String state = "state";
    String extras = "extras";
    String by = "by";
    String client_credentials = "client_credentials";
    String android = "android";
    String root_token = "root_token";
    String member_client_id = "member_client_id";
    String GLOBAL = "GLOBAL";
    String KEYTOKENVERSION3 = "aHY0dGs";
    String grant_type_qr = "qr_code";
    String qr_data = "qr_data";
    /*KEY JSONObject */
    String data = "data";

    String truemoney = "truemoney";

    /*
     * Auth0
     * */
    String AUTH0_URL = "auth_url";
    String AUTH0_CLIENT_ID = "client_id";
    String AUTH0_REDIRECT_URI = "redirect_uri";
    String AUTH0_STATE = "state";
    String AUTH0_CODE_VERIFIER = "code_verifier";
    String AUTH0_CODE_CHALLENGE = "code_challenge";
    String AUTH0_CODE_CHALLENGE_METHOD = "code_challenge_method";
    String AUTH0_METHOD = "S256";
    String AUTH0_CODE = "code";

    String AUTHORIZATION_CODE = "authorization_code";
    /*
     * Truemoney
     */
    String BINDING_IDS = "binding_ids";
    String STATUS = "status";
    String OK = "ok";
}
