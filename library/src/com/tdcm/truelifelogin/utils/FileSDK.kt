package com.tdcm.truelifelogin.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Environment
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.SDKEnvironment
import org.json.JSONObject
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*

/*
 * Created by boyDroids on 11/4/2018 AD.
 * ^.^
 */
internal class FileSDK {
    internal companion object {
        private val TAG = FileSDK::class.java.simpleName

        private fun getExternalDirectory(path: String): File {
            /*--- /storage/sdcard0/Documents ---*/
            val dir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                path
            )
            if (!dir.exists()) {
                dir.mkdirs()
            }
            return dir
        }

        @JvmStatic
        fun getFilePath(context: Context, packageName: String): File {
            return File(getExternalDirectory(pathData(context)), packageName)
        }

        @JvmStatic
        fun getFileToken(context: Context): File {
            return File(getExternalDirectory(pathRoot(context)), ".token")
        }

        @JvmStatic
        fun getFilesDirectory(context: Context): ArrayList<String> {
            val path = pathData(context)
            val packageName = ArrayList<String>()

            return try {
                val listFiles =
                    getExternalDirectory(path).listFiles() ?: return packageName
                for (i in listFiles.indices) {
                    val fileName = listFiles[i].name
                    if (UtilsSDK.launchApplication(context, fileName)) {
                        packageName.add(fileName)
                    } else {
                        deleteFile(getFilePath(context, fileName))
                    }
                }
                Logcat.i(TAG, "getFilesDirectory Size : ${packageName.size}")
                packageName
            } catch (e: Exception) {
                e.printStackTrace()
                packageName
            }
        }

        private fun currentTimeInstall(context: Context?): String {
            return try {
                val timeInstall = context?.packageManager?.getPackageInfo(
                    context.packageName,
                    0
                )?.firstInstallTime
                if (timeInstall == null) "" else (timeInstall / 1000).toString()
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
                ""
            }
        }

        @JvmStatic
        fun writeFile(context: Context, file: File) {
            try {
                val currentTime = currentTimeInstall(context)
                val data = Security(context).encrypt(currentTime)
                file.writeText(data)
                Logcat.i(TAG, "writeFile \"${file.name}\" is created")
            } catch (e: Exception) {
                Logcat.e(TAG, "writeFile Exception : $e")
            } catch (e: IOException) {
                Logcat.e(TAG, "writeFile IOException : $e")
            }
        }

        @JvmStatic
        fun deleteFile(file: File) {
            file.delete()
            Logcat.i(TAG, "deleteFile \"${file.name}\" is deleted")
        }

        fun writeFileToken(context: Context, file: File) {
            try {
                val pf = PreferencesTrueID(context)
                /*save 9 token to center data*/
                val jObj = JSONObject().apply {
                    put(KEYS.package_name, pf.package_name)
                    put(KEYS.client_id, pf.client_id)
                    put(KEYS.expires_in, pf.token_expires_second)
                    put(KEYS.root_access_token, pf.access_token_root)
                    put(KEYS.root_refresh_token, pf.refresh_token_root)
                }
                val data = Security(context).encrypt(jObj.toString())
                file.writeText(data)
            } catch (e: Exception) {
                Logcat.e(TAG, "writeFileToken Exception : $e")
            } catch (e: IOException) {
                Logcat.e(TAG, "writeFileToken IOException : $e")
            }
        }

        @JvmStatic
        fun readFile(context: Context, file: File): String {
            var data = ""
            return try {
                val inputStream = file.inputStream()
                data = inputStream.bufferedReader().use { it.readText() }
                Security(context).decrypt(data)
            } catch (e: FileNotFoundException) {
                Logcat.e(TAG, "readFile FileNotFoundException : $e")
                data
            } catch (e: IOException) {
                Logcat.e(TAG, "readFile IOException : $e")
                data
            } catch (e: Exception) {
                Logcat.e(TAG, "readFile Exception : $e")
                data
            }
        }

        @JvmStatic
        fun readJsonAssets(context: Context, fileName: String): String {
            return context.assets.open(fileName).bufferedReader(Charsets.UTF_8).use {
                it.readText()
            }
        }

        private fun pathRoot(context: Context): String {
            return when (PreferencesTrueID(context).environment) {
                SDKEnvironment.ALPHA.value -> {
                    context.getString(R.string.path_alpha)
                }
                SDKEnvironment.STAGING.value -> {
                    context.getString(R.string.path_staging)
                }
                SDKEnvironment.PRODUCTION.value -> {
                    context.getString(R.string.path_production)
                }
                else -> {
                    ""
                }
            }
        }

        @JvmStatic
        fun pathData(context: Context): String {
            return "${pathRoot(context)}package"
        }
    }
}