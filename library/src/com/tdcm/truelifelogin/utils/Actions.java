package com.tdcm.truelifelogin.utils;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by Pratanporn on 10/5/2017 AD.
 */

public class Actions implements Serializable{

    String category;
    String name;
    ArrayList<String> arrayList_reason;
    ArrayList<StatusCodes> arrayList_status_code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getArrayList_reason() {
        return arrayList_reason;
    }

    public void setArrayList_reason(ArrayList<String> arrayList_reason) {
        this.arrayList_reason = arrayList_reason;
    }

    public ArrayList<StatusCodes> getArrayList_status_code() {
        return arrayList_status_code;
    }

    public void setArrayList_status_code(ArrayList<StatusCodes> arrayList_status_code) {
        this.arrayList_status_code = arrayList_status_code;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
