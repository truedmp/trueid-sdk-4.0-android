package com.tdcm.truelifelogin.utils.crypt

internal interface CryptLibImpl {
    @Throws(Exception::class)
    fun encryptPlainTextWithRandomIV(plainText: String, key: String): String

    @Throws(Exception::class)
    fun decryptCipherTextWithRandomIV(cipherText: String, key: String): String
}