package com.tdcm.truelifelogin.utils

import android.content.Context
import com.tdcm.truelifelogin.interfaces.LoginServiceListener
import java.lang.Exception

/*
 * Created by boyDroids on 9/4/2018 AD.
 * ^.^
 */
internal class AAATracking(private val trackingService: TrackingService?,
                  private val listener: LoginServiceListener?) {
    companion object {
        private val TAG = AAATracking::class.java.simpleName
    }

    fun event(name: String, isSuccess: Boolean) {
        if (name.isEmpty()) return

        val actions = trackingService?.getEvents(name)
        if (actions != null) {
            Logcat.i(TAG, "onTrackingEvent : $name")
            val events = UtilsSDK.getEvents(actions, isSuccess)
            listener?.onReceivedEvent(events)
        }
    }

    fun screen(context: Context, name: String) {
        try {
            Logcat.i(TAG, "onTrackingScreens : $name")
            if (FileSecurity.isExists(context, Files.Analytics)) {
                if (name.isEmpty()) return
                val screen = trackingService?.getScreens(name)
                listener?.onReceivedScreen(screen)
            } else {
                Logcat.i(TAG, "onTrackingScreens isFilePresent is false")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}