package com.tdcm.truelifelogin.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.tdcm.truelifelogin.R;


/*
 * Created by Pratanporn on 8/24/2017 AD.
 */

public class CustomButtonText extends Button{

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomButtonText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public CustomButtonText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomButtonText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public CustomButtonText(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomText);
            String fontName = a.getString(R.styleable.CustomText_font_true_custom);

            try {
                if (fontName != null) {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                    setTypeface(myTypeface);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            a.recycle();
        }
    }

}
