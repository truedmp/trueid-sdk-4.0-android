package com.tdcm.truelifelogin.httpclient

import android.content.Context
import android.net.Uri
import android.os.StrictMode
import com.tdcm.truelifelogin.utils.APIs
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.UtilsSDK
import org.apache.commons.net.ntp.NTPUDPClient
import org.json.JSONObject
import java.net.InetAddress
import javax.net.ssl.HttpsURLConnection

/*
 * Created by boyDroids on 5/6/2018 AD.
 * ^.^
 */
internal class SDKHttpServices(private val context: Context) {

    private var pf: PreferencesTrueID = PreferencesTrueID(context)

    /**
     * this function refresh token because access token is expire or valid
     * @param refreshToken is auth refresh token from client
     * @return raw response from http services
     * */
    fun postRefreshToken(refreshToken: String, isRoot: Boolean): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.client_id, pf.client_id)
            appendQueryParameter(KEYS.grant_type, KEYS.refresh_token)
            appendQueryParameter(KEYS.refresh_token, refreshToken)
            appendQueryParameter(KEYS.device_id, pf.android_id)
            appendQueryParameter(KEYS.device_model, pf.android_model)
            appendQueryParameter(KEYS.latlong, pf.location)
            appendQueryParameter(KEYS.ip_address, pf.ip_address)
            if (isRoot) appendQueryParameter(KEYS.scope, pf.scope)
        }

        val query = builder.build().encodedQuery
        val baseUrl = APIs.api_get_token_url ?: pf.apiGetTokenUrl

        return SDKHttpClient(context).post(baseUrl, query, "")
    }

    fun sendQRData(qrData: String): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.client_id, pf.client_id)
            appendQueryParameter(KEYS.grant_type, KEYS.grant_type_qr)
            appendQueryParameter(KEYS.scope, pf.scope)
            appendQueryParameter(KEYS.device_id, pf.android_id)
            appendQueryParameter(KEYS.device_model, pf.android_model)
            appendQueryParameter(KEYS.latlong, pf.location)
            appendQueryParameter(KEYS.ip_address, pf.ip_address)
            appendQueryParameter(KEYS.refresh_token, pf.refresh_token_client)
            appendQueryParameter(KEYS.qr_data, qrData)
        }

        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.api_auth_qrcode, query, "")
    }

    /**
     * this function get firebase server time
     * @return current timestamp from server
     * */
    @Synchronized
    fun getServerTime(): Long {
        val jRoot = SDKHttpClient(context).get(APIs.api_server_time)
        val jData = jRoot.getJSONObject(SDKJsonParser.DATA)
        if (jRoot.getInt(SDKJsonParser.CODE) == HttpsURLConnection.HTTP_OK) {
            return if (jData.has("date")) jData.getLong("date") else 0
        }
        return 0
    }

    /**
     * this function get NTP(Network time protocol) from server
     * @return current timestamp from server
     * */
    fun serverTimeNTP(): Long {
        val urlNTP = APIs.api_server_ntp ?: APIs.external_ntp_google

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val timeClient = NTPUDPClient()
        val iNet = InetAddress.getByName(urlNTP)
        val timeInfo = timeClient.getTime(iNet)
        val timeStamp = timeInfo?.message?.transmitTimeStamp?.time
        return timeStamp?.let {
            it / 1000
        } ?: 0
    }

    /**
     * this function get access token from login code
     * @return raw response from http services
     * */
    fun tokenFromCode(loginCode: String): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.login_code, loginCode)
            appendQueryParameter(KEYS.root_token, "1")
            appendQueryParameter(KEYS.scope, pf.scope)
        }

        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.api_get_token_url, query, "")
    }

    /**
     * this function send logout to server and can't wait result from server.
     * @return raw response from http services
     * */
    fun logout(pref: PreferencesTrueID): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.ip_address, pref.ip_address)
            appendQueryParameter(KEYS.client_id, pref.client_id)
            appendQueryParameter(KEYS.device_model, pref.android_model)
            appendQueryParameter(KEYS.latlong, pref.location)
        }

        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.revoke_url, query, pref.access_token_client)
    }

    /**
     * this function get info(profile) from server by access token
     * @return raw response from http services
     * */
    fun getProfileServer(pref: PreferencesTrueID): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.ip_address, pref.ip_address)
            appendQueryParameter(KEYS.latlong, pref.location)
        }
        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.api_get_user_info_url, query, pref.access_token_client)
    }

    /**
     * this function get load all config from fireBase url
     * */
    fun loadFireBase(urlFireBase: String): JSONObject {
        return SDKHttpClient(context).get(urlFireBase)
    }

    /**
     * this function using for login native
     * */
    fun loginFromNative(user: String, pass: String, pref: PreferencesTrueID): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.username, user)
            appendQueryParameter(KEYS.password, UtilsSDK.encode(pass))
            appendQueryParameter(KEYS.client_id, pref.client_id)
            appendQueryParameter(KEYS.grant_type, KEYS.password)
            appendQueryParameter(KEYS.scope, pf.scope)
            appendQueryParameter(KEYS.state, "1")
            appendQueryParameter(KEYS.extras, KEYS.android)
            appendQueryParameter(KEYS.device_id, pf.android_id)
            appendQueryParameter(KEYS.device_model, pf.android_model)
            appendQueryParameter(KEYS.latlong, pf.location)
            appendQueryParameter(KEYS.root_token, "1")
            appendQueryParameter(KEYS.ip_address, pf.ip_address)
        }
        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.api_get_token_url, query, "")
    }

    fun exchangeAuthorization(verifier: String, code: String): JSONObject {
        val builder = Uri.Builder().apply {
            appendQueryParameter(KEYS.AUTH0_CODE, code)
            appendQueryParameter(KEYS.grant_type, KEYS.AUTHORIZATION_CODE)
            appendQueryParameter(KEYS.client_id, pf.client_id)
            appendQueryParameter(KEYS.device_id, pf.android_id)
            appendQueryParameter(KEYS.device_model, pf.android_model)
            appendQueryParameter(KEYS.latlong, pf.location)
            appendQueryParameter(KEYS.ip_address, pf.ip_address)
            appendQueryParameter(KEYS.AUTH0_CODE_VERIFIER, verifier)
            appendQueryParameter(KEYS.root_token, "1")
            appendQueryParameter(KEYS.AUTH0_REDIRECT_URI, pf.reDirectUrl)
        }
        val query = builder.build().encodedQuery
        return SDKHttpClient(context).post(APIs.api_get_token_url, query, "")
    }

    /**
     * this function using for binding true money
     * */
    fun bindingTrueMoney(ids: String, accessToken: String): JSONObject {
        val jsonObject = JSONObject().apply {
            put(KEYS.BINDING_IDS, ids)
            put(KEYS.device_id, pf.android_id)
        }
        return SDKHttpClient(context).post(APIs.tmn_binding_url, jsonObject.toString(), accessToken, true)
    }
}