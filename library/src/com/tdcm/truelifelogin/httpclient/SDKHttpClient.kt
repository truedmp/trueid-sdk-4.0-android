package com.tdcm.truelifelogin.httpclient

import android.content.Context
import com.tdcm.truelifelogin.constants.JWTError
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.UtilsSDK
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedWriter
import java.io.InputStream
import java.io.OutputStreamWriter
import java.net.URL
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext

/*
 * Created by boyDroids on 5/6/2018 AD.
 * ^.^
 */
internal class SDKHttpClient(val context: Context) {

    private val timeout = 10000

    companion object {
        private val TAG = SDKHttpClient::class.java.simpleName
    }

    fun post(baseUrl: String?, data: String?, bearer: String): JSONObject {
        return post(baseUrl, data, bearer, false)
    }

    /**
     * @param baseUrl is url connection to apis
     * @param query is query string set for request http
     * @param header is header for request http
     * */
    fun post(baseUrl: String?, query: String?, header: String, isRaw: Boolean): JSONObject {
        val jsonParser = SDKJsonParser(context)
        try {
            if (baseUrl == null || baseUrl.isEmpty()) return jsonParser.parser(getJsonConflict(), HttpsURLConnection.HTTP_CONFLICT)
            if (!Validate.hasInternetAccess(context)) return jsonParser.parser(getJsonNoContent(), HttpsURLConnection.HTTP_UNAVAILABLE)

            val url = URL(baseUrl)
            val conn = url.openConnection() as HttpsURLConnection
            if (!header.isEmpty()) {
                conn.setRequestProperty("Authorization", "Bearer $header")
            }
            if (isRaw) {
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
            }
//            conn.sslSocketFactory = getSSLv3()
            conn.requestMethod = HttpMethod.POST
            conn.connectTimeout = timeout
            conn.readTimeout = timeout
            conn.doInput = true
            conn.doOutput = true

            val os = conn.outputStream
            if (query != null) {
                BufferedWriter(OutputStreamWriter(os, Charsets.UTF_8)).apply {
                    write(query)
                    flush()
                    close()
                }
            }
            os.close()

            /**
             * check http code 200 first*/
            val responseCode = conn.responseCode
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                val ips: InputStream = BufferedInputStream(conn.inputStream)
                val response = ips.bufferedReader(Charsets.UTF_8).use { it.readText() }

                ips.close()
                conn.disconnect()

                Logcat.i(TAG, "HTTP SUCCESS : $response")
                return jsonParser.parser(response, responseCode)
            } else {
                val ips: InputStream = BufferedInputStream(conn.errorStream)
                val response = ips.bufferedReader(Charsets.UTF_8).use { it.readText() }

                ips.close()
                conn.disconnect()

                Logcat.e(TAG, "HTTP ERROR : $response")
                return jsonParser.parser(response, responseCode)
            }
        } catch (e: Exception) {
            Logcat.e(TAG, "Exception : $e")
            return JSONObject()
        }
    }

    /**
     * @param baseUrl is url connection to apis
     * */
    fun get(baseUrl: String?): JSONObject {
        val jsonParser = SDKJsonParser(context)
        try {
            if (baseUrl == null || baseUrl.isEmpty()) return jsonParser.parser(getJsonConflict(), HttpsURLConnection.HTTP_CONFLICT)
            if (!Validate.hasInternetAccess(context)) return jsonParser.parser(getJsonNoContent(), HttpsURLConnection.HTTP_UNAVAILABLE)

            val url = URL(baseUrl)
            val conn = url.openConnection() as HttpsURLConnection
//            conn.sslSocketFactory = getSSLv3()
            conn.requestMethod = HttpMethod.GET
            conn.connectTimeout = timeout
            conn.readTimeout = timeout

            /**
             * check http code 200 first*/
            val responseCode = conn.responseCode
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                val ips: InputStream = BufferedInputStream(conn.inputStream)
                val response = ips.bufferedReader(Charsets.UTF_8).use { it.readText() }

                ips.close()
                conn.disconnect()

                Logcat.i(TAG, "HTTP SUCCESS : $response")
                return jsonParser.parser(response, responseCode)
            } else {
                val ips: InputStream = BufferedInputStream(conn.errorStream)
                val response = ips.bufferedReader(Charsets.UTF_8).use { it.readText() }

                ips.close()
                conn.disconnect()

                Logcat.e(TAG, "HTTP ERROR : $response")
                return jsonParser.parser(response, responseCode)
            }
        } catch (e: Exception) {
            Logcat.e(TAG, "Exception : $e")
            return JSONObject()
        }

    }

    private fun getSSLv3(): NoSSLv3SocketFactory {
        val sslContext = SSLContext.getInstance("TLSv1")
        sslContext.init(null, null, null)
        return NoSSLv3SocketFactory(sslContext.socketFactory)
    }

    private fun getJsonNoContent(): String {
        val jObject = JSONObject().apply {
            put(KEYS.ERROR, UtilsSDK.getErrorCode(JWTError.ERROR_INTERNET_ACCESS))
        }
        return jObject.toString()
    }

    private fun getJsonConflict(): String {
        val jObject = JSONObject().apply {
            put(KEYS.ERROR, UtilsSDK.getErrorCode(JWTError.ERROR_EMPTY_URL))
        }
        return jObject.toString()
    }

}