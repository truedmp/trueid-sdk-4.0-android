package com.tdcm.truelifelogin.httpclient

/*
 * Created by boyDroids on 5/6/2018 AD.
 * ^.^
 */
internal class HttpMethod {
    companion object {
        const val GET = "GET"
        const val POST = "POST"
        const val PUT = "PUT"
        const val DELETE = "DELETE"
    }
}