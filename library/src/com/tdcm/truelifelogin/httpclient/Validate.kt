package com.tdcm.truelifelogin.httpclient

import android.content.Context
import android.net.ConnectivityManager
import com.tdcm.truelifelogin.utils.APIs
import com.tdcm.truelifelogin.utils.Logcat
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

/*
 * Created by boyDroids on 5/6/2018 AD.
 * ^.^
 */
internal class Validate {

    companion object {
        private val TAG = Validate::class.java.simpleName

        fun printNull(name: String) {
            Logcat.e(TAG, "Argument '$name' is null")
        }

        @JvmStatic
        fun hasInternetAccess(context: Context?): Boolean {
            context?.let {
                val cm: ConnectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val ani = cm.activeNetworkInfo
                return if (ani != null && ani.isConnected) {
                    checkInternetAccess()
                } else {
                    false
                }
            }
            printNull("Context >> hasInternetAccess")
            return false
        }

        private fun checkInternetAccess(): Boolean {
            return try {
                val urlc = URL(APIs.url_google_204)
                        .openConnection() as HttpURLConnection
                urlc.setRequestProperty("User-Agent", "Android")
                urlc.setRequestProperty("Connection", "close")
                urlc.connectTimeout = 1500
                urlc.connect()
                urlc.responseCode == HttpURLConnection.HTTP_NO_CONTENT && urlc.contentLength == 0
            } catch (e: IOException) {
                e.printStackTrace()
                Logcat.e(TAG, "ERROR IOException : ${e.message}")
                false
            }
        }
    }
}