package com.tdcm.truelifelogin.httpclient

import android.content.Context
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.utils.crypt.CryptLib
import org.json.JSONException
import org.json.JSONObject
import javax.net.ssl.HttpsURLConnection

internal class SDKJsonParser(val context: Context) {
    companion object {
        const val TIMESTAMP = "timestamp"
        const val STATUS = "status"
        const val CODE = "code"
        const val MESSAGE = "message"
        const val DATA = "data"

        private const val CODE_EXCEPTION = 9999
        private const val STATUS_SUCCESS = "success"
        private const val STATUS_FAILED = "failed"
    }

    fun parser(json: String?, code: Int): JSONObject {
        val timestamp = System.currentTimeMillis() / 1000
        try {
            val dataObject = if (json == null || json.isEmpty()) JSONObject() else {
                val dataText = if (json.contains("{")) {
                    json
                } else {
                    CryptLib().decryptCipherTextWithRandomIV(json, context.getString(R.string.hash))
                }
                JSONObject(dataText)
            }

            return JSONObject().apply {
                put(TIMESTAMP, timestamp)
                put(STATUS, if (code == HttpsURLConnection.HTTP_OK) STATUS_SUCCESS else STATUS_FAILED)
                put(CODE, code)
                put(DATA, dataObject)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            return JSONObject().apply {
                put(TIMESTAMP, timestamp)
                put(STATUS, STATUS_FAILED)
                put(CODE, CODE_EXCEPTION)
                put(MESSAGE, e.toString())
                put(DATA, JSONObject())
            }
        }
    }
}