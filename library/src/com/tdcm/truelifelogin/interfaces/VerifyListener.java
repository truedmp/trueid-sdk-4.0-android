package com.tdcm.truelifelogin.interfaces;

/*
 * Created by boyDroids on 5/25/2018 AD.
 */

/**
 * This is interface VerifyListener
 */
public interface VerifyListener {
    /**
     * This is callback onVerifySuccess will call when Ues function "selfVerify" in Class LoginService and VerifySuccess
     *
     * @param accessToken return AccessToken
     */
    void onVerifySuccess(String accessToken);

    /**
     * This is callback onVerifyFailed will call when Ues function "selfVerify" in Class LoginService and VerifyFailed
     *
     * @param errorMsg message Error
     */
    void onVerifyFailed(String errorMsg);
}
