package com.tdcm.truelifelogin.interfaces

/*
 * Created by boyDroids on 7/8/2018 AD.
 * ^.^
 */

/**
 * This is interface BuildVariant ser data Show log
 */
internal interface BuildVariant {
    /**
     * DEBUG set data = true is want Show Logcat
     */
    companion object {
        var DEBUG: Boolean = false
    }
}