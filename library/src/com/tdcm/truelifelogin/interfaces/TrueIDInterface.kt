package com.tdcm.truelifelogin.interfaces

interface TrueIDInterface {
    /**
     * Listener for used support function >
     * > Binding True Money
     * */
    interface BindingTrueMoneyListener {
        fun onBindingSuccess(accessToken: String?)
        fun onBindingError(message: String?)
    }

    /**
     * Listener for used support function >
     * > QR General Scanning
     * */
    interface QRGeneralListener {
        fun onSuccess(value: String?)
        fun onCancel()
    }

    /**
     * Listener for used support function >
     * > Change Password TrueID Account
     * */
    interface ChangePasswordListener {
        fun onChangePasswordSuccess(accessToken: String)
        fun onChangePasswordError(message: String)
    }

    /**
     * Listener for used support function >
     * > Force refresh token
     * */
    interface RefreshTokenListener {
        fun onSuccess(accessToken: String)
        fun onError(message: String)
    }
}