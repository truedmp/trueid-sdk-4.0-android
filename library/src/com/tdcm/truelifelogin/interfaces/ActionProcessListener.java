package com.tdcm.truelifelogin.interfaces;

/*
 * Created by boyDroids on 5/21/2018 AD.
 */

/**
 * This is interface ActionProcessListener
 */
public interface ActionProcessListener {

    /**
     * This is callback onFinish will call when end have work is already done
     *
     * @param isSuccess isSuccess is Work Success
     */
    void onFinish(boolean isSuccess);
}
