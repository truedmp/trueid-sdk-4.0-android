package com.tdcm.truelifelogin.interfaces;

import com.tdcm.truelifelogin.models.Events;
import com.tdcm.truelifelogin.models.Screens;

/*
 * Created by pratanporn on 27/12/2017 AD.
 * Updated by boyDroids on 3/5/2018 AD.
 */

/**
 * this is interface Listener for Action
 */
public interface LoginServiceListener {

    /**
     * This is callback onLoginSuccess will call when login Success
     *
     * @param json          jsonObject Data Info user
     * @param expiresSecond time expires Second
     */
    void onLoginSuccess(String json, int expiresSecond);

    /**
     * This is callback onLoginError will call when login Error
     *
     * @param msg message Login Error
     */
    void onLoginError(String msg);

    /**
     * This is callback onLogoutRespond will call when Logout
     *
     * @param isSuccess isSuccess is Logout Success
     * @param json      message JsonObject
     */
    void onLogoutRespond(boolean isSuccess, String json);

    /**
     * This is callback onRefreshAccessToken will call when refresh Access Token
     *
     * @param isSuccess isSuccess is Refresh Success
     */
    void onRefreshAccessToken(boolean isSuccess);

    /**
     * This is callback onCanceled will call when User action cancel SDK
     */
    void onCanceled();

    /**
     * This is callback onFindTrueIDApp will call when Use method callTrueIDApp in Call LoginService
     *
     * @param isFound isFound is have app TrueID
     */
    void onFindTrueIDApp(boolean isFound);

    //4.0 version

    /**
     * This is Mapping when ThaiID Already
     *
     * @param msg Message Already
     */
    void onMappingAlready(String msg);

    /**
     * This is Mapping when ThaiID Success
     *
     * @param msg Message Success
     */
    void onMappingSuccess(String msg);

    /**
     * This is Mapping when ThaiID Failed
     *
     * @param msg Message Failed
     */
    void onMappingFailed(String msg);

    /**
     * This is callback will call when Register Success
     *
     * @param loginCode message Login Code
     * @param clientId  message ClientID
     */
    void onRegisterSuccess(String loginCode, String clientId);

    /**
     * This is callback will call when Register Error
     *
     * @param errorMessage Message Error Register
     */
    void onRegisterError(String errorMessage);

    /**
     * This is callback will call when Forgot Success
     *
     * @param loginCode message Forgot Login Code
     * @param clientId  message Forgot Client ID
     */
    void onForgotSuccess(String loginCode, String clientId);

    /**
     * This is callback will call when Forget Error
     *
     * @param errorObject jsonObject Message Error
     */
    void onForgotError(String errorObject);

    /**
     * This is callback will call when GetInfoSuccess
     *
     * @param json          jsonObject data info
     * @param expiresSecond time expires Second
     */
    void onGetInfoSuccess(String json, int expiresSecond);

    /**
     * This is callback will call when GetInfoFailed
     *
     * @param errorMessage message GetInfoFailed
     */
    void onGetInfoFailed(String errorMessage);

    /**
     * This is callback onReceivedScreen will call when user Action SDK
     *
     * @param screens this is return Screens
     */
    void onReceivedScreen(Screens screens);

    /**
     * This is callback onReceivedEvent will call when user Action SDK
     *
     * @param events this is return Events User Action
     */
    void onReceivedEvent(Events events);

    /**
     * This is callback onRefreshAccessTokenFailed will call when Refresh Access Token Fail
     *
     * @param errorMessage message error Refresh Access Token Fail
     */
    void onRefreshAccessTokenFailed(String errorMessage);

    /**
     * This is callback onRevokeAlready will call when Access Token invalid
     */
    void onRevokeAlready();
}
