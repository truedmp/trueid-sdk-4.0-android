package com.tdcm.truelifelogin.interfaces

/*
 * Created by boyDroids on 11/7/2018 AD.
 * ^.^
 */
/**
 * This is interface Listener QRCode
 */
interface QRScanListener {
    /**
     * This callback Just when ScanQRCode Success
     */
    fun onSuccess(message: String?)

    /**
     * This callback Just when ScanQRCode Error
     */
    fun onError(message: String?)

    /**
     * This callback Just when ScanQRCode User action cancel
     */
    fun onCancel()
}