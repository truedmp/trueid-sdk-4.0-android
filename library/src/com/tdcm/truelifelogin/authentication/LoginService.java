package com.tdcm.truelifelogin.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;

import com.tdcm.truelifelogin.constants.ResourceString;
import com.tdcm.truelifelogin.constants.SDKEnvironment;
import com.tdcm.truelifelogin.interfaces.ActionProcessListener;
import com.tdcm.truelifelogin.interfaces.BuildVariant;
import com.tdcm.truelifelogin.interfaces.LoginServiceListener;
import com.tdcm.truelifelogin.interfaces.QRScanListener;
import com.tdcm.truelifelogin.interfaces.TrueIDInterface;
import com.tdcm.truelifelogin.interfaces.VerifyListener;
import com.tdcm.truelifelogin.utils.PreferencesTrueID;
import com.tdcm.truelifelogin.utils.UtilsSDK;

import java.util.List;

/*
 * Created by pratanporn on 27/12/2017 AD.
 */

/**
 * This is LoginService from SDK
 */
public class LoginService {
    protected String TAG = LoginService.class.getSimpleName();

    private Activity activity;
    private LoginServiceListener listener;
    private PreferencesTrueID pf;
    private ServiceSettings settings;

    /**
     * initialize Class LoginService Constructor
     * Not Support new feature Select Environment can add param is Dev false = Production true = staging
     * Not Support feature Self but Support function SSO only
     *
     * @param activity    for Callback Listener OnCanceled
     * @param scope       scope for sever response data
     * @param isDev       false = Production true = staging
     * @param reDirectUrl redirectUrl
     */
    @Deprecated
    public LoginService(Activity activity, List scope, boolean isDev, String reDirectUrl) {
        this.activity = activity;
        if (activity instanceof LoginServiceListener) {
            listener = (LoginServiceListener) activity;
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        SDKEnvironment environment = (isDev) ? SDKEnvironment.STAGING : SDKEnvironment.PRODUCTION;
        setDefault(scope, reDirectUrl, false, environment);
    }

    /**
     * initialize Class LoginService Constructor
     * Support new feature can select Environment SDKEnvironment
     * - Self Login (Auto is false[Using SSO])
     * - SDK Environment Alpha
     * this function is SSO
     *
     * @param activity    for Callback Listener OnCanceled
     * @param scope       scope for sever response data
     * @param env         Environment SDKEnvironment for select state
     * @param reDirectUrl redirectUrl
     */
    public LoginService(Activity activity, List scope, String reDirectUrl, SDKEnvironment env) {
        this(activity, scope, reDirectUrl, false, env);
    }

    /**
     * initialize Class LoginService Constructor
     * Support new feature can set boolean isSelf true for Self Login
     * - Self Login (Auto is false[Using SSO])
     * - SDK Environment Alpha
     *
     * @param activity    for Callback Listener OnCanceled
     * @param scope       scope for sever response data
     * @param reDirectUrl redirectUrl
     * @param isSelf      for Select SSO or Self  true = self false = SSO
     * @param env         Environment SDKEnvironment for select state
     */
    public LoginService(Activity activity, List scope, String reDirectUrl, boolean isSelf, SDKEnvironment env) {
        this.activity = activity;
        if (activity instanceof LoginServiceListener) {
            listener = (LoginServiceListener) activity;
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setDefault(scope, reDirectUrl, isSelf, env);
    }

    /**
     * set data for show Log SDK
     */
    public void setBuildConfig(boolean debugMode) {
        BuildVariant.Companion.setDEBUG(debugMode);
    }

    public void setClientId(String clientId) {
        pf.setClient_id(clientId);
    }

    /*
     * =============================================================================================
     * Configs
     * =============================================================================================
     */

    private void setDefault(List scope, String reDirectUrl, boolean isSelf, SDKEnvironment env) {
        pf = new PreferencesTrueID(activity);
        setTrueIDClient();

        pf.setIsSelf(isSelf);
        pf.setEnvironment(env.getValue());
        pf.setReDirectUrl(bindingDirectUrl(reDirectUrl));
        pf.setScope(bindingScope(scope));
        pf.setAndroid_id(getDeviceID());
        pf.setAndroid_model(Build.MODEL);
        pf.setPackage_name(activity.getPackageName());
        pf.setClient_class(activity.getClass().getCanonicalName());

        settings = new ServiceSettings(activity, pf, listener);
    }


    private void setTrueIDClient() {
        Resources mResource = activity.getResources();
        String mPackage = activity.getPackageName();

        int trueIdClientId = mResource.getIdentifier(ResourceString.clientId, "string", mPackage);
        int trueIdClientFlow = mResource.getIdentifier(ResourceString.clientFlow, "string", mPackage);
        int trueIdClientSchemeForSDK = mResource.getIdentifier(ResourceString.clientSchemeForSdk, "string", mPackage);

        if (trueIdClientId != 0) {
            pf.setClient_id(activity.getString(trueIdClientId));
        }
        if (trueIdClientFlow != 0) {
            pf.setClient_flow(activity.getString(trueIdClientFlow));
        }
        if (trueIdClientSchemeForSDK != 0) {
            pf.setClient_scheme_for_sdk(activity.getString(trueIdClientSchemeForSDK));
        }
    }

    /*
     * =============================================================================================
     * None UI
     * =============================================================================================
     */

    private String bindingScope(List scope) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < scope.size(); i++) {
            if (scope.size() == 1) {
                result.append(scope.get(i));
            } else {
                result.append(scope.get(i)).append(",");
            }
        }
        return result.toString();
    }

    private String bindingDirectUrl(String reDirectUrl) {
        if (reDirectUrl == null || reDirectUrl.isEmpty()) {
            return " ";
        }
        return reDirectUrl;
    }

    /**
     * Check that login
     *
     * @return check have AccessToken return Boolean
     */
    public boolean isLogin() {
        String acc = pf.getAccess_token_client();
        if (!acc.equalsIgnoreCase("")) {
            pf.setIsLogin(true);
        } else {
            pf.setIsLogin(false);
        }
        return pf.getIsLogin();
    }

    /**
     * return access token String not check access token Expired or not
     */
    public String getAccessToken() {
        return pf.getAccess_token_client();
    }

    /**
     * return refresh token String not check access token Expired or not
     */
    public String getRefreshToken() {
        return pf.getRefresh_token_client();
    }

    /**
     * this function check Access Token Expire
     * param verifyListener this callback return access token if not success return message error
     */
    public void selfVerify(VerifyListener verifyListener) {
        settings.selfVerify(verifyListener);
    }

    public void forceRefresh(String key, TrueIDInterface.RefreshTokenListener callback){
        settings.forceRefresh(key, callback);
    }

    /**
     * time Token Expire
     *
     * @return time AccessToken int type
     */
    public int getRefreshTokenTimeout() {
        int timestamp = (int) (pf.getCache_timestamp() - (System.currentTimeMillis()));
        int expire = pf.getToken_expires_second();
        int timeout = (timestamp - expire) / 1000;
        if (timeout <= 0) {
            timeout = 0;
        }
        return timeout;
    }

    /**
     * call method Callback onCanceled
     */
    public void onCanceled() {
        listener.onCanceled();
    }

    /**
     * return Device ID
     *
     * @return Device ID
     */
    public String getDeviceID() {
        return UtilsSDK.getDeviceId(activity);
    }

    /*
     * =============================================================================================
     * Function
     * =============================================================================================
     */

    /**
     * login this is Dialog webview Login Username TrueID
     *
     * @param language    change language on Screen "en" = English "th" = Thai
     * @param latitude    latitude
     * @param longitude   longitude
     * @param isAutoLogin this is Auto Login after login success   true = Auto login false = not Auto login
     */
    public void login(String language, String latitude, String longitude, boolean isAutoLogin) {
        pf.setLanguage(language);
        pf.setLocation(latitude + "," + longitude);
        pf.setIsAutoLogin(isAutoLogin);
        settings.login();
    }

    /**
     * register TrueID Username Password
     *
     * @param language    change language on Screen "en" = English "th" = Thai
     * @param latitude    latitude
     * @param longitude   longitude
     * @param flow        this is change Flow register has "A" OR "D" | Flow "A" = register use number only ,Flow "D" = register use number and email
     * @param isAutoLogin this is Auto Login after register success   true = Auto login false = not Auto login
     */
    public void register(String language, String latitude, String longitude, String flow, boolean isAutoLogin) {
        pf.setLanguage(language);
        pf.setLocation(latitude + "," + longitude);
        pf.setIsAutoLogin(isAutoLogin);
        pf.setClient_flow(flow);
        settings.register();
    }

    /**
     * loginNative this is dialog Login call When can not call webview
     *
     * @param language  change language on Screen "en" = English "th" = Thai
     * @param latitude  latitude
     * @param longitude longitude
     */
    private void loginNative(String language, String latitude, String longitude) {
        pf.setLanguage(language);
        pf.setLocation(latitude + "," + longitude);
        settings.loginNative();
    }

    /**
     * function this verifyThai id callback onMappingAlready verify thaiID already , nMappingSuccess verify thaiID Success and refaceAccess Token , onMappingFailed verify thaiID Fail
     *
     * @param language change language on Screen "en" = English "th" = Thai
     */
    public void verifyThaiId(String language) {
        pf.setLanguage(language);
        settings.verifyThaiId();
    }

    /**
     * recovery this is Forgot password
     *
     * @param language    change language on Screen "en" = English "th" = Thai
     * @param latitude    latitude
     * @param longitude   longitude
     * @param isAutoLogin this is Auto Login after forgot success   true = Auto login false = not Auto login
     */
    public void recovery(String language, String latitude, String longitude, boolean isAutoLogin) {
        pf.setLanguage(language);
        pf.setLocation(latitude + "," + longitude);
        pf.setIsAutoLogin(isAutoLogin);
        settings.recovery();
    }

    /**
     * function this is Update Password
     *
     * @param language change language on Screen "en" = English "th" = Thai
     */
    public void updatePassword(String language, String latitude, String longitude, TrueIDInterface.ChangePasswordListener callback) {
        pf.setLanguage(language);
        pf.setLocation(latitude + "," + longitude);
        settings.updatePassword(callback);
    }

    /**
     * getLoginInfo return data all profile
     */
    public void getLoginInfo() {
        settings.getLoginInfo(false, true, "");
    }

    /**
     * getProfileMore return data Profile format Json
     */
    public void getProfileMore() {
        settings.getProfileMore();
    }

    /**
     * FindTrueIDApp is Check TrueID App This Function Callback Boolean onFindTrueIDApp
     */
    public void FindTrueIDApp() {
        settings.FindTrueIDApp();
    }

    /**
     * callTrueIDApp will find TrueID App and Open TrueID App
     */
    public void callTrueIDApp() {
        settings.callTrueIDApp();
    }

    /**
     * logout delete User True and Callback onLogoutRespond return Boolean isSuccess
     */
    public void logout() {
        settings.logout();
    }

    /**
     * onResume check AccessToken
     */
    public void onResume() {
        if (pf.getIsSelf()) {
            settings.onResumeSelf();
        } else {
            settings.onResume();
        }
    }

    /**
     * function is login form QRCode Scanner to Web
     *
     * @param listener callback QRScanListener
     */
    public void openQRScanner(QRScanListener listener) {
        settings.openQRScanner(listener);
    }

    /**
     * function is login form QRCode Scanner General to Web
     *
     * @param listener callback TrueIDInterface.QRGeneralListener
     */
    public void openQRGeneral(TrueIDInterface.QRGeneralListener listener) {
        settings.openQRGeneral(listener);
    }

    public void sendQRData(String json, QRScanListener listener) {
        settings.sendQRData(json, listener);
    }

    /**
     * set data Referral App
     */
    public void setReferralApp(String referralApp) {
        pf.setReferral_app(referralApp);
    }

    /**
     * Interface definition for a callback to onFinish.
     */
    public void setActionListener(ActionProcessListener actionListener) {
        settings.setActionProcessListener(actionListener);
    }

    /**
     * Interface is BindingTrueMoney
     *
     * @param context    context
     * @param bindingIds bindingIds from client
     * @param callBack   callback BindingTrueMoneyListener
     */
    public void bindingTrueMoney(Context context, String bindingIds,
                                 TrueIDInterface.BindingTrueMoneyListener callBack) {
        settings.bindingTrueMoney(context, bindingIds, callBack);
    }
}
