package com.tdcm.truelifelogin.authentication;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import com.tdcm.truelifelogin.R;
import com.tdcm.truelifelogin.activities.QRGeneralActivity;
import com.tdcm.truelifelogin.activities.QRScannerActivity;
import com.tdcm.truelifelogin.constants.ClearSession;
import com.tdcm.truelifelogin.constants.JWTInvalidCase;
import com.tdcm.truelifelogin.constants.KeyRefreshToken;
import com.tdcm.truelifelogin.constants.KeyVerification;
import com.tdcm.truelifelogin.constants.MappingCase;
import com.tdcm.truelifelogin.dialogs.DialogTermAndCondition;
import com.tdcm.truelifelogin.dialogs.DialogVerification;
import com.tdcm.truelifelogin.dialogs.DialogVerificationNative;
import com.tdcm.truelifelogin.interfaces.ActionProcessListener;
import com.tdcm.truelifelogin.interfaces.LoginServiceListener;
import com.tdcm.truelifelogin.interfaces.QRScanListener;
import com.tdcm.truelifelogin.interfaces.TrueIDInterface;
import com.tdcm.truelifelogin.interfaces.VerifyListener;
import com.tdcm.truelifelogin.internal.ValidateJwtToken;
import com.tdcm.truelifelogin.tasks.APIServices;
import com.tdcm.truelifelogin.tasks.TaskBindingTrueMoney;
import com.tdcm.truelifelogin.tasks.TaskCheckNetwork;
import com.tdcm.truelifelogin.tasks.TaskConfigsDefault;
import com.tdcm.truelifelogin.tasks.TaskInfoServer;
import com.tdcm.truelifelogin.tasks.TaskLogout;
import com.tdcm.truelifelogin.tasks.TaskRefreshToken;
import com.tdcm.truelifelogin.tasks.TaskValidateJWT;
import com.tdcm.truelifelogin.utils.AAATracking;
import com.tdcm.truelifelogin.utils.APIs;
import com.tdcm.truelifelogin.utils.FileSDK;
import com.tdcm.truelifelogin.utils.KEYS;
import com.tdcm.truelifelogin.utils.KeysTracking;
import com.tdcm.truelifelogin.utils.Logcat;
import com.tdcm.truelifelogin.utils.PreferencesTrueID;
import com.tdcm.truelifelogin.utils.TrackingService;
import com.tdcm.truelifelogin.utils.UtilsSDK;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/*
 * Created by pratanporn on 28/12/2017 AD.
 */

public class ServiceSettings {

    private String TAG = ServiceSettings.class.getSimpleName();

    private Activity activity;
    private PreferencesTrueID pf;
    private LoginServiceListener listener;
    private ActionProcessListener actionListener;
    private VerifyListener verifyListener;
    private TrueIDInterface.ChangePasswordListener passwordListener;
    private TrueIDInterface.RefreshTokenListener refreshTokenListener;
    private TrackingService ga;

    /*
     * =============================================================================================
     * Initial
     * =============================================================================================
     */
    protected ServiceSettings(Activity activity, PreferencesTrueID pf, LoginServiceListener listener) {
        this.activity = activity;
        this.pf = pf;
        this.listener = listener;

        /*load configs*/
        exeLoadConfigs();
    }

    void setActionProcessListener(ActionProcessListener actionListener) {
        this.actionListener = actionListener;
    }

    private void exeLoadConfigs() {
        new TaskConfigsDefault(activity)
                .setOnLoadConfigs(new TaskConfigsDefault.OnLoadConfigs() {
                    @Override
                    public void onPostExecute(@Nullable TrackingService trackingService) {
                        ga = trackingService;
                    }
                }).execute();
    }

    /*
     * =============================================================================================
     * TrueIDWebViewClient
     * =============================================================================================
     */

    private void isLoadConfigsComplete(final String url, String funName, final String param) {
        UtilsSDK.clearCookie(activity.getApplicationContext());
        openWebDialog(url + param, funName);
    }

    private void openWebDialog(final String openUrl, final String funName) {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                openDialogVerification(openUrl, funName);
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    private void openDialogVerification(final String openUrl, final String funName) {
        if (!activity.isFinishing() && openUrl != null) {
            DialogVerification dialogVerify = new DialogVerification(activity, ga, pf, openUrl, funName);
            dialogVerify.setOnChangePassword(passwordListener);
            dialogVerify.setOnDialogVerification(listener, new DialogVerification.OnDialogVerification() {
                @Override
                public void onGetInfo(@NotNull String truemoney) {
                    getLoginInfo(false, false, truemoney);
                }

                @Override
                public void openDialog(@NotNull String url) {
                    openTermAndConditionDialog(url);
                }

                @Override
                public void onRefreshToken() {
                    String refreshTokenClient = pf.getRefresh_token_client();
                    RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_MAPPING);
                }

                @Override
                public void onOpenNative() {
                    loginNative();
                }

                @Override
                public void onRevoke() {
                    clearSession(ClearSession.REVOKE, null);
                }
            });
            dialogVerify.show();
        } else {
            Logcat.w(TAG, "activity is finish or url is null, please try again");
        }
    }

    private void openTermAndConditionDialog(final String mUrl) {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                if (!activity.isFinishing()) {
                    DialogTermAndCondition dialogTerm = new DialogTermAndCondition(activity, mUrl, pf);
                    dialogTerm.setOnDialogTermAndCondition(listener);
                    dialogTerm.show();
                }
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    /*
     * =============================================================================================
     * LoginNative
     * =============================================================================================
     */
    void loginNative() {
        new AAATracking(ga, listener).screen(activity, KeysTracking.Screens_LOGIN_NATIVE);
        DialogVerificationNative aNative = new DialogVerificationNative(activity, ga, pf);
        aNative.setOnDialogVerificationNative(listener, new DialogVerificationNative.OnDialogVerificationNative() {

            @Override
            public void getInfo() {
                getLoginInfo(false, false, "");
            }

            @Override
            public void enableService() {
            }
        });
        aNative.opened();
    }

    /*
     * =============================================================================================
     * checkTrueIDApp
     * =============================================================================================
     */

    void FindTrueIDApp() {
        boolean hasTrueIdApp = (activity.getPackageManager().getLaunchIntentForPackage(KEYS.ACCOUNT_TYPE)) != null;
        if (listener != null) {
            listener.onFindTrueIDApp(hasTrueIdApp);
        } else {
            Logcat.i(TAG, activity.getString(R.string.trueid_error_implement, pf.getClient_class(), "onFindTrueIDApp()"));
        }
    }

    void callTrueIDApp() {
        if (activity.getPackageManager().getLaunchIntentForPackage(KEYS.ACCOUNT_TYPE) == null) {
            String uriString = "market://details?id=" + APIs.ACCOUNT_TYPE;
            UtilsSDK.openStoreDialog(activity, uriString);
        } else {
            String uriString = "trueid://?referral=" + pf.getClient_scheme_for_sdk();
            UtilsSDK.openIntentActionView(activity, uriString);
        }
    }

    /*
     * =============================================================================================
     * Login
     * =============================================================================================
     */
    protected void login() {
        checkLogin();
    }

    private boolean isEmptyToken() {
        return (pf.getAccess_token_root().isEmpty() ||
                pf.getRefresh_token_root().isEmpty() ||
                pf.getAccess_token_client().isEmpty() ||
                pf.getRefresh_token_client().isEmpty());
    }

    private void checkLogin() {
        if (isEmptyToken()) {
            Logcat.d(TAG, "checking session : all token is empty.");

            String query = APIServices.INSTANCE.getQueryLogin(pf);
            isLoadConfigsComplete(APIs.web_signin_url, KeyVerification.caseSignIn, query);
        } else {
            Logcat.d(TAG, "checking session : token is available");
            getLoginInfo(false, false, "");
        }
    }

    /*
     * =============================================================================================
     * Register
     * =============================================================================================
     */
    void register() {
        String query = APIServices.INSTANCE.getQueryRegister(pf);
        isLoadConfigsComplete(APIs.web_signup_url, KeyVerification.caseSignUp, query);
    }

    /*
     * =============================================================================================
     * verify_thaiId
     * =============================================================================================
     */
    void verifyThaiId() {
        String query = APIServices.INSTANCE.getQueryVerify(pf);
        isLoadConfigsComplete(APIs.web_verify_thaiid_url, KeyVerification.caseVerify, query);
    }

    /*
     * =============================================================================================
     * recovery
     * =============================================================================================
     */
    void recovery() {
        String query = APIServices.INSTANCE.getQueryRecovery(pf);
        isLoadConfigsComplete(APIs.web_recovery_url, KeyVerification.caseRecovery, query);
    }

    void updatePassword(TrueIDInterface.ChangePasswordListener callback) {
        this.passwordListener = callback;
        String query = APIServices.INSTANCE.getQueryUpdatePassword(pf);
        isLoadConfigsComplete(APIs.web_update_password, KeyVerification.caseUpdatePassword, query);
    }

    /*
     * =============================================================================================
     * LoginInfo
     * =============================================================================================
     */
    void getLoginInfo(final boolean isRefreshToken, final boolean fromEvent, final String truemoney) {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                taskValidateJWTLoginInfo(isRefreshToken, fromEvent, truemoney);
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    private void taskValidateJWTLoginInfo(final boolean isRefreshToken, final boolean fromEvent, final String truemoney) {
        TaskValidateJWT task = new TaskValidateJWT(activity);
        task.setValidateJWTListener(new TaskValidateJWT.ValidateJWTListener() {
            @Override
            public void onSuccess(@Nullable String jsonProfile) {
                if (isRefreshToken) {
                    callActionListenerOnFinish(true);
                }
                /*
                 * Add JSON Truemoney
                 */
                if (!truemoney.isEmpty()) {
                    try {
                        JSONObject jsonTruemoney = new JSONObject(jsonProfile);
                        jsonTruemoney.put(KEYS.truemoney, truemoney);
                        jsonProfile = jsonTruemoney.toString();
                    } catch (JSONException e) {
                        Logcat.e(TAG, "JSONException : " + e);
                        e.printStackTrace();
                    }
                }

                listener.onLoginSuccess(jsonProfile, pf.getToken_expires_second());
                new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile, true);
            }

            @Override
            public void onError(@Nullable String msgErr, @NotNull JWTInvalidCase eCase) {
                Logcat.w(TAG, "JWT getLoginInfo() onFailed >> " + eCase.name() + "\n" + msgErr);
                switch (eCase) {
                    case EXPIRE:
                        String refreshTokenClient = pf.getRefresh_token_client();
                        RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_INFO);
                        break;
                    case NONE:
                        if (fromEvent) {
                            new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile, false);
                            listener.onLoginError(msgErr);
                        } else {
                            clearSession(ClearSession.ERROR, msgErr);
                        }
                        break;
                    case REVOKE:
                        new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile, false);
                        if (fromEvent) {
                            clearSession(ClearSession.REVOKE, msgErr);
                        } else {
                            if (isRefreshToken) callActionListenerOnFinish(false);
                            clearSession(ClearSession.ERROR, msgErr);
                        }
                        break;
                    case DATE_TIME:
                        JSONObject errorObject = UtilsSDK.getErrorObjectFromCode(activity, "480005");
                        UtilsSDK.openDateSetting(activity, errorObject);
                        break;
                }
            }
        });
        task.execute();
    }

    /*
     * =============================================================================================
     * getProfileMore
     * =============================================================================================
     */
    void getProfileMore() {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                taskValidateJWTProfileMore();
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    private void taskValidateJWTProfileMore() {
        TaskValidateJWT task = new TaskValidateJWT(activity);
        task.setValidateJWTListener(new TaskValidateJWT.ValidateJWTListener() {
            @Override
            public void onSuccess(@Nullable String jsonProfile) {
                getAccessInfoServer();
            }

            @Override
            public void onError(@Nullable String msgErr, @NotNull JWTInvalidCase eCase) {
                Logcat.w(TAG, "JWT getProfileMore() failed");
                switch (eCase) {
                    case EXPIRE:
                        String refreshTokenClient = pf.getRefresh_token_client();
                        RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_INFO_MORE);
                        break;
                    case REVOKE:
                        clearSession(ClearSession.REVOKE, msgErr);
                        new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile_More, false);
                        break;
                    case DATE_TIME:
                        JSONObject errorObject = UtilsSDK.getErrorObjectFromCode(activity, "480005");
                        UtilsSDK.openDateSetting(activity, errorObject);
                        break;
                    default:
                        listener.onGetInfoFailed(msgErr);
                        new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile_More, false);
                        break;
                }
            }
        });
        task.execute();
    }

    private void getAccessInfoServer() {
        TaskInfoServer info = new TaskInfoServer(activity);
        info.setOnTaskInfoServer(new TaskInfoServer.OnTaskInfoServer() {
            @Override
            public void onFailed(String error, boolean isInvalid) {
                if (isInvalid) {
                    String refreshTokenClient = pf.getRefresh_token_client();
                    RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_INFO_MORE);
                } else {
                    listener.onGetInfoFailed(error);
                    new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile_More, false);
                }
            }

            @Override
            public void onSuccess(String result, int expire) {
                listener.onGetInfoSuccess(result, expire);
                new AAATracking(ga, listener).event(KeysTracking.Events_Get_Profile_More, true);
            }
        });
        info.execute();
    }

    /*
     * =============================================================================================
     * Logout
     * =============================================================================================
     */
    protected void logout() {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                taskValidateJWTLogout();
            }

            @Override
            public void onNoneNetwork() {
            }
        }).execute();

        //clear session after client want to logout holding time 1500ms
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearSession(ClearSession.LOGOUT, null);
            }
        }, 1500);
    }

    private void taskValidateJWTLogout() {
        TaskValidateJWT task = new TaskValidateJWT(activity);
        task.setValidateJWTListener(new TaskValidateJWT.ValidateJWTListener() {
            @Override
            public void onSuccess(@Nullable String jsonProfile) {
                //call api logout before clear session
                callServerLogout(pf);
            }

            @Override
            public void onError(@Nullable String msgErr, @NotNull JWTInvalidCase eCase) {
            }
        });
        task.execute();
    }

    private void callServerLogout(PreferencesTrueID pf) {
        new TaskLogout(activity, pf).execute();
    }

    /*
     * =============================================================================================
     * RefreshAccessTokenClient
     * =============================================================================================
     */
    private void RefreshAccessTokenClient(final String refreshToken, final boolean isRoot, final String refreshCase) {
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                taskRefreshAccessTokenClient(refreshToken, isRoot, refreshCase);
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    private void taskRefreshAccessTokenClient(final String refreshToken, final boolean isRoot, final String refreshCase) {
        TaskRefreshToken taskRefresh = new TaskRefreshToken(activity.getApplicationContext(), refreshToken, isRoot);
        taskRefresh.setOnTaskRefreshToken(new TaskRefreshToken.RefreshListener() {
            @Override
            public void onSucceed() {
                new AAATracking(ga, listener).event(KeysTracking.Events_Refresh_Token, true);
                switch (refreshCase) {
                    case KeyRefreshToken.CASE_LOGOUT:
                        logout();
                        break;
                    case KeyRefreshToken.CASE_EXPIRE:
                        listener.onRefreshAccessToken(true);
                        callActionListenerOnFinish(true);
                        break;
                    case KeyRefreshToken.CASE_INFO_MORE:
                        getAccessInfoServer();
                        break;
                    case KeyRefreshToken.CASE_GET_ACCESS_TOKEN:
                        if (verifyListener != null)
                            verifyListener.onVerifySuccess(pf.getAccess_token_client());
                        break;
                    case KeyRefreshToken.CASE_MAPPING:
                        listener.onMappingSuccess(MappingCase.SUCCESS.getValue());
                        break;
                    case KeyRefreshToken.CASE_FORCE_REFRESH:
                        if (refreshTokenListener != null) {
                            refreshTokenListener.onSuccess(pf.getAccess_token_client());
                        }
                        break;
                    default:
                        listener.onRefreshAccessToken(true);
                        getLoginInfo(true, false, "");
                        break;
                }
            }

            @Override
            public void onFailed(String error, boolean isRevoke) {
                new AAATracking(ga, listener).event(KeysTracking.Events_Refresh_Token, false);
                if (isRevoke) {
                    clearSession(ClearSession.REVOKE, null);
                    return;
                }
                switch (refreshCase) {
                    case KeyRefreshToken.CASE_INFO_MORE:
                        listener.onGetInfoFailed(error);
                        break;
                    case KeyRefreshToken.CASE_GET_ACCESS_TOKEN:
                        if (verifyListener != null) verifyListener.onVerifyFailed(error);
                        break;
                    case KeyRefreshToken.CASE_MAPPING:
                        listener.onMappingFailed(MappingCase.FAILED.getValue());
                        break;
                    default:
                        listener.onRefreshAccessTokenFailed(error);
                        callActionListenerOnFinish(false);
                        break;
                }
            }
        });
        taskRefresh.taskExecute();
    }

    /*
     * =============================================================================================
     * Self Verify (Access Token Alive)
     * =============================================================================================
     */
    void selfVerify(final VerifyListener verifyListener) {
        this.verifyListener = verifyListener;
        new TaskCheckNetwork(activity, new TaskCheckNetwork.OnCheckNetWorkListener() {
            @Override
            public void onHaveNetwork() {
                taskValidateJWTSelfVerify();
            }

            @Override
            public void onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.getLanguage());
            }
        }).execute();
    }

    private void taskValidateJWTSelfVerify() {
        TaskValidateJWT task = new TaskValidateJWT(activity);
        task.setValidateJWTListener(new TaskValidateJWT.ValidateJWTListener() {
            @Override
            public void onSuccess(@Nullable String jsonProfile) {
                verifyListener.onVerifySuccess(pf.getAccess_token_client());
            }

            @Override
            public void onError(@Nullable String msgErr, @NotNull JWTInvalidCase eCase) {
                Logcat.w(TAG, "JWT selfVerify() failed");
                switch (eCase) {
                    case EXPIRE:
                        String refreshTokenClient = pf.getRefresh_token_client();
                        RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_GET_ACCESS_TOKEN);
                        break;
                    case REVOKE:
                        clearSession(ClearSession.REVOKE, msgErr);
                        break;
                    case DATE_TIME:
                        JSONObject errorObject = UtilsSDK.getErrorObjectFromCode(activity, "480005");
                        UtilsSDK.openDateSetting(activity, errorObject);
                        break;
                    default:
                        verifyListener.onVerifyFailed(msgErr);
                        break;
                }
            }
        });
        task.execute();
    }

    /*
     * =============================================================================================
     * Force refresh access token
     * =============================================================================================
     */
    void forceRefresh(String key, TrueIDInterface.RefreshTokenListener callback) {
        refreshTokenListener = callback;

        String tokenClient = pf.getAccess_token_client();
        String refreshTokenClient = pf.getRefresh_token_client();

        //Check login state
        if (tokenClient.equalsIgnoreCase("")) {
            refreshTokenListener.onError("Fore refresh as existing login");
            return;
        }

        //Check key matching state
        if (!key.contains(APIs.sc)) {
            refreshTokenListener.onError("Key not matching.");
            return;
        }

        RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_FORCE_REFRESH);
    }

    /*
     * =============================================================================================
     * OnResume
     * =============================================================================================
     */

    void onResumeSelf() {
        String accessTokenClient = pf.getAccess_token_client(); //checking case timeout and other application want not access token
        String refreshTokenClient = pf.getRefresh_token_client();

        String appPackage = pf.getPackage_name();
        long pfTimeStamp = pf.getCache_timestamp();

        long cTimeStamp = UtilsSDK.getBestCurrentTime(activity);
        if (cTimeStamp == 0L) {
            JSONObject errorObject = UtilsSDK.getErrorObjectFromCode(activity, "480005");
            UtilsSDK.openDateSetting(activity, errorObject);
//            listener.onRefreshAccessTokenFailed(errorObject.toString());
        }

        if (accessTokenClient.isEmpty()) {
            Logcat.i(TAG, "Self Case : this " + appPackage + " access token client is empty");
            callActionListenerOnFinish(true);
            return;
        }

        if (cTimeStamp > pfTimeStamp) {
            Logcat.i(TAG, "Self Case : this " + appPackage + " has been auto refresh token with over timestamp");
            RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_EXPIRE);
            return;
        }

        Logcat.i(TAG, "Self Case : this " + appPackage + " token it have availability");
        callActionListenerOnFinish(true);
    }

    void onResume() {
        if (!UtilsSDK.isWriteExternalPermission(activity)) {
            onResumeSelf();
            return;
        }

        String accessTokenRoot = pf.getAccess_token_root(); //checking case update version
        String refreshTokenRoot = pf.getRefresh_token_root(); //use case update v3 to v4
        String accessTokenClient = pf.getAccess_token_client(); //checking case timeout and other application want not access token
        String refreshTokenClient = pf.getRefresh_token_client();

        String appPackage = pf.getPackage_name();
        long pfTimeStamp = pf.getCache_timestamp();
        boolean isFirst = pf.getIsFirstInstall();

        /*
         * =========================================================================================
         * MANAGE FILE
         * =========================================================================================
         * */

        /*get list package from dir with same package inside device*/
        ArrayList<String> listPackage = FileSDK.getFilesDirectory(activity);

        /*get file path*/
        File filePackage = FileSDK.getFilePath(activity, appPackage);
        File fileToken = FileSDK.getFileToken(activity);

        /*checking for delete root file because all application deleted*/
        if ((!isFirst && filePackage.exists() && fileToken.exists()) ||
                (!isFirst && !filePackage.exists() && fileToken.exists() && listPackage.size() == 0)) {
            Logcat.e(TAG, "SSO Case : this " + appPackage + " deleted file root token. because all application deleted");
            FileSDK.deleteFile(fileToken);
        }

        /*add file package in center file, for checking sdk version 4*/
        if (!isFirst) {
            pf.setIsFirstInstall(true);
            FileSDK.writeFile(activity, filePackage);
        }

        /*
         * =========================================================================================
         * CHECKING CASE FOR AUTHENTICATION
         * =========================================================================================
         * */

        /*auto refresh token from v3 login to v4*/
        if (!accessTokenRoot.isEmpty() && (!accessTokenRoot.contains(".") || accessTokenRoot.contains(KEYS.KEYTOKENVERSION3))) {
            Logcat.i(TAG, "SSO Case : this " + appPackage + " goto refresh token. because update from SDK version 3");
            pf.setAccess_token_root(""); //clear "access_token_root" because update version 3 to 4 only
            RefreshAccessTokenClient(refreshTokenRoot, true, KeyRefreshToken.CASE_OLD_VERSION);
            return;
        }

        /*checking single sign on process*/
        if (fileToken.exists()) {
            try {
                long cTimeStamp = UtilsSDK.getBestCurrentTime(activity);
                if (cTimeStamp == 0L) {
                    JSONObject errorObject = UtilsSDK.getErrorObjectFromCode(activity, "480005");
                    UtilsSDK.openDateSetting(activity, errorObject);
//                    listener.onRefreshAccessTokenFailed(errorObject.toString());
                }

                JSONObject jsonObject = new JSONObject(FileSDK.readFile(activity, fileToken));
                String rootToken9 = jsonObject.getString(KEYS.root_refresh_token); //use token from data center (9 refresh token)

                if (accessTokenClient.isEmpty()) {
                    Logcat.i(TAG, "SSO Case : this " + appPackage + " auto refresh token because. other application is login");
                    RefreshAccessTokenClient(rootToken9, true, KeyRefreshToken.CASE_SINGLE_SIGN_ON);
                    return;
                }

                if (!rootToken9.equals(refreshTokenRoot)) {
                    Logcat.i(TAG, "SSO Case : this " + appPackage + " refresh token with case other application forgot password or change user");
                    RefreshAccessTokenClient(rootToken9, true, KeyRefreshToken.CASE_RESET_PASSWORD);
                    return;
                }

                if (cTimeStamp > pfTimeStamp) {
                    Logcat.i(TAG, "SSO Case : this " + appPackage + " has been auto refresh token with over timestamp");
                    RefreshAccessTokenClient(refreshTokenClient, false, KeyRefreshToken.CASE_EXPIRE);
                    return;
                }

                Logcat.i(TAG, "SSO Case : this " + appPackage + " token it have availability");
                callActionListenerOnFinish(true);
            } catch (JSONException e) {
                e.printStackTrace();
                Logcat.e(TAG, e.toString());
            }
        } else {
            if (accessTokenClient.isEmpty()) {
                Logcat.i(TAG, "SSO Case : this " + appPackage + " access token client is empty");
                callActionListenerOnFinish(true);
                return;
            }

            Logcat.i(TAG, "SSO Case : this " + appPackage + " auto logout because other application logout");
            new AAATracking(ga, listener).event(KeysTracking.Events_Logout, true);

            clearSession(ClearSession.REVOKE, null);
        }
    }

    /*
     * =============================================================================================
     * QR Scanner
     * =============================================================================================
     */
    void openQRScanner(QRScanListener qrScanListener) {
        QRScannerActivity.openCamera(activity, qrScanListener, listener);
    }

    void openQRGeneral(TrueIDInterface.QRGeneralListener qrScanListener) {
        QRGeneralActivity.openCamera(activity, qrScanListener);
    }

    void sendQRData(String json, QRScanListener qrScanListener) {
        QRScannerActivity.openSendingQR(activity, json, qrScanListener, listener);
    }

    /*
     * =============================================================================================
     * TMN Binding
     * =============================================================================================
     */
    void bindingTrueMoney(final Context context, final String bindingIds,
                          final TrueIDInterface.BindingTrueMoneyListener bindingTMNCallback) {

        new ValidateJwtToken(context).setValidateJwtTokenListener(new ValidateJwtToken.ValidateJwtTokenListener() {
            @Override
            public void onSuccess(@Nullable String payload) {
                bindingTrueWallet(context, bindingIds, bindingTMNCallback);
            }

            @Override
            public void onError(@Nullable String msgErr) {
                if (bindingTMNCallback != null) {
                    bindingTMNCallback.onBindingError(msgErr);
                }
            }

            @Override
            public void onRevoke() {
                clearSession(ClearSession.REVOKE, null);
            }
        }).check();

    }

    private void bindingTrueWallet(final Context context, String bindingIds,
                                   final TrueIDInterface.BindingTrueMoneyListener bindingTMNCallback) {

        TaskBindingTrueMoney taskBindingTrueMoney = new TaskBindingTrueMoney(context, bindingIds);
        taskBindingTrueMoney.setBindingTrueMoneyListener(new TaskBindingTrueMoney.BindingTrueMoneyListener() {
            @Override
            public void onSuccess(boolean isOK) {
                if (isOK) {
                    refreshTokenWallet(context, pf.getRefresh_token_client(), bindingTMNCallback);
                } else {
                    String accessToken = pf.getAccess_token_client();
                    if (bindingTMNCallback != null) {
                        bindingTMNCallback.onBindingSuccess(accessToken);
                    }
                }
            }

            @Override
            public void onError(@NotNull String msg) {
                if (bindingTMNCallback != null) {
                    bindingTMNCallback.onBindingError(msg);
                }
            }
        });
        taskBindingTrueMoney.execute();
    }

    /**
     * @param withCase callback with clear session case
     * @param msg      message sent to client show error
     */
    private void clearSession(ClearSession withCase, String msg) {
        new AAATracking(ga, listener).event(KeysTracking.Events_Logout, true);
        UtilsSDK.clearSession(activity);
        callActionListenerOnFinish(true);

        switch (withCase) {
            case LOGOUT:
                if (listener == null) return;
                listener.onLogoutRespond(true, msg);
                break;
            case REVOKE:
                listener.onRevokeAlready();
                break;
            case ERROR:
                listener.onLoginError(msg);
                break;
        }
    }

    private void callActionListenerOnFinish(boolean isSuccess) {
        if (actionListener == null) return;
        Logcat.d(TAG, "Action Listener > onFinish : " + isSuccess);
        actionListener.onFinish(isSuccess);
    }

    /*
     * =============================================================================================
     * RefreshToken Zone
     * =============================================================================================
     */

    private void refreshTokenWallet(Context context, String refreshToken,
                                    final TrueIDInterface.BindingTrueMoneyListener bindingTMNCallback) {
        new ValidateJwtToken(context).setValidateJwtTokenListener(new ValidateJwtToken.ValidateJwtTokenListener() {
            @Override
            public void onSuccess(@Nullable String payload) {
                String accessToken = pf.getAccess_token_client();
                if (bindingTMNCallback != null) {
                    bindingTMNCallback.onBindingSuccess(accessToken);
                }
            }

            @Override
            public void onError(@Nullable String msgErr) {
                if (bindingTMNCallback != null) {
                    bindingTMNCallback.onBindingError(msgErr);
                }
            }

            @Override
            public void onRevoke() {
                clearSession(ClearSession.REVOKE, null);
            }
        }).fetch(refreshToken, false);
    }
}
