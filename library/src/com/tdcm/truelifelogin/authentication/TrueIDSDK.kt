package com.tdcm.truelifelogin.authentication

import com.tdcm.truelifelogin.BuildConfig

/*
 * Created by boyDroids on 5/11/2018 AD.
 * ^.^
 */
object TrueIDSDK {

    fun versionCode(): Int {
        return BuildConfig.VERSION_CODE
    }

    fun versionName(): String {
        return BuildConfig.VERSION_NAME
    }

    fun onApplicationCreate() {
        isTokenRefreshing = false
    }

    fun isTokenRefreshing(): Boolean {
        return isTokenRefreshing
    }

    internal fun startRefreshTokenTask() {
        isTokenRefreshing = true
    }

    internal fun finishRefreshTokenTask() {
        isTokenRefreshing = false
    }

    private var isTokenRefreshing = false
}