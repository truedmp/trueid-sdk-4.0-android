package com.tdcm.truelifelogin.authentication

import android.content.Context
import com.tdcm.truelifelogin.tasks.TaskRefreshToken
import com.tdcm.truelifelogin.constants.JWTInvalidCase
import com.tdcm.truelifelogin.httpclient.Validate
import com.tdcm.truelifelogin.tasks.TaskValidateJWT
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.UtilsSDK

/*
 * Created by boyDroids on 5/6/2018 AD.
 * ^.^
 */
class AuthTokensManager(val context: Context?) {

    interface AuthListener {
        /**
         * @param accessToken this access token client used for everything api access
         * */
        fun onAuthSuccess(accessToken: String?)

        /**
         * @param errorMessage this is string error message for client
         * @param isRevoke this return "true". please clear session in client side
         * */
        fun onAuthFailed(errorMessage: String?, isRevoke: Boolean)
    }

    private var mCallback: AuthListener? = null

    companion object {
        private val TAG = AuthTokensManager::class.java.simpleName
    }

    private var pf: PreferencesTrueID? = null

    init {
        context?.let {
            pf = PreferencesTrueID(it)
        }
    }

    /**
     * this function AuthTokensManager use for get access token it be verify.
     * and open listener for receive callback access token
     * */
    fun get(callback: AuthListener?) {
        if (context == null) {
            Validate.printNull("Context >> get(...) in AuthTokensManager")
            return
        }
        mCallback = callback

        TaskValidateJWT(context)
                .setValidateJWTListener(object : TaskValidateJWT.ValidateJWTListener {
                    override fun onSuccess(jsonProfile: String?) {
                        Logcat.d(TAG, "verifyToken > onSucceed")
                        mCallback?.onAuthSuccess(pf?.access_token_client)
                    }

                    override fun onError(msgErr: String?, eCase: JWTInvalidCase) {
                        Logcat.d(TAG, "verifyToken > onFailed : $msgErr")
                        when (eCase) {
                            JWTInvalidCase.EXPIRE -> {
                                refreshToken(pf?.refresh_token_client)
                            }
                            JWTInvalidCase.REVOKE -> {
                                mCallback?.onAuthFailed(msgErr, true)
                            }
                            else -> {
                                mCallback?.onAuthFailed(msgErr, false)
                            }
                        }
                    }
                })
                .execute()
    }

    /**
     * for now "Not Support" root refresh token
     * */
    private fun refreshToken(refreshToken: String?) {
        if (context == null) {
            Validate.printNull("Context >> refreshToken(...) in AuthTokensManager")
            return
        }
        val task = TaskRefreshToken(context, refreshToken!!, false)
        task.setOnTaskRefreshToken(object : TaskRefreshToken.RefreshListener {
            override fun onSucceed() {
                Logcat.d(TAG, "refreshToken > onSucceed")
                mCallback?.onAuthSuccess(pf?.access_token_client)
            }

            override fun onFailed(error: String, isRevoke: Boolean) {
                Logcat.w(TAG, "refreshToken > onFailed : $error")
                if (isRevoke) UtilsSDK.clearSession(context)

                mCallback?.onAuthFailed(error, isRevoke)
            }
        })
        task.taskExecute()
    }
}