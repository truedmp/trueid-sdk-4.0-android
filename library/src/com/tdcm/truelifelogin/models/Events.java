package com.tdcm.truelifelogin.models;

import java.io.Serializable;

/*
 * Created by Pratanporn on 10/5/2017 AD.
 */

/**
 * This class To keep User action.
 */
public class Events implements Serializable {

    private String Category;
    private String Action;
    private String Label;

    public Events(String category, String action, String label) {
        this.Category = category;
        this.Action = action;
        this.Label = label;
    }

    /**
     * function Category is title on action
     *
     * @return Category
     */
    public String getCategory() {
        return Category;
    }

    /**
     * function getAction is User Action
     *
     * @return Action
     */
    public String getAction() {
        return Action;
    }

    /**
     * function getLabel is Label
     *
     * @return Label
     */
    public String getLabel() {
        return Label;
    }

}
