package com.tdcm.truelifelogin.models;

import java.io.Serializable;

/*
 * Created by Pratanporn on 10/5/2017 AD.
 */

/**
 * This class To keep User action Screens.
 */
public class Screens implements Serializable {

    private String ScreenName;

    public Screens(String screenName) {
        ScreenName = screenName;
    }

    /**
     * function Category will return ScreenName for User Action
     *
     * @return ScreenName
     */
    public String getScreenName() {
        return ScreenName;
    }
}
