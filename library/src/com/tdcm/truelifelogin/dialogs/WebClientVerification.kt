package com.tdcm.truelifelogin.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Bitmap
import android.net.http.SslError
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import com.tdcm.truelifelogin.constants.KeyVerification
import com.tdcm.truelifelogin.utils.Logcat
import java.net.URLDecoder

/*
 * Created by boyDroids on 5/4/2018 AD.
 * ^.^
 */
internal class WebClientVerification(val activity: Activity?,
                            private val callback: OnVerificationClientListener) : WebViewClient() {

    companion object {
        private val TAG = WebClientVerification::class.java.simpleName
    }

    interface OnVerificationClientListener {
        fun onDataResult(data: String, type: String)
        fun onPageStarted(url: String)
        fun onPageFinished(url: String)
        fun onReceivedError(errorCode: Int, description: String, failingUrl: String)
    }

    val waitTime = 30000
    var timeOut = false

    override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
        if (url == null) return false
        Logcat.i(TAG, "raw : $url")

        if (KeyVerification.isSuccess(url)) {
            val decodeUrl = URLDecoder.decode(url, "UTF-8")
            val type = decodeUrl.substringBefore("{")
            val result = decodeUrl.substringAfter("?")

            callback.onDataResult(result, type)
            return true
        }
        return false
    }

    override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
        if (activity == null) return
        AlertDialog.Builder(activity)
                .setMessage("There is a problem with this website certificate.\nPress continue to proceed.")
                .setPositiveButton("Continue") { _, _ -> handler?.proceed() }
                .setNegativeButton("Close") { _, _ ->
                    handler?.cancel()
                    val urlNotNull = view?.url ?: "web view null from ssl error"

                    callback.onReceivedError(WebViewClient.ERROR_FAILED_SSL_HANDSHAKE,
                            "ERROR_FAILED_SSL_HANDSHAKE",
                            urlNotNull)
                }
                .create()
                .show()
        super.onReceivedSslError(view, handler, error)
    }

    override fun onReceivedError(view: WebView?, errorCode: Int, description: String, failingUrl: String) {
        Logcat.e(TAG, "\nonReceivedError <errorCode> : $errorCode\n" +
                "onReceivedError <description> : $description\n" +
                "onReceivedError <failingUrl> : $failingUrl")

        callback.onReceivedError(errorCode, description, failingUrl)
        super.onReceivedError(view, errorCode, description, failingUrl)
    }

    override fun onPageStarted(view: WebView?, url: String, favicon: Bitmap?) {
        callback.onPageStarted(url)
        Thread(object : Runnable {
            override fun run() {
                synchronized(this) {
                    try {
                        waitTime.toLong() // 30 seconds
                        activity?.runOnUiThread {
                            if (timeOut) {
                                callback.onReceivedError(WebViewClient.ERROR_TIMEOUT,
                                        "ERROR_TIMEOUT",
                                        url)
                            }
                        }
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }

                }
            }
        }).start()
        super.onPageStarted(view, url, favicon)
    }

    override fun onPageFinished(view: WebView?, url: String) {
        timeOut = false
        callback.onPageFinished(url)
        super.onPageFinished(view, url)
    }
}