package com.tdcm.truelifelogin.dialogs

import android.app.Activity
import android.app.Dialog
import android.view.KeyEvent
import android.view.Window
import android.view.WindowManager
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.interfaces.LoginServiceListener
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import kotlinx.android.synthetic.main.webview_verification.*

/*
 * Created by boyDroids on 9/4/2018 AD.
 * ^.^
 */
internal class DialogTermAndCondition(val activity: Activity, openUrl: String, val pf: PreferencesTrueID)
    : Dialog(activity) {

    companion object {
        private val TAG = DialogTermAndCondition::class.java.simpleName
    }

    private var serviceListener: LoginServiceListener? = null

    fun setOnDialogTermAndCondition(serviceListener: LoginServiceListener) {
        this.serviceListener = serviceListener
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.webview_term_and_condition)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.domStorageEnabled = true

        setOnCancelListener {
            dismiss()
            if (serviceListener != null) {
                serviceListener?.onCanceled()
            } else {
                Logcat.i(TAG, activity.getString(R.string.trueid_error_implement, pf.client_class, "onCanceled()"))
            }
        }

        /*load webUrl*/
        webView.loadUrl(openUrl)
        webView.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when(keyCode){
                    KeyEvent.KEYCODE_BACK -> dismiss()
                }
            }
            return@setOnKeyListener false
        }

        btnClose.setOnClickListener {
            dismiss()
        }
    }
}