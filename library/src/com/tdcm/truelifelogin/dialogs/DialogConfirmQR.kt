package com.tdcm.truelifelogin.dialogs

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.KeyConfigs
import kotlinx.android.synthetic.main.dialog_confirm_qr.*
import org.json.JSONException
import org.json.JSONObject

/*
 * Created by boyDroids on 11/7/2018 AD.
 * ^.^
 */
internal class DialogConfirmQR(context: Context, msgObject: JSONObject?) :
        Dialog(context, android.R.style.Theme_Light) {

    companion object {
        private val TAG = DialogConfirmQR::class.java.simpleName
    }

    interface ConfirmQRListener {
        fun onConfirm()
        fun onCancel()
    }

    private var callback: ConfirmQRListener? = null

    fun setConfirmQRListener(callback: ConfirmQRListener) {
        this.callback = callback
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_confirm_qr)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        try {
            val textHeader = msgObject?.getString(KeyConfigs.qrScanLogin)
            val textMsg = msgObject?.getString(KeyConfigs.qrConfirmMsg1)
            val btnMsg = msgObject?.getString(KeyConfigs.qrConfirmButton)

            textViewHeader.text = textHeader ?: "null"
            textViewMessage.text = textMsg ?: "null"
            buttonConfirm.text = btnMsg ?: "null"
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        setOnCancelListener {
            dismiss()
            callback?.onCancel()
        }

        buttonConfirm.setOnClickListener {
            dismiss()
            callback?.onConfirm()
        }
    }
}