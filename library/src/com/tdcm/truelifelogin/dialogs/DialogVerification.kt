package com.tdcm.truelifelogin.dialogs

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.webkit.WebViewClient
import android.widget.Toast
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.KeyVerification
import com.tdcm.truelifelogin.constants.MappingCase
import com.tdcm.truelifelogin.constants.SDKEnvironment
import com.tdcm.truelifelogin.interfaces.LoginServiceListener
import com.tdcm.truelifelogin.interfaces.TrueIDInterface
import com.tdcm.truelifelogin.tasks.TaskExchangeAuthorization
import com.tdcm.truelifelogin.tasks.TaskGetTokenFromCode
import com.tdcm.truelifelogin.utils.*
import kotlinx.android.synthetic.main.webview_verification.*
import org.json.JSONException
import org.json.JSONObject

/*
 * Created by boyDroids on 5/4/2018 AD.
 * ^.^
 */
@SuppressLint("InlinedApi")
internal class DialogVerification(val activity: Activity,
                                  private val ga: TrackingService?,
                                  val pf: PreferencesTrueID,
                                  private var openUrl: String,
                                  funName: String) : Dialog(activity, android.R.style.Theme_DeviceDefault_NoActionBar), WebClientVerification.OnVerificationClientListener {

    companion object {
        private val TAG = DialogVerification::class.java.simpleName
    }

    interface OnDialogVerification {
        fun onGetInfo(truemoney: String)
        fun openDialog(url: String)
        fun onRefreshToken()
        fun onOpenNative()
        fun onRevoke()
    }

    private var callback: OnDialogVerification? = null
    private var serviceListener: LoginServiceListener? = null
    private var passwordListener: TrueIDInterface.ChangePasswordListener? = null

    fun setOnDialogVerification(serviceListener: LoginServiceListener, callback: OnDialogVerification) {
        this.serviceListener = serviceListener
        this.callback = callback
    }

    fun setOnChangePassword(passwordListener: TrueIDInterface.ChangePasswordListener?) {
        this.passwordListener = passwordListener
    }

    private var onPage = ""
    private var state: String? = null
    private var codeVerifier: String? = null
    private var isRevoke = false

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.webview_verification)

        //Check case with can't load webView url
        if (openUrl.startsWith("null")) {
            Handler().postDelayed({
                when (funName) {
                    KeyVerification.caseSignIn -> {
                        APIs.web_signin_url?.let { domain ->
                            openUrl = openUrl.replaceBefore('?', domain)
                        }
                    }
                    KeyVerification.caseSignUp -> {
                        APIs.web_signup_url?.let { domain ->
                            openUrl = openUrl.replaceBefore('?', domain)
                        }
                    }
                    KeyVerification.caseVerify -> {
                        APIs.web_verify_thaiid_url?.let { domain ->
                            openUrl = openUrl.replaceBefore('?', domain)
                        }
                    }
                    KeyVerification.caseRecovery -> {
                        APIs.web_recovery_url?.let { domain ->
                            openUrl = openUrl.replaceBefore('?', domain)
                        }
                    }
                    KeyVerification.caseUpdatePassword -> {
                        APIs.web_update_password?.let { domain ->
                            openUrl = openUrl.replaceBefore('?', domain)
                        }
                    }
                }
                progressBar.visibility = View.INVISIBLE
                renderWebView()
            }, 2000)
            progressBar.visibility = View.VISIBLE
        } else {
            renderWebView()
        }
    }

    private fun renderWebView() {
        if (openUrl.startsWith("null")) {
            Toast.makeText(activity, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show()
            dismiss()
        }

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.domStorageEnabled = true

        setOnCancelListener {
            webView.url?.let { _url ->
                tagCancelPage(_url)
                serviceListener?.onCanceled()
                dismiss()
            }
        }

        /* showing environment inside webView */
        when (pf.environment) {
            SDKEnvironment.ALPHA.value,
            SDKEnvironment.STAGING.value -> textEnvironment.text = pf.environment.toUpperCase()
            else -> textEnvironment.visibility = View.INVISIBLE
        }

        val verifierObject = Auth0.snipVerifierObject(openUrl)
        codeVerifier = verifierObject.getString(KEYS.AUTH0_CODE_VERIFIER)
        state = verifierObject.getString(KEYS.AUTH0_STATE)
        val authURL = verifierObject.getString(KEYS.AUTH0_URL)

        if (authURL.contains(KeyVerification.caseUpdatePassword, ignoreCase = true)) {
            /*Header*/
            val headers = HashMap<String, String>().apply {
                put("Authorization", "Bearer ${pf.access_token_client}")
            }
            /*load webUrl*/
            webView.loadUrl(authURL, headers)
            Logcat.i(TAG, "raw start url with header : $authURL")
        } else {
            /*load webUrl*/
            webView.loadUrl(authURL)
            Logcat.i(TAG, "raw start url no header : $authURL")
        }
        webView.webViewClient = WebClientVerification(activity, this@DialogVerification)

        btnClose.setOnClickListener {
            cancel()
        }

        btnBack.setOnClickListener {
            onKeyDown(KeyEvent.KEYCODE_BACK, KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK))
        }

        authURL?.let {
            onPage = it
        }
    }

    override fun onDataResult(data: String, type: String) {
        Logcat.w(TAG, "data : $data")
        Logcat.w(TAG, "type : $type")
        try {

            val jsonObj = if (!data.isEmpty() && data.startsWith("{")) JSONObject(data) else JSONObject()
            if (jsonObj.has(KEYS.ERROR)) {
                val errorObj = UtilsSDK.getErrorObject(activity,
                        jsonObj.getJSONObject(KEYS.ERROR))
                Logcat.e(TAG, "JSON Error : $errorObj")
                dismiss()
                return
            }

            tagEndPage(type)

            /*checking first step*/
            when {
                type.contains(KeyVerification.codeSuccess) -> {
                    if (jsonObj.has(KEYS.AUTH0_STATE) && jsonObj.has(KEYS.AUTH0_CODE)) {
                        val stateServer = jsonObj.getString(KEYS.AUTH0_STATE)
                        val code = jsonObj.getString(KEYS.AUTH0_CODE)
                        if (stateServer == state) {
                            exchangeAuthorization(codeVerifier!!, code)
                        } else {
                            serviceListener?.onLoginError(UtilsSDK.getErrorObjectFromCode(context, "1400180").toString())
                            dismiss()
                        }
                    } else {
                        Logcat.e(TAG, "KeyJSON : onFailed Not have Key")
                        serviceListener?.onLoginError(UtilsSDK.getErrorObjectFromCode(context, "1400180").toString())
                        dismiss()
                    }
                    return
                }
                type.contains(KeyVerification.resLogin) -> {
                    gettingInfo(jsonObj)
                    return
                }
                type.contains(KeyVerification.resForgotPassword) ||
                        type.contains(KeyVerification.resRegister) -> {
                    if (jsonObj.has(KEYS.login_code)) {
                        val loginCode = jsonObj.getString(KEYS.login_code)
                        val clientId = jsonObj.getString(KEYS.client_id)

                        val isForgot = type.contains(KeyVerification.resForgotPassword)
                        if (pf.isAutoLogin) {
                            /* truemoney */
                            val tmValue = checkObjTruemoney(jsonObj)

                            getResponseFromCode(loginCode, tmValue, isForgot)
                        } else {
                            closeWeb(isForgot, loginCode, clientId)
                        }
                    }
                    return
                }
                type.contains(KeyVerification.resUpdatePassword) -> {
                    UtilsSDK.setPreferenceToken(jsonObj, activity, true)
                    if (jsonObj.has(KEYS.access_token)) {
                        val accessToken = jsonObj.getString(KEYS.access_token)
                        passwordListener?.onChangePasswordSuccess(accessToken)
                    } else {
                        passwordListener?.onChangePasswordError("error")
                    }
                    dismiss()
                    return
                }
                type.contains(KeyVerification.resMapFailed) -> {
                    serviceListener?.onMappingFailed(MappingCase.FAILED.value)
                    dismiss()
                    return
                }
                type.contains(KeyVerification.resMapAlready) -> {
                    serviceListener?.onMappingAlready(MappingCase.ALREADY.value)
                    dismiss()
                    return
                }
                type.contains(KeyVerification.resMapSuccess) -> {
                    callback?.onRefreshToken()
                    dismiss()
                    return
                }
                type.contains(KeyVerification.casePolicy) -> {
                    callback?.openDialog(APIs.policy_th_url)
                    return
                }
                type.contains(KeyVerification.caseTermOfUse) -> {
                    callback?.openDialog(APIs.term_th_url)
                    return
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            Logcat.e(TAG, e.toString())
            dismiss()
        }
    }

    private fun gettingInfo(jsonObject: JSONObject) {
        UtilsSDK.setPreferenceToken(jsonObject, activity)
        val tmValue = checkObjTruemoney(jsonObject)
        callback?.onGetInfo(tmValue)
        dismiss()
    }

    private fun checkObjTruemoney(jsonObj: JSONObject): String {
        var tmValue = ""
        /* Check truemoney Value */
        if (jsonObj.has(KEYS.truemoney) && jsonObj.getString(KEYS.truemoney) == "register") {
            tmValue = jsonObj.getString(KEYS.truemoney)
        }
        return tmValue
    }

    private fun closeWeb(isForgot: Boolean, loginCode: String, clientId: String) {
        if (isForgot) {
            clearProfileData()
            serviceListener?.onForgotSuccess(loginCode, clientId)
        } else {
            serviceListener?.onRegisterSuccess(loginCode, clientId)
        }
        dismiss()
    }

    private fun getResponseFromCode(loginCode: String, truemoneyValue: String, isForgot: Boolean) {
        val taskFromCode = TaskGetTokenFromCode(context, loginCode)
        taskFromCode.setOnGetToken(object : TaskGetTokenFromCode.OnGetToken {
            override fun onSuccess(jsonObj: JSONObject) {
                truemoneyValue.let {
                    jsonObj.put(KEYS.truemoney, truemoneyValue)
                }
                gettingInfo(jsonObj)
            }

            override fun onError(msg: String) {
                if (isForgot) {
                    serviceListener?.onForgotError(msg)
                } else {
                    serviceListener?.onRegisterError(msg)
                }
                dismiss()
            }
        })
        taskFromCode.execute()
    }

    override fun onReceivedError(errorCode: Int, description: String, failingUrl: String) {
        when (errorCode) {
            WebViewClient.ERROR_CONNECT, WebViewClient.ERROR_TIMEOUT -> {
                gotoNative(description)
            }
            else -> {
                notResponse(description)
            }
        }
    }

    override fun onPageStarted(url: String) {
        showProgress()
    }

    override fun onPageFinished(url: String) {
        ga?.let {
            AAATracking(it, serviceListener).screen(activity, it.TrackingScreensURL(url))
        }

        hideProgress()
        tagStartPage(url)
        checkBackStackPage(url)
        isShowingClose(url)

        if (url.contains("logout") || url.contains("passwordsuccess")) {
            isRevoke = true
        }
    }

    private fun gotoNative(msg: String?) {
        Logcat.e(TAG, "error message : $msg")
        callback?.onOpenNative()
        dismiss()
    }

    private fun notResponse(msg: String?) {
        Logcat.e(TAG, "not response error : $msg")
        AlertDialog.Builder(activity)
                .setMessage("Service is not response.\nPlease try again")
                .setPositiveButton("OK") { _, _ -> cancel() }
                .setCancelable(false)
                .create()
                .show()
    }

    private fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    private fun isShowingClose(currentUrl: String) {
        if (currentUrl.contains("success?")) {
            btnClose.visibility = View.INVISIBLE
        } else {
            btnClose.visibility = View.VISIBLE
        }
    }

    private fun checkBackStackPage(currentUrl: String) {
        if ((onPage.contains(KeyVerification.caseSignIn) && currentUrl.contains(KeyVerification.caseRecovery))
                || (onPage.contains(KeyVerification.caseRecovery) && currentUrl.contains(KeyVerification.caseRecoveryOTP))
                || (onPage.contains(KeyVerification.caseRecoveryOTP) && currentUrl.contains(KeyVerification.caseRecovery))
                || (onPage.contains(KeyVerification.caseSignUp) && currentUrl.contains(KeyVerification.caseSignOTP))) {
            btnBack.visibility = View.VISIBLE
        } else {
            btnBack.visibility = View.INVISIBLE
        }
        onPage = currentUrl
    }

    private fun clearProfileData() {
        if (activity.isFinishing) return

        FileSecurity.delete(activity, Files.Profiles)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            when {
                btnBack.visibility == View.VISIBLE -> webView.goBack()
                isRevoke -> {
                    callback?.onRevoke()
                    dismiss()
                }
                else -> cancel()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun tagStartPage(url: String) {
        when {
            url.contains(KeyVerification.caseSignInStart) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Start, false)
            }
            url.contains(KeyVerification.caseSignUpStart) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Signup_Start, false)
            }
            url.contains(KeyVerification.caseRecoveryStart) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Forgot_Start, false)
            }
            url.contains(KeyVerification.caseMappingStart) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Verify_Start, false)
            }
            url.contains(KeyVerification.caseUpdatePasswordStart) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Change_New_Password_Start, false)
            }
        }
    }

    private fun tagCancelPage(url: String) {
        when {
            url.contains(KeyVerification.caseSignIn) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Cancel, false)
            }
            url.contains(KeyVerification.caseSignUp) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Signup_Cancel, false)
            }
            url.contains(KeyVerification.caseRecovery) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Forgot_Cancel, false)
            }
            url.contains(KeyVerification.caseMapping) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Verify_Cancel, false)
            }
            url.contains(KeyVerification.caseUpdatePassword) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Change_New_Password_Cancel, false)
            }
        }
    }

    private fun tagEndPage(url: String) {
        when {
            url.contains(KeyVerification.resLogin) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_End, true)
            }
            url.contains(KeyVerification.resRegister) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Signup_End, true)
            }
            url.contains(KeyVerification.resForgotPassword) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Forgot_End, true)
            }
            url.contains(KeyVerification.resMapSuccess) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Verify_End, true)
            }
            url.contains(KeyVerification.resUpdatePassword) -> {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Change_New_Password_End, true)
            }
        }
    }

    private fun exchangeAuthorization(codeVerifier: String, code: String) {
        val exchangeAuth = TaskExchangeAuthorization(context, codeVerifier, code)
        exchangeAuth.setExchangeListener(object : TaskExchangeAuthorization.ExchangeListener {
            override fun onSucceed(jsonObject: JSONObject) {
                gettingInfo(jsonObject)
            }

            override fun onFailed(error: String) {
                dismiss()
                Logcat.e(TAG, "TaskExchangeAuthorization : onFailed $error")
                serviceListener?.onLoginError(error)
            }
        })
        exchangeAuth.execute()
    }
}