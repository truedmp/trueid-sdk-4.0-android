package com.tdcm.truelifelogin.dialogs

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.Window
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.httpclient.Validate
import com.tdcm.truelifelogin.interfaces.LoginServiceListener
import com.tdcm.truelifelogin.tasks.TaskCheckNetwork
import com.tdcm.truelifelogin.tasks.TaskLoginNative
import com.tdcm.truelifelogin.utils.*
import kotlinx.android.synthetic.main.dialog_verification_native.*
import org.json.JSONException
import org.json.JSONObject

/*
 * Created by boyDroids on 5/4/2018 AD.
 * ^.^
 */
internal class DialogVerificationNative(val activity: Activity,
                                        private val ga: TrackingService?,
                                        val pf: PreferencesTrueID) : Dialog(activity, android.R.style.Theme_Black_NoTitleBar) {

    companion object {
        private val TAG = DialogVerificationNative::class.java.simpleName
    }

    interface OnDialogVerificationNative {
        fun getInfo()
        fun enableService()
    }

    private var callback: OnDialogVerificationNative? = null
    private var serviceListener: LoginServiceListener? = null
    private var languageConfig: LanguageConfig? = null

    fun setOnDialogVerificationNative(serviceListener: LoginServiceListener, callback: OnDialogVerificationNative) {
        this.serviceListener = serviceListener
        this.callback = callback
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_verification_native)

        languageConfig = LanguageConfig(activity, pf.language)

        setOnCancelListener {
            closed()
        }

        btnLogin.setOnClickListener {
            val user = editUser.text.toString()
            val pass = editPass.text.toString()
            if (checkConditionLogin(user, pass)) {
                showErrorMsgStatus(0)
                getLogin(user, pass)
            }
        }

        btnClose.setOnClickListener {
            closed()
        }
    }

    fun opened() {
        AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Native_Start, true)
        showErrorMsgStatus(0)
        show()
    }

    private fun closed() {
        AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Native_Cancel, false)
        serviceListener?.onCanceled()
        dismiss()
    }


    private fun checkConditionLogin(user: String, pass: String): Boolean {
        if (user.isEmpty() && pass.isEmpty()) {
            showErrorMsgStatus(1)
            txtErrorUser.text = languageConfig?.error_user
            txtErrorPass.text = languageConfig?.error_pass
            return false
        } else if (user.isEmpty()) {
            showErrorMsgStatus(2)
            txtErrorUser.text = languageConfig?.error_user
            return false
        } else if (pass.isEmpty()) {
            showErrorMsgStatus(3)
            txtErrorPass.text = languageConfig?.error_pass
            return false
        } else if (!checkEmailOrTel(user)) {
            showErrorMsgStatus(4)
            txtErrorUser.text = languageConfig?.error_wrong_user
            return false
        } else {
            return true
        }
    }

    private fun showErrorMsgStatus(status: Int) {
        when (status) {
            0 //clear msg
            -> {
                txtErrorUser.visibility = View.GONE
                txtErrorPass.visibility = View.GONE
                txtErrorCenter.visibility = View.GONE
            }
            1 // blank username and password
            -> {
                txtErrorUser.visibility = View.VISIBLE
                txtErrorPass.visibility = View.VISIBLE
                txtErrorPass.visibility = View.GONE
            }
            2 // blank user
            -> {
                txtErrorUser.visibility = View.VISIBLE
                txtErrorPass.visibility = View.GONE
                txtErrorPass.visibility = View.GONE
            }
            3 // blank pass
            -> {
                txtErrorUser.visibility = View.GONE
                txtErrorPass.visibility = View.VISIBLE
                txtErrorPass.visibility = View.GONE
            }
            4 // wrong username
            -> {
                txtErrorUser.visibility = View.VISIBLE
                txtErrorPass.visibility = View.GONE
                txtErrorPass.visibility = View.GONE
            }
            5 // bottom error
            -> {
                txtErrorUser.visibility = View.GONE
                txtErrorPass.visibility = View.GONE
                txtErrorPass.visibility = View.VISIBLE
            }
        }
    }

    private fun checkEmailOrTel(user: String): Boolean {
        return (user.matches(".*\\d+.*".toRegex()) && user.length == 10) || UtilsSDK.isEmailValid(user)
    }

    private fun getLogin(user: String, pass: String) {
        TaskCheckNetwork(activity, object : TaskCheckNetwork.OnCheckNetWorkListener {
            override fun onHaveNetwork() {
                taskLoginNative(user, pass)
            }

            override fun onNoneNetwork() {
                UtilsSDK.openToastInternet(activity, pf.language)
                serviceListener?.onLoginError(activity.getString(R.string.error_internet_access))
            }
        }).execute()
    }

    private fun taskLoginNative(user: String, pass: String) {
        val taskLogin = TaskLoginNative(activity, user, pass)
        taskLogin.setOnTaskLogin(object : TaskLoginNative.OnTaskLogin {
            override fun onFailed(error: String) {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Native_End, false)
                try {
                    val jRoot = JSONObject(error)
                    if (!jRoot.has("service_code")) return //TODO:: Show Error

                    val code = jRoot.getInt("service_code")
                    showErrorLoginCode(code)
                } catch (e: JSONException) {
                    Logcat.e(TAG, "JSONException : $e")
                }
            }

            override fun onSuccess(result: String) {
                AAATracking(ga, serviceListener).event(KeysTracking.Events_Login_Native_End, true)
                callback?.getInfo()
                if (pf.isRunService) {
                    callback?.enableService()
                }
                dismiss()
            }

        })
        taskLogin.execute()
    }

    fun showErrorLoginCode(code: Int) {
        when (code) {
            1400038 -> txtErrorPass.text = languageConfig?.error_server_wrong_id_1
            1400169 -> txtErrorPass.text = languageConfig?.error_server_wrong_id_2
            1400019 -> txtErrorPass.text = languageConfig?.error_server_wrong_id_3
            1400043 -> txtErrorPass.text = languageConfig?.error_server_wrong_id_4
        }
        showErrorMsgStatus(5)
    }
}