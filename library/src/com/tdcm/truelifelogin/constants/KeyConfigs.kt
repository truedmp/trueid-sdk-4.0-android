package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 17/7/2018 AD.
 * ^.^
 */
internal object KeyConfigs {
    const val langEN = "en"
    const val langTH = "th"
    const val qr = "qr"
    const val qrScanner = "scanner"
    const val qrConfirmButton = "confirm_button"
    const val qrConfirmMsg1 = "confirm_msg_1"
    const val qrConfirmMsg2 = "confirm_msg_2"
    const val qrFailedButton = "failed_button"
    const val qrFailedMsg1 = "failed_msg_1"
    const val qrFailedMsg2 = "failed_msg_2"
    const val qrScanLogin = "scan_login"
    const val qrScanMsg = "scan_msg_1"
    const val qrStartMsg = "start_msg"
    const val qrSuccessfulButton = "successful_button"
    const val qrSuccessfulMsg1 = "successful_msg_1"
    const val qrSuccessfulMsg2 = "successful_msg_2"
}