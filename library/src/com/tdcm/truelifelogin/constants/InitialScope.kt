package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 27/4/2018 AD.
 * ^.^
 */
/**
 * InitialLScope can Select Scope Existing
 */
class InitialScope {
    companion object {
        /**
         * profile get Scope thaiID
         */
        const val PROFILE = "public_profile"
        /**
         * mobile get number mobile
         */
        const val MOBILE = "mobile"
        /**
         * email get data email
         */
        const val EMAIL = "email"
        /**
         * get data references
         */
        const val REFERENCE = "references"
    }
}