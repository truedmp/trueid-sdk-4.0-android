package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 19/4/2018 AD.
 * ^.^
 */
internal class KeyRefreshToken {
    internal companion object {
        const val CASE_LOGOUT = "logout"
        const val CASE_INFO = "info"
        const val CASE_INFO_MORE = "info_more"
        const val CASE_CREDENTIAL = "credential"
        const val CASE_OLD_VERSION = "old_version"
        const val CASE_EXPIRE = "expire"
        const val CASE_SINGLE_SIGN_ON = "single_sign_on"
        const val CASE_RESET_PASSWORD = "reset_password"
        const val CASE_GET_ACCESS_TOKEN = "get_access_token"
        const val CASE_MAPPING = "mapping"
        const val CASE_FORCE_REFRESH = "force_refresh"
    }
}