package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 1/6/2018 AD.
 * ^.^
 */
/**
 * SDKEnvironment set from environment server
 */
enum class SDKEnvironment(val value: String) {
    /**
     * select server Alpha
     */
    ALPHA("alpha"), //client please not use, this environment for sdk developer only
    /**
     * select server Staging
     */
    STAGING("staging"),
    /**
     * select server Production
     */
    PRODUCTION("production")
}