package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 3/8/2018 AD.
 * ^.^
 */
internal enum class MappingCase(val value: String) {
    FAILED("Mapping Failed"),
    ALREADY("Mapping Already"),
    SUCCESS("Mapping Success")
}