package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 1/6/2018 AD.
 * ^.^
 */
internal enum class ClearSession {
    LOGOUT, REVOKE, ERROR
}