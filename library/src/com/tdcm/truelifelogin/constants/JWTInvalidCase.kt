package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 1/6/2018 AD.
 * ^.^
 */
internal enum class JWTInvalidCase {
    NONE, EXPIRE, REVOKE, DATE_TIME
}