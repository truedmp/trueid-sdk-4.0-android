package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 26/4/2018 AD.
 * ^.^
 */
internal class KeyErrorCode {
    internal companion object {
        const val InvalidRefreshToken = "1403001"
        const val InvalidAccessToken = "1403002"
    }
}