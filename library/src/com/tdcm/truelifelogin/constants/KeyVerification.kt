package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 5/4/2018 AD.
 * ^.^
 */
internal class KeyVerification {

    internal companion object {
        const val resLogin = "login_success"
        const val resRegister = "register_success"
        const val resForgotPassword = "forgotpassword_success"
        const val resMapSuccess = "mapping_success"
        const val resMapFailed = "mapping_failed"
        const val resMapAlready = "mapping_already"
        const val resUpdatePassword = "updatepassword_success"

        const val caseSignIn = "signin"
        const val caseSignInStart = "signin?"
        const val caseSignUp = "signup"
        const val caseSignUpStart = "signup?"
        const val caseSignOTP = "signup/otp"
        const val caseVerify = "verify"
        const val caseRecovery = "recovery"
        const val caseRecoveryStart = "recovery?"
        const val caseRecoveryOTP = "recovery/otp"
        const val casePolicy = "policy"
        const val caseTermOfUse = "termofuse"
        const val caseMapping = "mapping"
        const val caseMappingStart = "mapping?"
        const val caseUpdatePassword = "newpassword"
        const val caseUpdatePasswordStart = "newpassword?"

        const val codeSuccess = "code_success"

        fun isSuccess(url: String): Boolean {
            if (url.startsWith("trueid://?") || url.startsWith("trueid://")) {
                return true
            }
            return false
        }
    }
}