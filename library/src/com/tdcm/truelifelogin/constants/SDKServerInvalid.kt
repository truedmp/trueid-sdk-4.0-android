package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 30/8/2018 AD.
 * ^.^
 */
internal object SDKServerInvalid {
    private const val REFRESH_TOKEN = "1403001"
    private const val ACCESS_TOKEN = "1403002"
    private const val ACCOUNT = "1400034"
    private const val ACCESS_TIME_OUT = "3001"
    private const val MISSING_CLIENT_ID = "4045"

    private fun listInvalid(): List<String> {
        return listOf(REFRESH_TOKEN,
                ACCESS_TOKEN,
                ACCOUNT,
                ACCESS_TIME_OUT,
                MISSING_CLIENT_ID)
    }

    fun isRevoke(code: String): Boolean {
        for (i in listInvalid().indices) {
            if (code == listInvalid()[i]) {
                return true
            }
        }
        return false
    }
}