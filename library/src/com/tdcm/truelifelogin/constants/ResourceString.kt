package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 27/4/2018 AD.
 * ^.^
 */
internal class ResourceString {
    internal companion object {
        const val clientId = "trueid_client_id"
        const val clientFlow = "trueid_client_flow"
        const val clientSchemeForSdk = "trueid_client_scheme_for_sdk"
    }
}