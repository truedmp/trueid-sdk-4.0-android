package com.tdcm.truelifelogin.constants

/*
 * Created by boyDroids on 29/6/2018 AD.
 * ^.^
 */
internal class JWTError {
    internal companion object {
        const val ERROR_CASE = "sdk.error"

        /*
        * Something wrong in function, please call to developer checking catch case.
        * */
        const val ERROR_EXCEPTION = "$ERROR_CASE#480001"
        /*
        * JWTUrl and KidAndroid is empty please call LoginService in Activity, before used function
        * */
        const val ERROR_NOT_INITIAL = "$ERROR_CASE#480002"
        /*
        * Access token is empty, please login before used function
        * */
        const val ERROR_ACCESS_EMPTY = "$ERROR_CASE#480003"
        /*
        * JsonWebKey is null, because setting "kid" not matching in data from firebase,
        * please reload firebase config and call function again
        * */
        const val ERROR_JWT_WEB_KEY = "$ERROR_CASE#480004"
        /*
        * Timestamp in device not is open auto datetime and can't get datetime by server,
        * please call this function again.
        * */
        const val ERROR_BAD_DATETIME = "$ERROR_CASE#480005"
        /*
        * Error in catch "InvalidJwtException".
        * */
        const val ERROR_JWT_INVALID = "$ERROR_CASE#480006"
        /*
        * Can't get json from jwt payload. because some field in payload json
        * not unsupported encode
        * */
        const val ERROR_JSON_ENCODE = "$ERROR_CASE#480007"
        /*
        * JWT >> It normal case JWT is expire, don't worry.
        * */
        const val JWT_TIME_EXPIRE = "$ERROR_CASE#486001"
        /*
        * JWT >> The JWS signature was not successfully verified with the given resolved key,
        * and auto revoke session.
        * */
        const val JWT_SIGNATURE_INVALID = "$ERROR_CASE#486009"
        /*
         * JWT >> Http cannot connect to server
         * and auto revoke session.
         * */
        const val ERROR_INTERNET_ACCESS = "$ERROR_CASE#480008"

        // APIs url is empty
        const val ERROR_EMPTY_URL = "$ERROR_CASE#480009"
    }
}