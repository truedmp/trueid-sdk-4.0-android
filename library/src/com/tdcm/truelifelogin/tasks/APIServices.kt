package com.tdcm.truelifelogin.tasks

import com.tdcm.truelifelogin.utils.Auth0
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.PreferencesTrueID

/*
 * Created by boyDroids on 29/10/2018 AD.
 * ^.^
 */
internal object APIServices {
    /**
     * query login send data to Server
     */
    fun getQueryLogin(preferences: PreferencesTrueID): String {
        return mergeQuery(preferences) +
                "&device_id=${preferences.android_id}" +
                "&device_model=${preferences.android_model}" +
                "&latlong=${preferences.location}" +
                "&ip_address=${preferences.ip_address}" +
                "&flow=${preferences.client_flow}" +
                "&lang=${preferences.language}" +
                "&scope=${preferences.scope}" +
                "&root_token=1"
    }

    /**
     * query register send data to Server
     */
    fun getQueryRegister(preferences: PreferencesTrueID): String {
        return mergeQuery(preferences) +
                "&device_id=${preferences.android_id}" +
                "&device_model=${preferences.android_model}" +
                "&latlong=${preferences.location}" +
                "&ip_address=${preferences.ip_address}" +
                "&flow=${preferences.client_flow}" +
                "&lang=${preferences.language}" +
                "&scope=${preferences.scope}" +
                "&root_token=1"
    }

    /**
     * query verify send data to Server
     */
    fun getQueryVerify(preferences: PreferencesTrueID): String {
        return mergeQuery(preferences) +
                "&access_token=${preferences.access_token_client}" +
                "&device_id=${preferences.android_id}" +
                "&device_model=${preferences.android_model}" +
                "&latlong=${preferences.location}" +
                "&ip_address=${preferences.ip_address}" +
                "&lang=${preferences.language}"
    }

    /**
     * query recovery send data to Server
     */
    fun getQueryRecovery(preferences: PreferencesTrueID): String {
        return mergeQuery(preferences) +
                "&device_id=${preferences.android_id}" +
                "&device_model=${preferences.android_model}" +
                "&latlong=${preferences.location}" +
                "&ip_address=${preferences.ip_address}" +
                "&lang=${preferences.language}" +
                "&flow=${preferences.client_flow}"
    }

    /**
     * query update password send data to Server (params same verify)
     */
    fun getQueryUpdatePassword(preferences: PreferencesTrueID): String {
        return mergeQuery(preferences) +
                "&device_id=${preferences.android_id}" +
                "&device_model=${preferences.android_model}" +
                "&latlong=${preferences.location}" +
                "&ip_address=${preferences.ip_address}" +
                "&lang=${preferences.language}"
    }

    private fun mergeQuery(pf: PreferencesTrueID): String {
        val verifier = Auth0.createCodeVerifier()
        return "?device=android" +
                "&${KEYS.AUTH0_STATE}=${Auth0.randomState()}" +
                "&${KEYS.AUTH0_CODE_CHALLENGE}=${Auth0.createCodeChallenge(verifier)}" +
                "&${KEYS.AUTH0_CODE_CHALLENGE_METHOD}=${KEYS.AUTH0_METHOD}" +
                "&${KEYS.AUTH0_CLIENT_ID}=${pf.client_id}" +
                "&${KEYS.AUTH0_REDIRECT_URI}=${pf.reDirectUrl}" +
                "{$verifier}"
    }
}