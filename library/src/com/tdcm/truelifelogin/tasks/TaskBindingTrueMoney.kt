package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import org.json.JSONObject

/*
 * Created by boyDroids on 23/8/2018 AD.
 * ^.^
 */
internal class TaskBindingTrueMoney(context: Context,
                                    private val bindingIDs: String) : BaseServiceTask(context) {

    interface BindingTrueMoneyListener {
        fun onSuccess(isOK: Boolean)
        fun onError(msg: String)
    }

    private var callback: BindingTrueMoneyListener? = null

    fun setBindingTrueMoneyListener(callback: BindingTrueMoneyListener): TaskBindingTrueMoney {
        this.callback = callback
        return this@TaskBindingTrueMoney
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskBindingTrueMoney
    }

    override fun onBackground(context: Context): JSONObject {
        val pref = PreferencesTrueID(context)
        return SDKHttpServices(context).bindingTrueMoney(bindingIDs, pref.access_token_client)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        val isOK = jsonObject.getString(KEYS.STATUS) == KEYS.OK
        callback?.onSuccess(isOK)
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onError(message)
    }
}