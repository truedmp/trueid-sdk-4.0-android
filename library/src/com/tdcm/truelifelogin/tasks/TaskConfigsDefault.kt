package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.utils.*
import java.lang.ref.WeakReference

internal class TaskConfigsDefault(context: Context) : AsyncTask<Void, Void, Void>() {

    companion object {
        private val TAG = TaskConfigsDefault::class.java.simpleName
    }

    interface OnLoadConfigs {
        fun onPostExecute(trackingService: TrackingService?)
    }

    private var callback: OnLoadConfigs? = null

    private var mContext: WeakReference<Context>? = null
    private var trackingService: TrackingService? = null

    /**
     * callback load Data from server
     */
    fun setOnLoadConfigs(callback: OnLoadConfigs): TaskConfigsDefault {
        this.callback = callback
        return this@TaskConfigsDefault
    }

    init {
        mContext = WeakReference(context)
    }

    /**
     * Initialize set Data before load Data
     */
    override fun doInBackground(vararg params: Void?): Void? {
        val context = mContext?.get()!!.applicationContext
        val pref = PreferencesTrueID(context)

        AAAConfigurations.loadBundle(context, pref)
        AAAConfigurations.loadUrlConfig(context, pref)
        AAAConfigurations.loadError(context)

        trackingService = TrackingService(context, KeysTracking.ga_tracking_url)

        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Logcat.i(TAG, "LoadConfigsDefault Success")

        callback?.onPostExecute(trackingService)
    }
}