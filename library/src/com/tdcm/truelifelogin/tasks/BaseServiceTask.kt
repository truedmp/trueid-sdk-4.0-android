package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.constants.JWTError
import com.tdcm.truelifelogin.constants.SDKServerInvalid
import com.tdcm.truelifelogin.utils.KEYS
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.UtilsSDK
import org.json.JSONObject
import java.lang.ref.WeakReference
import javax.net.ssl.HttpsURLConnection

/*
 * Created by boyDroids on 8/10/2018 AD.
 * ^.^
 */
internal abstract class BaseServiceTask(context: Context) : AsyncTask<Void, Void, JSONObject>() {
    companion object {
        var TAG: String = BaseServiceTask::class.java.simpleName
    }

    private var mContext: WeakReference<Context>? = null

    init {
        this.mContext = WeakReference(context)
    }

    abstract fun setTag(): AsyncTask<Void, Void, JSONObject>
    abstract fun onBackground(context: Context): JSONObject
    abstract fun onPostSuccess(context: Context, jsonObject: JSONObject)
    abstract fun onPostError(message: String, isRevoke: Boolean)

    override fun onPreExecute() {
        super.onPreExecute()
        TAG = setTag()::class.java.simpleName
    }

    override fun doInBackground(vararg params: Void?): JSONObject {
        val mContext: Context = mContext?.get()?.applicationContext ?: return JSONObject()
        return onBackground(mContext)
    }

    override fun onPostExecute(result: JSONObject) {
        super.onPostExecute(result)
        mContext?.get()?.applicationContext?.let { mContext ->
            try {
                if (result.length() == 0) {
                    onPostError(UtilsSDK.getErrorObjectFromCode(mContext, JWTError.ERROR_EXCEPTION).toString(), false)
                    return
                }

                val data = result.getJSONObject(KEYS.data)

                if (result.getInt(KEYS.code) == HttpsURLConnection.HTTP_OK) {
                    onPostSuccess(mContext, data)
                } else {
                    if (data.length() == 0) {
                        onPostError(result.getString(KEYS.messages), false)
                        return
                    }
                    val errObj = data.getJSONObject(KEYS.ERROR)
                    val isRevoke = SDKServerInvalid.isRevoke(errObj.getString(KEYS.code))

                    val errResult = UtilsSDK.getErrorObject(mContext, errObj)
                    onPostError(errResult.toString(), isRevoke)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Logcat.e(TAG, "Exception : $e")
                onPostError(UtilsSDK.getErrorObjectFromCode(mContext, JWTError.ERROR_EXCEPTION).toString(), false)
            }
        }
    }
}