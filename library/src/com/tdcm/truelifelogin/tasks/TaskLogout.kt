package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import org.json.JSONObject

internal class TaskLogout(context: Context,
                          private val pref: PreferencesTrueID) : BaseServiceTask(context) {

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskLogout
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).logout(pref)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        Logcat.i(TAG, "Client logout successfully.")
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
    }
}