package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.Validate
import java.lang.ref.WeakReference

class TaskCheckNetwork(context: Context, private val onCheckNetWorkListener: OnCheckNetWorkListener?) : AsyncTask<Void, Void, Boolean>() {

    interface OnCheckNetWorkListener {
        fun onHaveNetwork()
        fun onNoneNetwork()
    }

    private var mContext: WeakReference<Context>? = null

    init {
        mContext = WeakReference(context)
    }

    override fun doInBackground(vararg params: Void?): Boolean? {
        return Validate.hasInternetAccess(mContext?.get())
    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        if (result == true) {
            onCheckNetWorkListener?.onHaveNetwork()
        } else {
            onCheckNetWorkListener?.onNoneNetwork()
        }
    }
}