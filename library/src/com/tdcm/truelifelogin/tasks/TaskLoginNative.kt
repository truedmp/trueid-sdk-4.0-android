package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.UtilsSDK
import org.json.JSONObject

internal class TaskLoginNative(context: Context,
                               val user: String,
                               val password: String) : BaseServiceTask(context) {

    interface OnTaskLogin {
        fun onFailed(error: String)
        fun onSuccess(result: String)
    }

    private var callback: OnTaskLogin? = null

    fun setOnTaskLogin(callback: OnTaskLogin): TaskLoginNative {
        this.callback = callback
        return this@TaskLoginNative
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskLoginNative
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).loginFromNative(user, password, PreferencesTrueID(context))
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        UtilsSDK.setPreferenceToken(jsonObject, context)
        callback?.onSuccess(jsonObject.toString())
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onFailed(message)
    }
}