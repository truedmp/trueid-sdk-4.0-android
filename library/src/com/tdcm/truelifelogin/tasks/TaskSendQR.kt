package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import org.json.JSONObject

internal class TaskSendQR(context: Context,
                          private val qrData: String) : BaseServiceTask(context) {

    interface QRSendingListener {
        fun onSucceed()
        fun onFailed(error: String)
    }

    private var callback: QRSendingListener? = null

    fun setQRSendingListener(callback: QRSendingListener): TaskSendQR {
        this.callback = callback
        return this@TaskSendQR
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskSendQR
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).sendQRData(qrData)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        callback?.onSucceed()
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onFailed(message)
    }
}