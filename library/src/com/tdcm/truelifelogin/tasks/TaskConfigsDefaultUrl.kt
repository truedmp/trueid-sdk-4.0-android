package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.constants.KeyVerification
import com.tdcm.truelifelogin.utils.*
import java.lang.ref.WeakReference

/*
 * Created by boyDroids on 29/10/2018 AD.
 * ^.^
 */
internal class TaskConfigsDefaultUrl(context: Context, val urlName: String, val params: String) :
        AsyncTask<Void, Void, Void>() {

    companion object {
        private val TAG = TaskConfigsDefaultUrl::class.java.simpleName
    }

    interface OnLoadConfigs {
        fun onPostExecute(trackingService: TrackingService?, finalUrl: String)
    }

    private var callback: OnLoadConfigs? = null

    private var mContext: WeakReference<Context>? = null
    private var trackingService: TrackingService? = null

    /**
     * callback load Data from server
     */
    fun setOnLoadConfigs(callback: OnLoadConfigs): TaskConfigsDefaultUrl {
        this.callback = callback
        return this@TaskConfigsDefaultUrl
    }

    init {
        mContext = WeakReference(context)
    }

    /**
     * Initialize set Data before load Data
     */
    override fun doInBackground(vararg params: Void?): Void? {
        val context = mContext?.get()!!.applicationContext
        val pref = PreferencesTrueID(context)

        AAAConfigurations.loadUrlConfig(context, pref)
        AAAConfigurations.loadError(context)

        trackingService = TrackingService(context, KeysTracking.ga_tracking_url)

        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Logcat.i(TAG, "LoadConfigsWithUrl Success")

        var url = ""
        when {
            urlName.equals(KeyVerification.caseSignIn, ignoreCase = true) -> url = APIs.web_signin_url
            urlName.equals(KeyVerification.caseSignUp, ignoreCase = true) -> url = APIs.web_signup_url
            urlName.equals(KeyVerification.caseVerify, ignoreCase = true) -> url = APIs.web_verify_thaiid_url
            urlName.equals(KeyVerification.caseRecovery, ignoreCase = true) -> url = APIs.web_recovery_url
            urlName.equals(KeyVerification.caseUpdatePassword, ignoreCase = true) -> url = APIs.web_update_password
        }
        callback?.onPostExecute(trackingService, url + params)
    }
}