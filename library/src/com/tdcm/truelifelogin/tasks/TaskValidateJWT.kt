package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.constants.JWTError
import com.tdcm.truelifelogin.constants.JWTInvalidCase
import com.tdcm.truelifelogin.internal.JWTUtilsSDK
import com.tdcm.truelifelogin.utils.Logcat
import com.tdcm.truelifelogin.utils.UtilsSDK
import java.lang.ref.WeakReference

/*
 * Created by boyDroids on 19/4/2018 AD.
 * ^.^
 */
internal class TaskValidateJWT(context: Context) : AsyncTask<String, Void, String>() {

    private var mContext: WeakReference<Context>? = null

    interface ValidateJWTListener {
        fun onSuccess(jsonProfile: String?)

        fun onError(msgErr: String?, eCase: JWTInvalidCase)
    }

    private var callback: ValidateJWTListener? = null

    fun setValidateJWTListener(callback: ValidateJWTListener): TaskValidateJWT {
        this.callback = callback
        return this@TaskValidateJWT
    }

    init {
        this.mContext = WeakReference(context)
    }

    companion object {
        private val TAG = TaskValidateJWT::class.java.simpleName
    }

    override fun doInBackground(vararg params: String?): String {
        val mContext: Context = mContext?.get()!!.applicationContext
        return JWTUtilsSDK.check(mContext)
    }

    override fun onPostExecute(result: String) {
        super.onPostExecute(result)
        val mContext: Context = mContext?.get()!!.applicationContext

        try {
            if (result.contains(JWTError.ERROR_CASE)) {
                val errorObject = UtilsSDK.getErrorObjectFromCode(mContext, result)
                when (result) {
                    JWTError.JWT_TIME_EXPIRE -> {
                        callback?.onError(errorObject.toString(), JWTInvalidCase.EXPIRE)
                    }
                    JWTError.JWT_SIGNATURE_INVALID -> {
                        callback?.onError(errorObject.toString(), JWTInvalidCase.REVOKE)
                    }
                    JWTError.ERROR_BAD_DATETIME -> {
                        callback?.onError(errorObject.toString(), JWTInvalidCase.DATE_TIME)
                    }
                    else -> {
                        callback?.onError(errorObject.toString(), JWTInvalidCase.NONE)
                    }
                }
            } else {
                callback?.onSuccess(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Logcat.e(TAG, "Exception : $e")
            callback?.onError(UtilsSDK.getErrorObjectFromCode(mContext, JWTError.ERROR_EXCEPTION).toString(), JWTInvalidCase.NONE)
        }
    }
}