package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.httpclient.SDKJsonParser
import com.tdcm.truelifelogin.utils.FileSecurity
import com.tdcm.truelifelogin.utils.Files
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import org.json.JSONObject
import javax.net.ssl.HttpsURLConnection

internal class TaskInfoServer(context: Context) : BaseServiceTask(context) {

    interface OnTaskInfoServer {
        fun onFailed(error: String, isInvalid: Boolean)
        fun onSuccess(result: String, expire: Int)
    }

    private val fileName = Files.Profiles
    private var callback: OnTaskInfoServer? = null

    fun setOnTaskInfoServer(callback: OnTaskInfoServer): TaskInfoServer {
        this.callback = callback
        return this@TaskInfoServer
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskInfoServer
    }

    override fun onBackground(context: Context): JSONObject {
        val isExists = FileSecurity.isExists(context, fileName)
        return if (isExists) {
            val data = FileSecurity.read(context, fileName)
            SDKJsonParser(context).parser(data, HttpsURLConnection.HTTP_OK)
        } else {
            SDKHttpServices(context).getProfileServer(PreferencesTrueID(context))
        }
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        val isExists = FileSecurity.isExists(context, fileName)
        if (!isExists) FileSecurity.write(context, fileName, jsonObject.toString())

        callback?.onSuccess(jsonObject.toString(), PreferencesTrueID(context).token_expires_second)
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onFailed(message, isRevoke)
    }
}