package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import org.json.JSONObject

internal class TaskGetTokenFromCode(context: Context,
                                    private val loginCode: String) : BaseServiceTask(context) {

    interface OnGetToken {
        fun onSuccess(jsonObj: JSONObject)
        fun onError(msg: String)
    }

    private var callback: OnGetToken? = null

    fun setOnGetToken(callback: OnGetToken) : TaskGetTokenFromCode{
        this.callback = callback
        return this@TaskGetTokenFromCode
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskGetTokenFromCode
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).tokenFromCode(loginCode)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        callback?.onSuccess(jsonObject)
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onError(message)
    }
}