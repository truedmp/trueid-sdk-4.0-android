package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import org.json.JSONObject

internal class TaskExchangeAuthorization(context: Context,
                                         private val verifier: String,
                                         private val code: String) : BaseServiceTask(context) {
    interface ExchangeListener {
        fun onSucceed(jsonObject: JSONObject)
        fun onFailed(error: String)
    }

    private var callback: ExchangeListener? = null

    fun setExchangeListener(callback: ExchangeListener) {
        this.callback = callback
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskExchangeAuthorization
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).exchangeAuthorization(verifier, code)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        callback?.onSucceed(jsonObject)
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onFailed(message)
    }
}