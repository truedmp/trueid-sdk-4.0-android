package com.tdcm.truelifelogin.tasks

import android.content.Context
import android.os.AsyncTask
import com.tdcm.truelifelogin.authentication.TrueIDSDK
import com.tdcm.truelifelogin.httpclient.SDKHttpServices
import com.tdcm.truelifelogin.utils.*
import org.json.JSONObject

internal class TaskRefreshToken(context: Context,
                                private val refreshToken: String,
                                private val isRoot: Boolean) : BaseServiceTask(context) {

    interface RefreshListener {
        fun onSucceed()
        fun onFailed(error: String, isRevoke: Boolean)
    }

    private var callback: RefreshListener? = null

    fun setOnTaskRefreshToken(callback: RefreshListener): TaskRefreshToken {
        this.callback = callback
        return this@TaskRefreshToken
    }

    fun taskExecute(){
        if (TrueIDSDK.isTokenRefreshing()) {
            Logcat.w(TAG, "Blocking duplicate calling refresh token in single task.")
            return
        }
        TrueIDSDK.startRefreshTokenTask()
        Logcat.d(TAG, "Start task refresh token.")
        execute()
    }

    override fun setTag(): AsyncTask<Void, Void, JSONObject> {
        return this@TaskRefreshToken
    }

    override fun onBackground(context: Context): JSONObject {
        return SDKHttpServices(context).postRefreshToken(refreshToken, isRoot)
    }

    override fun onPostSuccess(context: Context, jsonObject: JSONObject) {
        UtilsSDK.setPreferenceToken(jsonObject, context)

        FileSecurity.delete(context, Files.Profiles)
        callback?.onSucceed().also {
            TrueIDSDK.finishRefreshTokenTask()
        }
    }

    override fun onPostError(message: String, isRevoke: Boolean) {
        callback?.onFailed(message, isRevoke).also {
            TrueIDSDK.finishRefreshTokenTask()
        }
    }
}