package com.tdcm.truelifelogin.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.tdcm.truelifelogin.utils.Logcat;
import com.tdcm.truelifelogin.utils.PreferencesTrueID;

/**
 * Pass data SDK v.3 to SDK v.4 and save data
 */
public class AuthActivity extends Activity {

	private String TAG = AuthActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Logcat.i(TAG, "onCreate()");

		Intent intent = getIntent();
		String accessToken = intent.getStringExtra("access_token");
		String refreshToken = intent.getStringExtra("refresh_token");
		boolean isFromMore = intent.getBooleanExtra("isFromMore", false);

		PreferencesTrueID preferencesTID = new PreferencesTrueID(this);
		if ("".equals(accessToken) || "".equals(refreshToken)) {
			Logcat.d(TAG, "AccessToken or RefreshToken not found.");
		} else {
			Logcat.i(TAG, "Set token to sdk preference.");

			preferencesTID.setAccess_token_root(accessToken);
			preferencesTID.setRefresh_token_root(refreshToken);
			preferencesTID.setAccess_token_client(accessToken);
			preferencesTID.setRefresh_token_client(refreshToken);
			preferencesTID.setRoot_refresh_token(refreshToken); //use for LoginServiceSettings
			preferencesTID.setVersion("3"); //use for LoginServiceSettings

			if (isFromMore) {
				Logcat.i(TAG, "Data isFromMore is true");

				int trueIdClientSchemeForApp = getResources().getIdentifier("trueid_client_scheme_for_app", "string", getPackageName());
				Intent mIt = new Intent(Intent.ACTION_VIEW);
				mIt.setData(Uri.parse(getString(trueIdClientSchemeForApp) + "://?referral=trueid"));
				mIt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(mIt);
			}
		}
		finish();
	}
}