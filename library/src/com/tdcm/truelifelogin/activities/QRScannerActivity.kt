package com.tdcm.truelifelogin.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.zxing.integration.android.IntentIntegrator
import com.tdcm.truelifelogin.tasks.TaskSendQR
import com.tdcm.truelifelogin.constants.KeyConfigs
import com.tdcm.truelifelogin.dialogs.DialogConfirmQR
import com.tdcm.truelifelogin.interfaces.LoginServiceListener
import com.tdcm.truelifelogin.interfaces.QRScanListener
import com.tdcm.truelifelogin.utils.*
import org.json.JSONObject

/*
 * Created by boyDroids on 2/7/2018 AD.
 * ^.^
 */
internal class QRScannerActivity : Activity() {

    private var ga: TrackingService? = null
    private var configObject: JSONObject? = null
    private var isConfirm = false
    private var objSuccess = JSONObject()
    private var objFailed = JSONObject()

    companion object {
        private val TAG = QRScannerActivity::class.java.simpleName
        private const val KEY_QR_JSON_DATA = "key_qr_json_data"

        private var qrCallback: QRScanListener? = null
        private var serviceCallback: LoginServiceListener? = null

        @JvmStatic
        fun openCamera(activity: Activity?, qrCallback: QRScanListener, serviceCallback: LoginServiceListener) {
            if (activity == null || activity.isFinishing) return

            this.qrCallback = qrCallback
            this.serviceCallback = serviceCallback

            val intent = Intent(activity, QRScannerActivity::class.java)
            activity.startActivity(intent)
        }

        @JvmStatic
        fun openSendingQR(activity: Activity?, jsonData: String, qrCallback: QRScanListener, serviceCallback: LoginServiceListener) {
            if (activity == null || activity.isFinishing) return

            this.qrCallback = qrCallback
            this.serviceCallback = serviceCallback

            val intent = Intent(activity, QRScannerActivity::class.java).apply {
                putExtras(Bundle().apply {
                    putString(KEY_QR_JSON_DATA, jsonData)
                })
            }
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ga = TrackingService(this@QRScannerActivity)

        configObject = getQRConfigs()
        configObject?.let {
            val qrData: String? = intent?.getStringExtra(KEY_QR_JSON_DATA)
            qrData?.let { data ->
                addResultObject(data)
                openDialogConfirm(data)
            } ?: kotlin.run {
                IntentIntegrator(this@QRScannerActivity).apply {
                    captureActivity = CaptureActivityPortrait::class.java
                    addExtra(CaptureActivityPortrait.TYPE_KEY, CaptureActivityPortrait.TYPE_LOGIN)
                    setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
                    setPrompt(it.getString(KeyConfigs.qrScanMsg))
                    setCameraId(0)
                    setOrientationLocked(true)
                    setBeepEnabled(false)
                    setBarcodeImageEnabled(false)
                    initiateScan()
                }
            }
            bindResultObject(it)
        }.also {
            AAATracking(ga, serviceCallback).event(KeysTracking.Events_QR_Login_Start, true)
            AAATracking(ga, serviceCallback).screen(this@QRScannerActivity, KeysTracking.Screens_QR_LOGIN_SCAN)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isConfirm) {
            AAATracking(ga, serviceCallback).event(KeysTracking.Events_QR_Login_Cancel, false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result == null || result.contents == null) {
            super.onActivityResult(requestCode, resultCode, data)
            finish()
            return
        }

        addResultObject(result.contents)
        openDialogConfirm(result.contents)
    }

    private fun openDialogConfirm(qrData: String) {
        val dialog = DialogConfirmQR(this@QRScannerActivity, configObject)
        dialog.setConfirmQRListener(object : DialogConfirmQR.ConfirmQRListener {
            override fun onConfirm() {
                openTaskSendQR(qrData)
                finish()
            }

            override fun onCancel() {
                AAATracking(ga, serviceCallback).event(KeysTracking.Events_QR_Login_Cancel, false)
                qrCallback?.onCancel()
                finish()
            }
        })
        dialog.show().also {
            AAATracking(ga, serviceCallback).screen(this@QRScannerActivity, KeysTracking.Screens_QR_LOGIN_CONFIRM)
        }
    }

    fun openTaskSendQR(qrData: String) {
        val task = TaskSendQR(this@QRScannerActivity, qrData)
        task.setQRSendingListener(object : TaskSendQR.QRSendingListener {
            override fun onSucceed() {
                Logcat.i(TAG, "TaskSendQR >> onSucceed()")

                isConfirm = true
                AAATracking(ga, serviceCallback).event(KeysTracking.Events_QR_Login_End, true)
                qrCallback?.onSuccess(objSuccess.toString())
            }

            override fun onFailed(error: String) {
                Logcat.w(TAG, "TaskSendQR >> onFailed : $error")

                AAATracking(ga, serviceCallback).event(KeysTracking.Events_QR_Login_End, false)
                qrCallback?.onError(objFailed.toString())
            }
        })
        task.execute()
    }

    private fun bindResultObject(jsonObject: JSONObject) {
        objSuccess.apply {
            put("button", jsonObject.get(KeyConfigs.qrSuccessfulButton))
            put("msg_1", jsonObject.get(KeyConfigs.qrSuccessfulMsg1))
            put("msg_2", jsonObject.get(KeyConfigs.qrSuccessfulMsg2))
        }
        objFailed.apply {
            put("button", jsonObject.get(KeyConfigs.qrFailedButton))
            put("msg_1", jsonObject.get(KeyConfigs.qrFailedMsg1))
            put("msg_2", jsonObject.get(KeyConfigs.qrFailedMsg2))
        }
    }

    private fun addResultObject(result: String) {
        objSuccess.apply {
            put("value", result)
        }
        objFailed.apply {
            put("value", result)
        }
    }

    private fun getQRConfigs(): JSONObject? {
        val context: Context = this@QRScannerActivity

        val pref = PreferencesTrueID(context)

        val exists = FileSecurity.isExists(context, Files.Configs)
        return if (exists) {
            val response = FileSecurity.read(context, Files.Configs)

            val resObj = JSONObject(response)
            if (!resObj.has(KeyConfigs.qr)) return null

            val qrObj = resObj.getJSONObject(KeyConfigs.qr)
            if (!qrObj.has(KeyConfigs.qrScanner)) return null

            val scanObj = qrObj.getJSONObject(KeyConfigs.qrScanner)
            if (pref.language == KeyConfigs.langEN) {
                scanObj.getJSONObject(KeyConfigs.langEN)
            } else {
                scanObj.getJSONObject(KeyConfigs.langTH)
            }
        } else {
            Logcat.w(TAG, "File configs is empty, please check init LoginService(...) and try again.")
            null
        }
    }
}