package com.tdcm.truelifelogin.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.google.zxing.integration.android.IntentIntegrator
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.KeyConfigs
import com.tdcm.truelifelogin.interfaces.TrueIDInterface
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.TrackingService

/*
 * Created by boyDroids on 1/11/2018 AD.
 * ^.^
 */
internal class QRGeneralActivity : Activity() {

    private var ga: TrackingService? = null
    private var isResult = false

    companion object {
        private val TAG = QRGeneralActivity::class.java.simpleName

        private var qrCallback: TrueIDInterface.QRGeneralListener? = null

        @JvmStatic
        fun openCamera(activity: Activity?, qrCallback: TrueIDInterface.QRGeneralListener) {
            if (activity == null || activity.isFinishing) return

            this.qrCallback = qrCallback

            val intent = Intent(activity, QRGeneralActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ga = TrackingService(this@QRGeneralActivity)

        IntentIntegrator(this@QRGeneralActivity).apply {
            captureActivity = CaptureActivityPortrait::class.java
            addExtra(CaptureActivityPortrait.TYPE_KEY, CaptureActivityPortrait.TYPE_GENERAL)
            setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
            setPrompt(getWording())
            setCameraId(0)
            setOrientationLocked(true)
            setBeepEnabled(false)
            setBarcodeImageEnabled(false)
            initiateScan()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isResult) {
            qrCallback?.onCancel()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result == null || result.contents == null) {
            super.onActivityResult(requestCode, resultCode, data)
            finish()
            return
        }

        isResult = true
        qrCallback?.onSuccess(result.contents)
        finish()
    }

    private fun getWording(): String {
        val pref = PreferencesTrueID(this@QRGeneralActivity)
        return if(pref.language == KeyConfigs.langEN){
            resources.getString(R.string.qr_pay_en)
        } else {
            resources.getString(R.string.qr_pay_th)
        }
    }
}