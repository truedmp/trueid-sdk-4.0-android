package com.tdcm.truelifelogin.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import com.tdcm.truelifelogin.R
import com.tdcm.truelifelogin.constants.KeyConfigs
import com.tdcm.truelifelogin.utils.PreferencesTrueID
import com.tdcm.truelifelogin.utils.UtilsSDK
import kotlinx.android.synthetic.main.activity_qr_scanner.*

internal class CaptureActivityPortrait : Activity(), DecoratedBarcodeView.TorchListener {

    companion object {
        const val TYPE_KEY = "KEY_TYPE"
        const val TYPE_GENERAL = "GENERAL"
        const val TYPE_LOGIN = "LOGIN"
        private val TAG = CaptureActivityPortrait::class.java.simpleName
    }

    private var captureManager: CaptureManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_scanner)

        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)



        barcodeScanner.setTorchListener(this)

//        if (!hasFlash()) {
//            switchFlashlight.visibility = View.GONE
//        }

        captureManager = CaptureManager(this@CaptureActivityPortrait, barcodeScanner)
        captureManager?.initializeFromIntent(intent, savedInstanceState)
        captureManager?.decode()

        switchFlashlight.setOnClickListener {
            setOnOffFlash(it)
        }

        val type = intent?.extras?.getString(TYPE_KEY)
        setTextIcon(type)
    }

    override fun onResume() {
        super.onResume()
        //checking allow permission
        val activity = this@CaptureActivityPortrait
        if (PreferencesTrueID(activity).isCameraIgnore && !UtilsSDK.isCameraPermission(activity)) {
            openDialog()
            return
        } else {
            PreferencesTrueID(activity).isCameraIgnore = false
        }
        //...
        captureManager?.onResume()
    }

    override fun onPause() {
        super.onPause()
        captureManager?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        captureManager?.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        captureManager?.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return barcodeScanner.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }

    private fun hasFlash(): Boolean {
        return applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
    }

    private fun setTextIcon(type: String?) {
        type?.let {
            when (type) {
                TYPE_GENERAL -> {
                    barcodeScanner.statusView
                            .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_scantopay, 0, 0, 0)
                }
                TYPE_LOGIN -> {
                    barcodeScanner.statusView
                            .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_scanlogin, 0, 0, 0)
                }
            }
        }
    }

    fun setOnOffFlash(view: View) {
        if (switchFlashlight.text == getString(R.string.text_on_flash)) {
            barcodeScanner.setTorchOn()
        } else {
            barcodeScanner.setTorchOff()
        }
    }

    override fun onTorchOn() {
        switchFlashlight.setText(R.string.text_off_flash)
    }

    override fun onTorchOff() {
        switchFlashlight.setText(R.string.text_on_flash)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissions?.let {
            val activity = this@CaptureActivityPortrait
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, it[0])) {
                //denied
                finish()
            } else {
                if (ActivityCompat.checkSelfPermission(this, it[0]) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                } else {
                    //set to never ask again
                    PreferencesTrueID(activity).isCameraIgnore = true
                    finish()
                }
            }
        }
    }

    private fun openDialog() {
        val activity = this@CaptureActivityPortrait
        val pref = PreferencesTrueID(activity)

        val message: String
        val btnPositive: String
        val btnNegative: String
        if (pref.language == KeyConfigs.langEN) {
            message = resources.getString(R.string.qr_dialog_message_en)
            btnPositive = resources.getString(R.string.qr_dialog_positive_en)
            btnNegative = resources.getString(R.string.qr_dialog_negative_en)
        } else {
            message = resources.getString(R.string.qr_dialog_message_th)
            btnPositive = resources.getString(R.string.qr_dialog_positive_th)
            btnNegative = resources.getString(R.string.qr_dialog_negative_th)
        }

        AlertDialog.Builder(activity).apply {
            setMessage(message)
            setPositiveButton(btnPositive) { _, _ ->
                gotoSetting(activity)
            }
            setNegativeButton(btnNegative) { _, _ ->
                finish()
            }
            setCancelable(false)
        }.create().show()
    }

    private fun gotoSetting(context: Context) {
        val pref = PreferencesTrueID(context)
        val uri = Uri.fromParts("package", pref.package_name, null)
        Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = uri
        }.also {
            startActivityForResult(it, 200)
        }
    }
}